import Foundation
import Vapor
import FluentPostgreSQL

final class LicenseGradeTime: Codable {
    
    var id: UUID?
    var startTime: Date // 開始申請時間
    var endTime: Date // 結束申請時間
    var state: Int // 0: 證照 1: 成績進步
    
    init(startTime: Date, endTime: Date, state: Int) {
        self.startTime = startTime
        self.endTime = endTime
        self.state = state
    }
}

// Make it conform to Fluent's Model
extension LicenseGradeTime: PostgreSQLUUIDModel {}
extension LicenseGradeTime: Content {}
extension LicenseGradeTime: Parameter {}
extension LicenseGradeTime: Migration {}
