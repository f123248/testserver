// 連結 該學生所有已投票的課程IDs 跟 投票該課程的學生們IDs
import FluentPostgreSQL
import Foundation

final class VotePivot: PostgreSQLUUIDPivot,ModifiablePivot {
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var recommendcourseID: RecommendCourse.ID
    var studentID: Student.ID
    var voteTime: Date = Date()
    var courseWeek: [Int] = [0,0,0,0,0,0,0] //學生希望上課的星期 0:無 1:有 ex: [1,1,1,1,1,0,0] 希望平日一到五上課
    
    // Define the Left and Right types required by Pivot.
    typealias Left = RecommendCourse
    typealias Right = Student
    
    static let entity = "VotePivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.recommendcourseID
    static let rightIDKey: RightIDKey = \.studentID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ recommendcourse: RecommendCourse, _ student: Student) throws {
        self.recommendcourseID = try recommendcourse.requireID()
        self.studentID = try student.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension VotePivot: Migration {}
