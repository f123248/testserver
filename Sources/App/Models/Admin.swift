//
//  Admin.swift
//  App
//
//  Created by hsu on 2019/2/12.
//

import Foundation
import Vapor
import FluentPostgreSQL

final class Admin: Codable {
    
    var id: UUID?
    var account: String
    var password: String
    var token: String?
    
    init(account:String, password: String, token: String) {
        self.account = account
        self.password = password
        self.token = token
    }
}

// Make the User model conform to Fluent's Model
extension Admin: PostgreSQLUUIDModel {}
extension Admin: Content {}
extension Admin: Migration {}
extension Admin: Parameter {}


