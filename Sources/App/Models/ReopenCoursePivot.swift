//已結束的課程重新開啟新課程
import FluentPostgreSQL
import Foundation
import Vapor

final class ReopenCoursePivot: PostgreSQLUUIDPivot,ModifiablePivot, Content {
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var oldCourseID: Course.ID
    var newCourseID: Course.ID
    
    // Define the Left and Right types required by Pivot.
    
    typealias Left = Course
    typealias Right = Course
    
    static let entity = "ReopenCoursePivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.oldCourseID
    static let rightIDKey: RightIDKey = \.newCourseID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ oldCourse: Course, _ newCourse: Course) throws {
        self.oldCourseID = try oldCourse.requireID()
        self.newCourseID = try newCourse.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension ReopenCoursePivot: Migration {}


