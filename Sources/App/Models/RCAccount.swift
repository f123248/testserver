//點名帳號
import FluentPostgreSQL
import Foundation
import Vapor

final class RCAccount: Codable {
    
    var id: UUID?
    var account: String
    var password: String
    
    init(account: String, password: String) throws {
        self.account = account
        self.password = password
    }
}
extension RCAccount: Migration {}
extension RCAccount: PostgreSQLUUIDModel {}
extension RCAccount: Content {}
extension RCAccount: Parameter {}
extension RCAccount {
    // RCAStudentPivot
    var rcaStudent: Siblings<RCAccount,Student,RCAStudentPivot> {
        return siblings()
    }
    
    // RCACoursePivot
    var rcaCourse: Siblings<RCAccount,Course,RCACoursePivot> {
        return siblings()
    }
}
