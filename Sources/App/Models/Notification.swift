//
//  Notice.swift
//  App
//
//  Created by hsu on 2019/5/21.
//

import Foundation
import Vapor
import FluentPostgreSQL

final class Notification: Codable {
    
    var id: UUID?
    var title: String
    var type: Int // 0:一般公告 1:課程群組 2:個人
    var courseType: Int //0:語言 1：證照 2:其他 3:教發中心
    var pushTime: Date
    var content: String
    
    init(title: String, type: Int, courseType: Int, pushTime: Date, content: String) {
        self.title = title
        self.type = type
        self.courseType = courseType
        self.pushTime = pushTime
        self.content = content
    }
}

// Make the User model conform to Fluent's Model
extension Notification: PostgreSQLUUIDModel {}
extension Notification: Content {}
extension Notification: Migration {}
extension Notification: Parameter {}
extension Notification{
    // NoticeStudentPivot
    var noticeStudent: Siblings<Notification,Student,NoticeStudentPivot> {
        return siblings()
    }
}
