// 連結課程跟點名帳號
import FluentPostgreSQL
import Foundation
import Vapor

final class RCACoursePivot: PostgreSQLUUIDPivot,ModifiablePivot, Content {
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var rcAccountID: RCAccount.ID
    var courseID: Course.ID
    var courseCode: String = ""
    
    // Define the Left and Right types required by Pivot.
    
    typealias Left = RCAccount
    typealias Right = Course
    
    static let entity = "RCACoursePivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.rcAccountID
    static let rightIDKey: RightIDKey = \.courseID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ rcAccount: RCAccount, _ course: Course) throws {
        self.rcAccountID = try rcAccount.requireID()
        self.courseID = try course.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension RCACoursePivot: Migration {}
