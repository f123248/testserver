import Foundation
import Vapor
import FluentPostgreSQL

final class Lottery: Codable {
    
    var id: UUID?
    var getLotteryDate: Date // 獲得抽獎券日期
    var isUsedDate: Date // 兌換日期
    var isUsed: Bool // 是否使用過
    var pointID: Point.ID
    var studentID: Student.ID
//    var type: Int //0:一級抽獎券 1:二級抽獎券 2:特級抽獎券
    
    init(getLotteryDate: Date, isUsedDate: Date, isUsed: Bool, pointID: Point.ID, studentID: Student.ID) {
        self.getLotteryDate = getLotteryDate
        self.isUsedDate = isUsedDate
        self.isUsed = isUsed
        self.pointID = pointID
        self.studentID = studentID
//        self.type = type
    }
}

// Make it conform to Fluent's Model
extension Lottery: PostgreSQLUUIDModel {}
extension Lottery: Content {}
extension Lottery: Parameter {}
extension Lottery {
    var student: Parent<Lottery, Student> {
        return parent(\.studentID)
    }
    
    var point: Parent<Lottery, Point> {
        return parent(\.pointID)
    }
}
extension Lottery: Migration {
    static func prepare(
        on connection: PostgreSQLConnection
        ) -> Future<Void> {
        return Database.create(self, on:connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.studentID, to: \Student.id)
        }
    }
}



