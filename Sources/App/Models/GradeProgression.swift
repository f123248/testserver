import Foundation
import Vapor
import FluentPostgreSQL

final class GradeProgression: Codable {
    
    var id: UUID?
    var applyYear: String
    var applySemester: String
    var gradePicture: String
    var gradeDescription: String
    var studentID: Student.ID
    var gradeState: Int
    var verificationRejectedReason: String = ""
    var applyDate: Date = Date()
    var verifiedDate: Date = Date()
    
    init(applyYear: String, applySemester: String, gradePicture: String, gradeDescription: String, studentID: Student.ID, gradeState: Int, verificationRejectedReason: String, applyDate: Date, verifiedDate: Date) {
        self.applyYear = applyYear
        self.applySemester = applySemester
        self.gradePicture = gradePicture
        self.gradeDescription = gradeDescription
        self.studentID = studentID
        self.gradeState = gradeState
        self.verificationRejectedReason = verificationRejectedReason
        self.applyDate = applyDate
        self.verifiedDate = verifiedDate
    }
}

// Make it conform to Fluent's Model
extension GradeProgression: PostgreSQLUUIDModel {}
extension GradeProgression: Content {}
extension GradeProgression: Parameter {}
extension GradeProgression {
    var student: Parent<GradeProgression, Student> {
        return parent(\.studentID)
    }
}
extension GradeProgression: Migration {
    static func prepare(
        on connection: PostgreSQLConnection
        ) -> Future<Void> {
        return Database.create(self, on:connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.studentID, to: \Student.id)
        }
    }
}


