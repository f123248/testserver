// Create a basic model for the recommend course
import Foundation
import Vapor
import FluentPostgreSQL

//MARK: verifyState
//0 未審核
//1 審核通過
//2 審核未通過
//3 待投票
//4 投票通過
//5 投票未通過
//6 開放報名中
//7 報名人數不足
//8 上課中
//9 課程結束
enum verifyState:String {
    case unverify, verifyPass, verifyFail, voting, votePass, voteFail
}
final class RecommendCourse: Codable {
    
    var id: UUID?
    var courseName: String
    var courseType: Int // 0:語言 1:證照 2:其他
    var campus: Int // 0:博愛校區 1:天母校區
    var recommendReason: String = ""
    var courseIntroduction: String = ""
    var proposerID: UUID?
    var voteStartTime: Date = Date()
    var voteEndTime: Date = Date()
    var recommendTime: Date = Date()
    var verifiedTime: Date = Date()
    var verifyState: Int = 0 //請洽第六行
    var verificationRejectedReason: String = ""
    var numberOfVoters:Int = 0
    
    init(courseName: String, courseType: Int, campus: Int, recommendReason: String, courseIntroduction: String,
         proposerID: UUID?, voteStartTime: Date, voteEndTime: Date, recommendTime: Date, verifiedTime: Date,
         verifyState: Int,verificationRejectedReason: String, numberOfVoters: Int) {
        self.courseName = courseName
        self.courseType = courseType
        self.campus = campus
        self.recommendReason = recommendReason
        self.courseIntroduction = courseIntroduction
        self.proposerID = proposerID
        self.voteStartTime = voteStartTime
        self.voteEndTime = voteEndTime
        self.recommendTime = recommendTime
        self.verifiedTime = verifiedTime
        self.verifyState = verifyState
        self.verificationRejectedReason = verificationRejectedReason
        self.numberOfVoters = numberOfVoters
    }
}

// Make the User model conform to Fluent's Model
extension RecommendCourse: PostgreSQLUUIDModel {}
extension RecommendCourse: Content {}
extension RecommendCourse: Migration {}
extension RecommendCourse: Parameter {}
extension RecommendCourse {
    // VotePivot
    var voteStudents: Siblings<RecommendCourse,Student,VotePivot> {
        return siblings()
    }
    
    // OpenCoursePivot
    var openCourse: Siblings<RecommendCourse,Course,OpenCoursePivot> {
        return siblings()
    }
}
