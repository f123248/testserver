import Foundation
import Vapor
import FluentPostgreSQL

final class License: Codable {
    
    var id: UUID?
    var licenseName: String
    var licenseType: Int // 0: 國內 1:國外
    var licensePicture: String
    var licenseDate: String //考取證照日期
    var studentID: Student.ID
    var licenseState: Int // 0: 未審核 1: 審核通過 2: 審核不通過
    var verificationRejectedReason: String = ""
    var applyDate: Date = Date()
    var verifiedDate: Date = Date()
    
    init(licenseName: String, licenseType: Int, licenseDate: String, licensePicture: String, studentID: Student.ID, licenseState: Int, verificationRejectedReason: String, applyDate: Date, verifiedDate: Date) {
        self.licenseName = licenseName
        self.licenseType = licenseType
        self.licensePicture = licensePicture
        self.studentID = studentID
        self.licenseState = licenseState
        self.verificationRejectedReason = verificationRejectedReason
        self.licenseDate = licenseDate
        self.applyDate = applyDate
        self.verifiedDate = verifiedDate
    }
}

// Make it conform to Fluent's Model
extension License: PostgreSQLUUIDModel {}
extension License: Content {}
extension License: Parameter {}
extension License {
    var student: Parent<License, Student> {
        return parent(\.studentID)
    }
}
extension License: Migration {
    static func prepare(
        on connection: PostgreSQLConnection
        ) -> Future<Void> {
        return Database.create(self, on:connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.studentID, to: \Student.id)
        }
    }
}

