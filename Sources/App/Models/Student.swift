// Create a basic model for the student
import Foundation
import Vapor
import FluentPostgreSQL

final class Student: Codable {
    
    var id: UUID?
    var studentID: String
    var password: String
    var studentName: String
    var studentType: Int // 0:一般生 1:陽光種子
    var studentDepartment: String // 系所
    
    var studentRegisterDate: Date
    var studentAddress: String
    var studentPhoneNumber: String
    var studentQRcode: String

    var studentIdCard:  String  // 學生證正面檔名
    var idCard: String    // 身分證正面檔名
    var idCardBackSide: String // 身分證背面檔名
    
    var bankbook: String  // 存簿影本檔名
    var portrait: String  // 大頭照檔名
   
    var studentBlackList: Int // 黑名單
    var blackLastDate: Date  // 黑名單解除日期
    var token: String
    var pushToken: String
    
    init(studentID: String, password: String, studentName: String,studentType: Int, studentDepartment: String,
         studentRegisterDate: Date, studentAddress: String, studentPhoneNumber: String, studentQRcode: String,
         studentIdCard: String, idCard: String, idCardBackSide: String,
         bankbook: String, portrait: String, studentBlackList: Int, blackLastDate: Date, token: String, pushToken: String) {
        
        self.studentID = studentID
        self.password = password
        self.studentName = studentName
        self.studentType = studentType
        self.studentDepartment = studentDepartment
        
        self.studentRegisterDate = studentRegisterDate
        self.studentAddress = studentAddress
        self.studentPhoneNumber = studentPhoneNumber
        self.studentQRcode = studentQRcode

        self.studentIdCard = studentIdCard
        self.idCard = idCard
        self.idCardBackSide = idCardBackSide
        
        self.bankbook = bankbook
        self.portrait = portrait
        self.studentBlackList = studentBlackList
        self.blackLastDate = blackLastDate
        self.token = token
        self.pushToken = pushToken
    }
    
}

// Make it conform to Fluent's Model
extension Student: PostgreSQLUUIDModel {}
extension Student: Content {}
extension Student: Migration {}
extension Student: Parameter {}

// Build relationship to connect with other models
extension Student {
    // VotePivot
    var voteCourses: Siblings<Student,RecommendCourse,VotePivot> {
        return siblings()
    }
    
    // InCoursePivot
    var inCourses: Siblings<Student,Course,InCoursePivot> {
        return siblings()
    }
    
    // ApplyPivot
    var applyCourses: Siblings<Student,Course,ApplyPivot> {
        return siblings()
    }
    
    // FavoritePivot
    var favoriteCourses: Siblings<Student,Course,FavoritePivot> {
        return siblings()
    }
    
    // FavoriteRecPivot
    var favRecCourses: Siblings<Student,RecommendCourse,FavRecPivot> {
        return siblings()
    }
    
    // Course evaluation is Children of Student
    var courseEvaluation: Children<Student, CourseEvaluation> {
        return children(\.studentID)
    }
    
    // Leave is Children of Student
    var leave: Children<Student, Leave> {
        return children(\.studentID)
    }
    
    // CourseSatisfactionAns is Children of Student
    var courseSatisfactionAns: Children<Student, CourseSatisfactionAns> {
        return children(\.studentID)
    }
    
    // RollCall is Children of Student
    var rollCall: Children<Student, RollCall> {
        return children(\.studentID)
    }
    
    // License is Children of Student
    var license: Children<Student, License> {
        return children(\.studentID)
    }
    
    // GradeProgression is Children of Student
    var gradeProgression: Children<Student, GradeProgression> {
        return children(\.studentID)
    }
    
    // RCAStudentPivot
    var rcaStudent: Siblings<Student,RCAccount,RCAStudentPivot> {
        return siblings()
    }
    
    // Lottery is Children of Student
    var lottery: Children<Student, Lottery> {
        return children(\.studentID)
    }
    
    // Point is Children of Student
    var point: Children<Student, Point> {
        return children(\.studentID)
    }
    
    // NoticeStudentPivot
    var noticeStudent: Siblings<Student,Notification,NoticeStudentPivot> {
        return siblings()
    }
}
