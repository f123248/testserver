//
//  Banner.swift
//  App
//
//  Created by hsu on 2019/6/11.
//

import Foundation
import Vapor
import FluentPostgreSQL

final class Banner: Codable {
    
    var id: UUID?
    var title: String
    var content: String
    var pictureName: String
    var sort: Int
    var webPage: String
    
    init(title: String, content: String, pictureName: String, sort: Int, webPage: String) {
        self.title = title
        self.content = content
        self.pictureName = pictureName
        self.sort = sort
        self.webPage = webPage
    }
}

// Make the User model conform to Fluent's Model
extension Banner: PostgreSQLUUIDModel {}
extension Banner: Content {}
extension Banner: Migration {}
extension Banner: Parameter {}

