// Create a basic model for leave
import Foundation
import Vapor
import FluentPostgreSQL

final class Leave: Codable {
    
    var id: UUID?
    var leaveDate: Date = Date()
    var leaveReason: Int = 0 // 0:病假 1:事假 2:公假 3:其他
    var detailReason: String = ""
    var courseID: Course.ID
    var studentID: Student.ID
    
//    var courseName: String=""
//    var studentName: String=""
    
    init(leaveDate: Date, leaveReason: Int, detailReason: String, courseID: Course.ID, studentID: Student.ID) {
        self.leaveDate = leaveDate
        self.leaveReason = leaveReason
        self.detailReason = detailReason
        self.courseID = courseID
        self.studentID = studentID
//        self.courseName = courseName
//        self.studentName = studentName
    }
}

// Make it conform to Fluent's Model
extension Leave: PostgreSQLUUIDModel {}
extension Leave: Content {}
extension Leave: Parameter {}
extension Leave {
    var course: Parent<Leave, Course> {
        return parent(\.courseID)
    }
    
    var student: Parent<Leave, Student> {
        return parent(\.studentID)
    }
}
extension Leave: Migration {
    static func prepare(
        on connection: PostgreSQLConnection
        ) -> Future<Void> {
        return Database.create(self, on:connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.courseID, to: \Course.id)
        }
    }
}
