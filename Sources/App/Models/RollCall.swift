// Create a basic model for roll call
import Foundation
import Vapor
import FluentPostgreSQL

final class RollCall: Codable {
    
    var id: UUID?
    var rollCallDate: Date = Date()
    var courseID: Course.ID
    var studentID: Student.ID
    
    init(rollCallDate: Date, courseID: Course.ID, studentID: Student.ID) {
        self.rollCallDate = rollCallDate
        self.courseID = courseID
        self.studentID = studentID
    }
}

// Make it conform to Fluent's Model
extension RollCall: PostgreSQLUUIDModel {}
extension RollCall: Content {}
extension RollCall: Parameter {}
extension RollCall {
    var course: Parent<RollCall, Course> {
        return parent(\.courseID)
    }
    
    var student: Parent<RollCall, Student> {
        return parent(\.studentID)
    }
}
extension RollCall: Migration {
    static func prepare(
        on connection: PostgreSQLConnection
        ) -> Future<Void> {
        return Database.create(self, on:connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.courseID, to: \Course.id)
        }
    }
}

