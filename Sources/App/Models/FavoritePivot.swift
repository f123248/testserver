// 該學生所有收藏的課程IDs
import FluentPostgreSQL
import Foundation

final class FavoritePivot: PostgreSQLUUIDPivot,ModifiablePivot {
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var courseID: Course.ID
    var studentID: Student.ID
    var favoriteTime: Date = Date()
    
    // Define the Left and Right types required by Pivot.
    
    typealias Left = Course
    typealias Right = Student
    
    static let entity = "FavoritePivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.courseID
    static let rightIDKey: RightIDKey = \.studentID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ course: Course, _ student: Student) throws {
        self.courseID = try course.requireID()
        self.studentID = try student.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension FavoritePivot: Migration {}


