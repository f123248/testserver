// 連結學生跟點名帳號
import FluentPostgreSQL
import Foundation
import Vapor

final class RCAStudentPivot: PostgreSQLUUIDPivot,ModifiablePivot, Content {
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var rcAccountID: RCAccount.ID
    var studentID: Student.ID
    
    // Define the Left and Right types required by Pivot.
    
    typealias Left = RCAccount
    typealias Right = Student
    
    static let entity = "RCAStudentPivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.rcAccountID
    static let rightIDKey: RightIDKey = \.studentID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ rcAccount: RCAccount, _ student: Student) throws {
        self.rcAccountID = try rcAccount.requireID()
        self.studentID = try student.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension RCAStudentPivot: Migration {}
