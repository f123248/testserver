//
//  CourseSatisfactionAns.swift
//  App
//
//  Created by hsu on 2019/3/22.
//

import Vapor
import FluentPostgreSQL

final class CourseSatisfactionAns: Codable {
    
    var id: UUID?
    var courseSatisfactionAnsUrl:String
    var courseID: Course.ID
    var studentID: Student.ID
    
    init(courseSatisfactionAnsUrl:String,courseID: Course.ID,studentID: Student.ID){
        self.courseSatisfactionAnsUrl = courseSatisfactionAnsUrl
        self.courseID = courseID
        self.studentID = studentID
    }
}

// Make it conform to Fluent's Model
extension CourseSatisfactionAns: PostgreSQLUUIDModel {}
extension CourseSatisfactionAns: Content {}
extension CourseSatisfactionAns: Parameter {}
extension CourseSatisfactionAns: Migration {
}
extension CourseSatisfactionAns {
    var student: Parent<CourseSatisfactionAns,Student> {
        return parent(\.studentID)
    }
}
extension CourseSatisfactionAns {
    var course: Parent<CourseSatisfactionAns,Course> {
        return parent(\.courseID)
    }
}
