// Create a basic model for course evaluation
import Foundation
import Vapor
import FluentPostgreSQL

final class CourseEvaluation: Codable {
    
    var id: UUID?
    var state: Int // 0:公開 1:私人
    var studentContent: String
    var evaluationTime: Date = Date()
    var teacherResponse: String = ""
    var teacherResponseTime: Date = Date()
    var courseID: Course.ID
    var studentID: Student.ID
    
    init(state: Int, studentContent: String, evaluationTime: Date ,teacherResponse: String, teacherResponseTime: Date,
         courseID: Course.ID, studentID: Student.ID) {
        self.state = state
        self.studentContent = studentContent
        self.evaluationTime = evaluationTime
        self.teacherResponse = teacherResponse
        self.teacherResponseTime = teacherResponseTime
        self.courseID = courseID
        self.studentID = studentID
    }
}

// Make it conform to Fluent's Model
extension CourseEvaluation: PostgreSQLUUIDModel {}
extension CourseEvaluation: Content {}
extension CourseEvaluation: Parameter {}
extension CourseEvaluation {
    var course: Parent<CourseEvaluation, Course> {
        return parent(\.courseID)
    }
}
extension CourseEvaluation: Migration {
    static func prepare(
        on connection: PostgreSQLConnection
        ) -> Future<Void> {
        return Database.create(self, on:connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.courseID, to: \Course.id)
        }
    }
}
extension CourseEvaluation {
    var student: Parent<CourseEvaluation, Student> {
        return parent(\.studentID)
    }
}
