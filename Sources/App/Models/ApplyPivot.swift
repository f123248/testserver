// 連結 該學生所有已報名的課程IDs 跟 報名該課程的學生們IDs
import FluentPostgreSQL
import Foundation
import Vapor

final class ApplyPivot: PostgreSQLUUIDPivot,ModifiablePivot, Content{
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var courseID: Course.ID
    var studentID: Student.ID
    var applyTime: Date = Date()
    var applyResult: Int = 0
    
    
    // Define the Left and Right types required by Pivot.
    typealias Left = Course
    typealias Right = Student
    
    static let entity = "ApplyPivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.courseID
    static let rightIDKey: RightIDKey = \.studentID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ course: Course, _ student: Student) throws {
        self.courseID = try course.requireID()
        self.studentID = try student.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension ApplyPivot: Migration {}
extension ApplyPivot: Parameter {}
