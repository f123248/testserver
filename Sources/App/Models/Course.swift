// Create a basic model for course
import Foundation
import Vapor
import FluentPostgreSQL

enum courseState:String {
    case apply, during, end
}

final class Course: Codable {
    
    var id: UUID?
    var courseName: String
    var courseType: Int // 0:語言 1:證照 2:其他
    var campus: Int // 0:博愛校區 1:天母校區
    var courseDate: [Date] // 上課日期+時間
    
    var courseTeacher: String
    var coursePlace: String
    var courseCost: Int
    var targetStudent: Int // 0:臺北市立大學在學學生
    
    var courseState: Int // 0:報名中 1:上課中 2:課程結束
    var numberOfApply: Int // 錄取學生數
    var firstNumberOfStudents: Int // 第一階段招生人數
    var firstApplyStartTime: Date // 第一階段報名開始時間
    var firstApplyEndTime: Date // 第一階段報名結束時間
    var secondNumberOfStudents: Int // 第二階段招生人數
    var secondApplyStartTime: Date // 第二階段報名開始時間
    var secondApplyEndTime: Date // 第二階段報名結束時間
    
    var applyMethod: Int // 報名方式 0:透過 push pull app
    var enrollPost: Int // 錄取公告  0:透過 push pull app
    
    var courseIntroduction: String
    var courseContent: [String] // 每堂課的上課內容
    var other: String
    
    init(courseName: String, courseType: Int, campus: Int, courseDate: [Date],
         courseTeacher: String, coursePlace: String, courseCost: Int, targetStudent: Int,
         courseState: Int, numberOfApply: Int, firstNumberOfStudents: Int, firstApplyStartTime: Date,
         firstApplyEndTime: Date, secondNumberOfStudents: Int, secondApplyStartTime: Date, secondApplyEndTime: Date,
         applyMethod: Int, enrollPost: Int,
         courseIntroduction: String, courseContent:[String] ,other: String) {
        
        self.courseName = courseName
        self.courseType = courseType
        self.campus = campus
        self.courseDate = courseDate
        
        self.courseTeacher = courseTeacher
        self.coursePlace = coursePlace
        self.courseCost = courseCost
        self.targetStudent = targetStudent
        
        self.courseState = courseState
        self.numberOfApply = numberOfApply
        
        self.firstNumberOfStudents = firstNumberOfStudents
        self.firstApplyStartTime = firstApplyStartTime
        self.firstApplyEndTime = firstApplyEndTime
        
        self.secondNumberOfStudents = secondNumberOfStudents
        self.secondApplyStartTime = secondApplyStartTime
        self.secondApplyEndTime = secondApplyEndTime
        
        self.applyMethod = applyMethod
        self.enrollPost = enrollPost
        
        self.courseIntroduction = courseIntroduction
        self.courseContent = courseContent
        self.other = other
    }
}

// Make it conform to Fluent's Model
extension Course: PostgreSQLUUIDModel {}
extension Course: Content {}
extension Course: Migration {}
extension Course: Parameter {}

// Build relationship to connect with other models
extension Course {
    
    
    // InCoursePivot
    var inCourseStudents: Siblings<Course,Student,InCoursePivot> {
        return siblings()
    }
    
    // ApplyPivot
    var applyStudents: Siblings<Course,Student,ApplyPivot> {
        return siblings()
    }

    // Course evaluation is Children of Course
    var courseEvaluation: Children<Course, CourseEvaluation> {
        return children(\.courseID)
    }
    var courseSatisfaction: Children<Course, CourseSatisfaction> {
        return children(\.courseID)
    }
    
    // Leave is Children of Course
    var leave: Children<Course, Leave> {
        return children(\.courseID)
    }
    
    // CourseSatisfactionAns is Children of Course
    var courseSatisfactionAns: Children<Course, CourseSatisfactionAns> {
        return children(\.courseID)
    }
    
    // RollCall is Children of Course
    var rollCall: Children<Course, RollCall> {
        return children(\.courseID)
    }
    
    //OpenCoursePivot
    var openCourse: Siblings<Course,RecommendCourse,OpenCoursePivot> {
        return siblings()
    }
    
    //ReopenCoursePivot
    var reopenCourse: Siblings<Course,Course,ReopenCoursePivot> {
        return siblings(ReopenCoursePivot.leftIDKey, ReopenCoursePivot.rightIDKey)
    }
    
    // RCACoursePivot
    var rcaCourse: Siblings<Course,RCAccount,RCACoursePivot> {
        return siblings()
    }
    
    func reopen(course: Course, on connection: DatabaseConnectable) -> Future<(current: Course, reopen: Course)> {
        return Future.flatMap(on: connection) {
            let pivot = try ReopenCoursePivot(self, course)
            return pivot.save(on: connection).transform(to: (self, course))
        }
    }
}
