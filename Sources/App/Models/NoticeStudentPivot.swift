// 連結 學生跟通知
import FluentPostgreSQL
import Foundation

final class NoticeStudentPivot: PostgreSQLUUIDPivot,ModifiablePivot {
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var notificationID: Notification.ID
    var studentID: Student.ID
    
    // Define the Left and Right types required by Pivot.
    typealias Left = Notification
    typealias Right = Student
    
    static let entity = "NoticeStudentPivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.notificationID
    static let rightIDKey: RightIDKey = \.studentID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ notice: Notification, _ student: Student) throws {
        self.notificationID = try notice.requireID()
        self.studentID = try student.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension NoticeStudentPivot: Migration {}
