// 該學生所有收藏的投票中課程IDs
import FluentPostgreSQL
import Foundation

final class FavRecPivot: PostgreSQLUUIDPivot,ModifiablePivot {
    
    var id: UUID?
    // Define two properties to link to the IDs of RecommendCourse and Student.
    var recommendcourseID: RecommendCourse.ID
    var studentID: Student.ID
    var favRecTime: Date = Date()
    
    // Define the Left and Right types required by Pivot.
    
    typealias Left = RecommendCourse
    typealias Right = Student
    
    static let entity = "FavRecPivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.recommendcourseID
    static let rightIDKey: RightIDKey = \.studentID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ recommendcourse: RecommendCourse, _ student: Student) throws {
        self.recommendcourseID = try recommendcourse.requireID()
        self.studentID = try student.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension FavRecPivot: Migration {}

