
import Foundation
import Vapor
import FluentPostgreSQL

final class LuckyDay: Codable {
    
    var id: UUID?
    var points: Int
    var percent: Double
    var monthTotal: Int
    
    init(points: Int, percent: Double, monthTotal: Int) {
        self.points = points
        self.percent = percent
        self.monthTotal = monthTotal
    }
}

// Make the User model conform to Fluent's Model
extension LuckyDay: PostgreSQLUUIDModel {}
extension LuckyDay: Content {}
extension LuckyDay: Migration {}
extension LuckyDay: Parameter {}
