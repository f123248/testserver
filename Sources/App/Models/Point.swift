import Foundation
import Vapor
import FluentPostgreSQL

final class Point: Codable {
    
    var id: UUID?
    var pointReason: String // 獲點原因
    var getPoint: Int // 獲得的點數
    var getPointDate: Date // 獲得點數日期
    var studentID: Student.ID
    
    
    init(pointReason: String, getPoint: Int, getPointDate: Date, studentID: Student.ID) {
        self.pointReason = pointReason
        self.getPoint = getPoint
        self.getPointDate = getPointDate
        self.studentID = studentID
    }
}

// Make it conform to Fluent's Model
extension Point: PostgreSQLUUIDModel {}
extension Point: Content {}
extension Point: Parameter {}
extension Point {
    
    var student: Parent<Point, Student> {
        return parent(\.studentID)
    }
    
    // Lottery is Children of Point
    var lottery: Children<Point, Lottery> {
        return children(\.pointID)
    }
}
extension Point: Migration {
    static func prepare(
        on connection: PostgreSQLConnection
        ) -> Future<Void> {
        return Database.create(self, on:connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.studentID, to: \Student.id)
        }
    }
}


