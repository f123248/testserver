//
//  CourseSatisfaction.swift
//  App
//
//  Created by 徐兆陽 on 2019/3/5.
//

import Foundation
import Vapor
import FluentPostgreSQL

final class CourseSatisfaction: Codable {
    
    var id: UUID?
    var courseSatisfactionEmptyUrl:String
    var courseID: Course.ID
    
    init(courseSatisfactionEmptyUrl:String,courseID: Course.ID){
        self.courseSatisfactionEmptyUrl = courseSatisfactionEmptyUrl
        self.courseID = courseID
    }
}

// Make it conform to Fluent's Model
extension CourseSatisfaction: PostgreSQLUUIDModel {}
extension CourseSatisfaction: Content {}
extension CourseSatisfaction: Parameter {}
extension CourseSatisfaction: Migration {
}
extension CourseSatisfaction {
    var course: Parent<CourseSatisfaction,Course> {
        return parent(\.courseID)
    }
}

