//推薦課程加到正式課程中
import FluentPostgreSQL
import Foundation
import Vapor

final class OpenCoursePivot: PostgreSQLUUIDPivot,ModifiablePivot, Content {
    
    var id: UUID?
    // Define two properties to link to the IDs of Course and Student.
    var rcmCourseID: RecommendCourse.ID
    var courseID: Course.ID
    
    // Define the Left and Right types required by Pivot.
    
    typealias Left = RecommendCourse
    typealias Right = Course
    
    static let entity = "OpenCoursePivot"
    
    // Tell Fluent the key path of the two ID properties for each side of the relationship.
    static let leftIDKey: LeftIDKey = \.rcmCourseID
    static let rightIDKey: RightIDKey = \.courseID
    // Implement the throwing initializer, as required by ModifiablePivot.
    init(_ rcmCourse: RecommendCourse, _ course: Course) throws {
        self.rcmCourseID = try rcmCourse.requireID()
        self.courseID = try course.requireID()
    }
}
// Conform to Migration so Fluent can set up the table.
extension OpenCoursePivot: Migration {}
extension OpenCoursePivot: Parameter {}

