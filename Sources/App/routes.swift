import Vapor

// Register your application's routes here.
public func routes(_ router: Router) throws {
    let courseController = CourseController()
    try router.register(collection: courseController)
    
    let courseWebController = CourseWebController()
    try router.register(collection: courseWebController)
    
    let studentController = StudentController()
    try router.register(collection: studentController)
    
    let recommendCourseController = RecommendCourseController()
    try router.register(collection: recommendCourseController)
    
    let courseEvaluationController = CourseEvaluationController()
    try router.register(collection: courseEvaluationController)
    
    let leaveController = LeaveController()
    try router.register(collection: leaveController)
    
    let rollCallController = RollCallController()
    try router.register(collection: rollCallController)
    
    let adminCourseController = AdminController()
    try router.register(collection: adminCourseController)
    

    let courseSatisfactionController = CourseSatisfactionController()
    try router.register(collection: courseSatisfactionController)
    
    let courseSatisfactionAnsController = CourseSatisfactionAnsController()
    try router.register(collection: courseSatisfactionAnsController)
    
    let licenseController = LicenseController()
    try router.register(collection: licenseController)
    
    let gradeProgressionController = GradeProgressionController()
    try router.register(collection: gradeProgressionController)
    
    let rcAccountController = RCAccountController()
    try router.register(collection: rcAccountController)
    
    let studentPivotController = StudentPivotController()
    try router.register(collection: studentPivotController)
    
    let notificationController = NotificationController()
    try router.register(collection: notificationController)
    
    
    let lotteryController = LotteryController()
    try router.register(collection: lotteryController)
    
    let pointController = PointController()
    try router.register(collection: pointController)
    
    let bannerController = BannerController()
    try router.register(collection: bannerController)
    
    let luckyDayController = LuckyDayController()
    try router.register(collection: luckyDayController)
    
    let licenseGradeTimeController = LicenseGradeTimeController()
    try router.register(collection: licenseGradeTimeController)

    // Basic "It works" example
    router.get { req in
        return "It works!"
    }
    
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }
    
    router.get("login") { req -> Future<View> in
        return try req.view().render("login")
    }
}
