import Vapor
import Fluent

struct LeaveController: RouteCollection {
    
    func boot(router: Router) throws {
        let webRoute = router
        webRoute.get("viewLeave", use:leaveViewHandler)
//        webRoute.get("viewLeave",Course.parameter, use:leaveViewHandler)
        webRoute.get("viewLeave",UUID.parameter, use: getLeaveView1Handler)
        
        
        let leaveRoute = router.grouped("api", "leave")
        leaveRoute.post(Leave.self, use: createHandler)
        leaveRoute.get(use: getAllHandler)
        leaveRoute.get(Leave.parameter, use: getHandler)
        leaveRoute.get(Leave.parameter, "course", use: getCourseHandler)
        leaveRoute.get(Leave.parameter, "student", use: getStudentHandler)
        
        let courseRoute = router.grouped("api","course")
        courseRoute.get("getCourseDate",Course.parameter, use: getCourseDateHandler)
//        leaveRoute.get(Leave.parameter, "viewLeave", use: getLeaveViewHandler)
        
    }
    func getCourseDateHandler(_ req: Request) throws -> Future<CourseDate> {
        return try req.parameters.next(Course.self).map(to: CourseDate.self){
            course in
            if course.courseDate.count == 0 {
                
                let timeInterval:TimeInterval = 0
                let date1 = Date(timeIntervalSince1970: timeInterval)

                var dateArr: [Date] = []
                dateArr.append(date1)

                return CourseDate(courseDate: dateArr)
            }
            else {

                return CourseDate(courseDate: course.courseDate)
            }
        }
    }
    
   
    func createHandler(
        _ req: Request,
        leave: Leave
        ) throws -> Future<Leave> {
        var incourseCheck: Bool = false
        return InCoursePivot.query(on: req)
            .filter(\InCoursePivot.courseID==leave.courseID)
            .filter(\InCoursePivot.studentID==leave.studentID)
            .first()
            .flatMap(to: Leave.self) {
                checkincourses in
//                let avoidincourse = InCoursePivot.query(on: req)
//                .all()
//                for checkincourse in checkincourses {
//                    if leave.courseID == checkincourse.courseID {
//                        if leave.studentID == checkincourse.studentID {
//                            incourseCheck = true
//                        }
//                        else {
//                            incourseCheck = false
//                        }
                if checkincourses == nil {
                    incourseCheck = false
                }
                else {
                    incourseCheck = true
                }
//                    }
//                    else {
//                        incourseCheck = false
//                    }
//        
//                }
                if incourseCheck {
                    
                    return Leave.query(on: req).all().flatMap(to: Leave.self) { allLeaves in
                        
                        for oneleave in allLeaves {
                            if leave.courseID == oneleave.courseID {
                                if leave.studentID == oneleave.studentID {
                                    let newDateFormatter: DateFormatter = DateFormatter()
                                    newDateFormatter.dateFormat = "yyyy/MM/dd"
                                    let oldDateFormatter: DateFormatter = DateFormatter()
                                    oldDateFormatter.dateFormat = "yyyy/MM/dd"
                                    let newDateString: String = newDateFormatter.string(from: leave.leaveDate)
                                    let oldDateString: String = oldDateFormatter.string(from: oneleave.leaveDate)
                                    if newDateString == oldDateString {
                                        throw Abort(.custom(code: 601, reasonPhrase: "Already added a leave for this day."))
                                    }
                                }
                            }   
                        }

                        let returnleave:Leave = Leave(leaveDate: leave.leaveDate, leaveReason: leave.leaveReason, detailReason: leave.detailReason, courseID: leave.courseID, studentID: leave.studentID)
                        return returnleave.save(on: req)
                    }
                    
                }
                else{                    
                    throw Abort(.custom(code: 600, reasonPhrase: "Cannot add leave request because this student not in this course."))
                        //.nonAuthoritativeInformation, reason:"Cannot add leave request because this student not in this course")
                }
        }
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Leave]> {
        return Leave.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<Leave> {
        return try req.parameters.next(Leave.self)
    }
    
    func getCourseHandler(_ req: Request) throws -> Future<Course> {
        return try req
            .parameters.next(Leave.self)
            .flatMap(to: Course.self) { leave in
                leave.course.get(on: req)
        }
    }
    
    func getStudentHandler(_ req: Request) throws -> Future<Student> {
        return try req
            .parameters.next(Leave.self)
            .flatMap(to: Student.self) { leave in
                leave.student.get(on: req)
        }
    }
    
    // MARK: LEAVE for back-end
    func getLeaveView1Handler(_ req: Request) throws -> Future<View> {
        let couID = try req.parameters.next(UUID.self)
        var leaveAll:[LeaveOneStruct] = []
        return Leave.query(on: req)
            
            .filter(\Leave.courseID==couID)
            .join(\Student.id, to: \Leave.studentID)
            .join(\Course.id, to: \Leave.courseID)
            .alsoDecode(Student.self)
            .alsoDecode(Course.self)
            .all()
            .flatMap(to: View.self){ leaves in
                var leaveOne: LeaveOneStruct?
                var dateSet: Set<Date> = []
                for leave in leaves {
                    if leave.0.0.id != nil {
                        leaveOne = LeaveOneStruct(courseName: leave.1.courseName,
                                                  courseID: leave.0.0.courseID,
                                                  leaveDate: leave.0.0.leaveDate,
                                                  nametest: leave.0.1.studentName,
                                                  studentID: leave.0.1.studentID,
                                                  leaveReason: leave.0.0.leaveReason,
                                                  detailReason: leave.0.0.detailReason,
                                                  leaveID: leave.0.0.id!)
                        
                       
                        dateSet.insert((leaveOne?.leaveDate)!)
                    }
                
                    if leaveOne != nil {
                        
                        leaveAll.append(leaveOne!)
//                        print(dateSet)
//                        print(leaveAll)
                    }
                    
                    leaveAll = leaveAll.sorted(by: {$0.leaveDate.compare($1.leaveDate) == .orderedDescending})
//                    for leaveOne in leaveAll {
//                        print("Sorted Date: \(leaveOne.leaveDate)")
//                    }
                  
//                print(leaves)
//                print(leaveAll)
//                print(leaveOne)
                
                }
                let context = LeaveAllStruct(title: "title", oneAll: leaveAll, setDate: dateSet)
//                print(dateSet.count)
//                let context = LeaveAllStruct(title: "title", oneAll: leaveAll, setDate: dateSet)
                return try req.view().render("viewLeave", context)
            }
    }

  
    
    func handler(req: Request,source:String) -> Future<View> {
        let searchTerm:UUID = req.query[UUID.self, at: "term"]!
        return Leave.query(on: req).filter(\.courseID == searchTerm).all().flatMap({ leaves in

            return Course.query(on: req).filter(\.id == searchTerm).all().flatMap({ courses in

                let context = LeaveContext(title: "Leaves", leaves:leaves,courses:courses)
                return try req.view().render(source, context)
            })
        })

    }
    
    func leaveViewHandler(_ req: Request) throws -> Future<View> {
        return try! getLeaveView1Handler(req)
    }

}

struct LeaveOneStruct: Content, Codable {
    let courseName: String
    let courseID: UUID
    let leaveDate: Date
    let nametest: String
    let studentID: String
    let leaveReason: Int
    let detailReason: String
    let leaveID:UUID
}
struct LeaveAllStruct: Encodable {
    let title: String
    let oneAll: [LeaveOneStruct]
    let setDate:Set<Date>
}

struct LeaveContext: Encodable {
    let title: String
    var leaves: [Leave]?
    var courses: [Course]?

}

struct CourseDate: Content,Encodable {
//    let startDate: Date
//    let endDate: Date
    let courseDate: [Date]
}
