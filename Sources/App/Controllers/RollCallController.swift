import Vapor
import Fluent
import Crypto

struct RollCallController: RouteCollection {
    
    func boot(router: Router) throws {
        
        let webRoute = router
        webRoute.get("courseGetStudent", UUID.parameter, UUID.parameter, use: getStudentRollCallViewHandler)
        //courseGetStudent/courseID/studentID 抓取該學生於該堂課的點名
        
        webRoute.get("rollCallList", Course.parameter, use: rollCallListViewHandler)
        
        let rollcallRoute = router.grouped("api", "rollcall")
        
        rollcallRoute.post(RollCallAll.self, use: createHandler)
        rollcallRoute.get(use: getAllHandler)
        rollcallRoute.get(RollCall.parameter, use: getHandler)
        rollcallRoute.get(RollCall.parameter, "course", use: getCourseHandler)
        rollcallRoute.get(RollCall.parameter, "student", use: getStudentHandler)
        rollcallRoute.delete("deleterollcall",RollCall.parameter, use: deleteHandler)
    }
    
    func createHandler(
        _ req: Request,
        rollcall: RollCallAll
        ) throws -> Future<HTTPStatus> {
        var incourseCheck: Bool = false
        
            return InCoursePivot.query(on: req)
                .filter(\InCoursePivot.courseID==rollcall.courseID)
                .filter(\InCoursePivot.studentID==rollcall.studentID)
//                .filter(\InCoursePivot.verifyString==rollcall.scanString)
                .first()
                .flatMap(to: HTTPStatus.self) {
                    checkincourses in
                    //                let avoidincourse = InCoursePivot.query(on: req)
                    //                .all()
//                    for checkincourse in checkincourses {
                    if checkincourses == nil {
                        incourseCheck = false
                    }
                    else {
                        if rollcall.scanString == checkincourses!.verifyString {
                            incourseCheck = true
                            //                                    throw Abort(.ok)
                        }
                        else {
                            throw Abort(.custom(code: 605, reasonPhrase: "verify string not equal."))
                        }
                        
                    }
                    
                    
            if incourseCheck {
                return RollCall.query(on: req).all().map(to: HTTPStatus.self) { allrollcalls in
                    
                    for onerollcall in allrollcalls {
                        if rollcall.courseID == onerollcall.courseID {
                            if rollcall.studentID == onerollcall.studentID {
                                let newDateFormatter: DateFormatter = DateFormatter()
                                newDateFormatter.dateFormat = "yyyy/MM/dd"
                                let oldDateFormatter: DateFormatter = DateFormatter()
                                oldDateFormatter.dateFormat = "yyyy/MM/dd"
                                let newDateString: String = newDateFormatter.string(from: rollcall.rollCallDate)
                                let oldDateString: String = oldDateFormatter.string(from: onerollcall.rollCallDate)
                                if newDateString == oldDateString {
                                    throw Abort(.custom(code: 601, reasonPhrase: "Already added a roll call for this day."))
                                }
                            }
                        }
                    }
                    let returnRollCall:RollCall = RollCall(rollCallDate: Date(), courseID: rollcall.courseID, studentID: rollcall.studentID)
                    _ = returnRollCall.save(on: req)
                    return HTTPStatus.ok
                }
            }
            else{
                throw Abort(.custom(code: 600, reasonPhrase: "Cannot add leave request because this student not in this course."))
            }
        }
    }
        
    func getAllHandler(_ req: Request) throws -> Future<[RollCall]> {
        return RollCall.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<RollCall> {
        return try req.parameters.next(RollCall.self)
    }
    
    func getCourseHandler(_ req: Request) throws -> Future<Course> {
        return try req
            .parameters.next(RollCall.self)
            .flatMap(to: Course.self) { rollcall in
                rollcall.course.get(on: req)
        }
    }
    
    func getStudentHandler(_ req: Request) throws -> Future<Student> {
        return try req
            .parameters.next(RollCall.self)
            .flatMap(to: Student.self) { rollcall in
                rollcall.student.get(on: req)
        }
    }
    
    func getStudentRollCallViewHandler(
        _ req: Request
        ) throws -> Future<View> {
        let courseID = try req.parameters.next(UUID.self)
        let studentID = try req.parameters.next(UUID.self)
        return RollCall.query(on: req)
            .filter(\RollCall.courseID==courseID)
            .filter(\RollCall.studentID==studentID)
            .all().flatMap({rollcall in
                let context = rollCallIndexContext(title: "Rollcall", rollcall: rollcall.isEmpty ? nil : rollcall)
                return try req.view().render("studentRollCall", context)})
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(RollCall.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    func rollCallListViewHandler(
        _ req: Request
        ) throws -> Future<View> {
        var courseDate:[Date] = []
        var studentsID:[UUID?] = []
        var studentsName:[String] = []
        var students:[Student] = []
        var studentsInfo:[StudentInfo] = []
        var initAttendList:[String] = []
        var courseDateString:[String] = []
        
        return try req.parameters.next(Course.self).flatMap(to: View.self){
            course in
//            courseDate = course.courseDate
            for (index,date) in course.courseDate.enumerated() {
                if (index % 2)==0 {
                    courseDate.append(date)
                    let addDate = Calendar.current.date(byAdding: .hour, value: +8, to: date) ?? date
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy/MM/dd"
                    let getDate = dateFormatter.string(from: addDate)
                    courseDateString.append(getDate)
                    if date < Date() {
                        initAttendList.append("曠課")
                    }
                    else {
                        initAttendList.append("")
                    }
                }
            }
            return InCoursePivot.query(on: req).filter(\InCoursePivot.courseID == course.id!).join(\Student.id, to: \InCoursePivot.studentID).alsoDecode(Student.self).all().flatMap(to: View.self){
                pivots in
                for pivot in pivots {
                    studentsID.append(pivot.1.id)
                    studentsName.append(pivot.1.studentName)
                    var studentInfo = StudentInfo(studentName: pivot.1.studentName, attendList: initAttendList)
                    studentsInfo.append(studentInfo)
                    students.append(pivot.1)
                    //                    leaveList.append(leaves)
//                    rollcallList.append(rollcalls)
//                    let newAttend = self.getPivotHandler(req,courseDate: courseDate, getLeaves: leaves, attendList: studentInfo.attendList)
//                    _ = try pivot.1.leave.query(on: req).filter(\Leave.courseID == course.id!).all().map(to: Int.self){
//                        leaves in
//                        for leave in leaves {
//                            for (index, oneDate) in courseDate.enumerated() {
//                                if Calendar.current.isDate(leave.leaveDate, inSameDayAs: oneDate) {
//                                    switch(leave.leaveReason) {
//                                    case 0: studentInfo.attendList[index] = "病假"
//                                    case 1: studentInfo.attendList[index] = "事假"
//                                    case 2: studentInfo.attendList[index] = "公假"
//                                    default: break
//                                    }
//                                }
//                            }
//                        }
//                        for (index, info) in studentsInfo.enumerated() {
//                            if info.studentName == studentInfo.studentName {
//                                studentsInfo[index] = studentInfo
//                            }
//                        }
//                        return 1
//                    }
//                    _ = try pivot.1.rollCall.query(on: req).filter(\RollCall.courseID == course.id!).all().map(to: Int.self){
//                        rollcalls in
//                        for rollcall in rollcalls {
//                            for (index, oneDate) in courseDate.enumerated() {
//                                if Calendar.current.isDate(rollcall.rollCallDate, inSameDayAs: oneDate) {
//                                    studentInfo.attendList[index] = "已點名"
//                                }
//                            }
//                            for (index, info) in studentsInfo.enumerated() {
//                                if info.studentName == studentInfo.studentName {
//                                    studentsInfo[index] = studentInfo
//                                }
//                            }
//                        }
//                        return 1
//                    }
                }
                print(studentsInfo)
                print(students)
                return try self.getPivotHandler(req, courseID: course.id!,courseDate: courseDate, students: students, attendList: initAttendList, courseDateString: courseDateString)
//                return try req.view().render("rollCallList")
            }
        }
    }
    
    func getPivotHandler(_ req: Request, courseID: UUID, courseDate: [Date], students: [Student], attendList: [String], courseDateString: [String]) throws -> Future<View> {
        var studentsInfo:[StudentInfo] = []
        for stu in students {
            let stuInfo = StudentInfo(studentName: stu.studentName, attendList: attendList)
            studentsInfo.append(stuInfo)
        }
        _ = Leave.query(on: req).filter(\Leave.courseID == courseID).sort(\Leave.leaveDate, ._ascending).all().map(to: Int.self) {
            leaves in
            for leave in leaves {
                for (index, oneDate) in courseDate.enumerated() {
                    if Calendar.current.isDate(leave.leaveDate, inSameDayAs: oneDate) {
                        for stu in students {
                            if leave.studentID == stu.id {
                                for (indexInfo, info) in studentsInfo.enumerated() {
                                    if info.studentName == stu.studentName {
                                        switch(leave.leaveReason) {
                                            case 0: studentsInfo[indexInfo].attendList[index] = "病假"
                                            case 1: studentsInfo[indexInfo].attendList[index] = "事假"
                                            case 2: studentsInfo[indexInfo].attendList[index] = "公假"
                                            case 3: studentsInfo[indexInfo].attendList[index] = "請假"
                                            default: break
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return 1
        }
        return RollCall.query(on: req).filter(\RollCall.courseID == courseID).sort(\RollCall.rollCallDate, ._ascending).all().flatMap(to: View.self) {
            rollcalls in
            for rollcall in rollcalls {
                for (index, oneDate) in courseDate.enumerated() {
                    if Calendar.current.isDate(rollcall.rollCallDate, inSameDayAs: oneDate) {
                        for stu in students {
                            if rollcall.studentID == stu.id {
                                for (indexInfo, info) in studentsInfo.enumerated() {
                                    if info.studentName == stu.studentName {
                                       studentsInfo[indexInfo].attendList[index] = "已點名"
                                    }
                                }
                            }
                        }
                    }
                }
            }
            let rollcalllist = RollCallList(courseDate: courseDateString, studentsInfo: studentsInfo)
            return try req.view().render("rollCallList", rollcalllist)
        }
    }
}
struct rollCallIndexContext: Encodable {
    let title: String
    let rollcall: [RollCall]?
}

struct RollCallAll: Encodable,Content {
    var id: UUID?
    var scanString: String
    var rollCallDate: Date = Date()
    var courseID: Course.ID
    var studentID: Student.ID
}

struct RollCallList: Content, Encodable {
    var courseDate: [String]
    var studentsInfo: [StudentInfo]
}
struct StudentInfo: Content, Encodable {
    var studentName: String
    var attendList: [String]
}
