import Vapor
import Fluent
import FluentPostgreSQL
import CryptoSwift

struct StudentPivotController: RouteCollection {
    
    func boot(router: Router) throws {
        let studentRoute = router.grouped("api", "student")
        studentRoute.post(GetCourseWeek.self, at: Student.parameter,"voteCourses",RecommendCourse.parameter,use: addVoteCoursesHandler)
        studentRoute.put(GetCourseWeek.self, at: Student.parameter,"voteCourses",RecommendCourse.parameter,use: putVoteCoursesHandler)
        studentRoute.get(Student.parameter,"voteCourses",use: getVoteCoursesHandler)
        studentRoute.delete(Student.parameter,"voteCourses",RecommendCourse.parameter, use: deleteVoteCoursesHandler)
        
        studentRoute.post(Student.parameter,"inCourses",Course.parameter,use: addInCoursesHandler)
        studentRoute.get(Student.parameter,"inCourses",use: getInCoursesHandler)
        studentRoute.delete(Student.parameter,"inCourses",Course.parameter,use: deleteInCoursesHandler)
        
        studentRoute.post(Student.parameter,"applyCourses",Course.parameter,use: addApplyCoursesHandler)
        studentRoute.get(Student.parameter,"applyCourses",use: getApplyCoursesHandler)
        studentRoute.put("applyCourses",ApplyPivot.parameter,use: changeApplyCoursesHandler)
        studentRoute.delete(Student.parameter,"applyCourses",Course.parameter, use: deleteApplyCoursesHandler)
        
        studentRoute.post(Student.parameter,"favoriteCourses",Course.parameter,use: addFavoriteCoursesHandler)
        studentRoute.get(Student.parameter,"favoriteCourses",use: getFavoriteCoursesHandler)
        studentRoute.delete(Student.parameter,"favoriteCourses",Course.parameter,use: deleteFavoriteCoursesHandler)
        
        studentRoute.post(Student.parameter,"favoriteRecCourses",RecommendCourse.parameter,use: addFavRecCoursesHandler)
        studentRoute.get(Student.parameter,"favoriteRecCourses",use: getFavRecCoursesHandler)
        studentRoute.delete(Student.parameter,"favoriteRecCourses",RecommendCourse.parameter,use: deleteFavRecCoursesHandler)
    }
    
    // MARK: Vote for this course and avoid repeat
    func addVoteCoursesHandler(
        _ req: Request,
        courseWeek: GetCourseWeek
        ) throws -> Future<StructNumbers> {
        return try flatMap(
            to:StructNumbers.self,
            req.parameters.next(Student.self),
            req.parameters.next(RecommendCourse.self)) { student, recommendcourse in
                return VotePivot.query(on: req)
                    .filter(\VotePivot.studentID==student.id!)
                    .filter(\VotePivot.recommendcourseID==recommendcourse.id!)
                    .all()
                    .map(to: StructNumbers.self) { avoidagains in
                        var voteCount = 0
                        for avoidagain in avoidagains {
                            if recommendcourse.id == avoidagain.recommendcourseID {
                                if student.id == avoidagain.studentID {
                                    voteCount = -1
                                    throw Abort(.custom(code: 601, reasonPhrase: "Already voted this course."))
                                }
                            }
                        }
                        if voteCount == 0{
                            recommendcourse.numberOfVoters = recommendcourse.numberOfVoters+1
                            let _ = recommendcourse.save(on: req)
                        }
                        
                        let pivot = student.voteCourses
                            .attach(recommendcourse, on: req)
                            .map(to: VotePivot.self){
                                votepivot in
                                votepivot.courseWeek = courseWeek.courseWeek
                                return votepivot
                        }
                        let _ = pivot.save(on: req)
                        return StructNumbers(numbers: recommendcourse.numberOfVoters)
                }
        }
    }
    func putVoteCoursesHandler(
        _ req: Request,
        courseWeek: GetCourseWeek
        ) throws -> Future<HTTPStatus> {
        return try flatMap(
            to:HTTPStatus.self,
            req.parameters.next(Student.self),
            req.parameters.next(RecommendCourse.self)) { student, recommendcourse in
                var success = 0
                return try student.voteCourses.pivots(on: req).all().flatMap(to: HTTPStatus.self){
                    pivots in
                    for pivot in pivots{
                        if pivot.recommendcourseID == recommendcourse.id{
                            pivot.courseWeek = courseWeek.courseWeek
                            success = 1
                            return pivot.save(on: req).transform(to: .noContent)
                        }
                    }
                    if success==0{
                        throw Abort(.custom(code: 611, reasonPhrase: "The student didn't vote the course."))
                    }
                    else{
                        throw Abort(.custom(code: 200, reasonPhrase: "Success"))
                    }
            }
        }
    }
    func getVoteCoursesHandler(
        _ req: Request
        ) throws -> Future<[RecommendCourse]> {
        
        return try req.parameters.next(Student.self)
            .flatMap(to: [RecommendCourse].self) { student in
                try student.voteCourses.query(on: req).all()
        }
    }
    
    func deleteVoteCoursesHandler(_ req: Request) throws -> Future<StructNumbers> {
        return try flatMap(
            to: StructNumbers.self,
            req.parameters.next(Student.self),
            req.parameters.next(RecommendCourse.self)
        ) { student, recommendcourse in
            return VotePivot.query(on: req)
                .filter(\VotePivot.studentID==student.id!)
                .filter(\VotePivot.recommendcourseID==recommendcourse.id!)
                .all()
                .map(to: StructNumbers.self) { avoidagains in
                    for avoidagain in avoidagains {
                        if recommendcourse.id == avoidagain.recommendcourseID {
                            if student.id == avoidagain.studentID {
                                recommendcourse.numberOfVoters = recommendcourse.numberOfVoters-1
                                let _ = recommendcourse.save(on: req)
                                let _ = student.voteCourses
                                    .detach(recommendcourse, on: req)
                                return StructNumbers(numbers: recommendcourse.numberOfVoters)
                            }
                        }
                    }
                    throw Abort(.custom(code: 617, reasonPhrase: "Didn't vote the course."))
            }
        }
    }
    
    // MARK: Add this student to this course and avoid repeat
    func addInCoursesHandler(
        _ req: Request
        ) throws -> Future<HTTPStatus> {
        return try flatMap(
            to:HTTPStatus.self,
            req.parameters.next(Student.self),
            req.parameters.next(Course.self)) { student, course in
                return InCoursePivot.query(on: req)
                    .filter(\InCoursePivot.studentID==student.id!)
                    .filter(\InCoursePivot.courseID==course.id!)
                    .all()
                    .flatMap(to: HTTPStatus.self) { avoidagains in
                        for avoidagain in avoidagains {
                            if course.id == avoidagain.courseID {
                                if student.id == avoidagain.studentID {
                                    throw Abort(.custom(code: 601, reasonPhrase: "Already in this course."))
                                }
                            }
                        }
                        _ = course.save(on: req)
                        return student.inCourses
                            .attach(course, on: req)
                            .transform(to: .created)
                }
        }
    }
    
    
    func getInCoursesHandler(
        _ req: Request
        ) throws -> Future<[Course]> {
        return try req.parameters.next(Student.self)
            .flatMap(to: [Course].self) { student in
                
                try student.inCourses.query(on: req)
                    .filter(\Course.courseState == 1)
                    .all()
        }
    }
    
    func deleteInCoursesHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(Student.self),
            req.parameters.next(Course.self)
        ) { student, course in
            return student.inCourses
                .detach(course, on: req)
                .transform(to: .noContent)
        }
    }
    
    // MARK: Apply for this course and avoid repeat
    func addApplyCoursesHandler(
        _ req: Request
        ) throws -> Future<StructApplyNumbers> {
        return try flatMap(
            to:StructApplyNumbers.self,
            req.parameters.next(Student.self),
            req.parameters.next(Course.self)) { student, course in
                var applyNumber = 0
                _ = ApplyPivot.query(on: req).filter(\ApplyPivot.courseID==course.id!).all().map(to: Int.self){
                    pivots in
                    for _ in pivots{
                        applyNumber+=1
                    }
                    return applyNumber
                }
                return ApplyPivot.query(on: req)
                    .filter(\ApplyPivot.studentID==student.id!)
                    .filter(\ApplyPivot.courseID==course.id!)
                    .all()
                    .map(to: StructApplyNumbers.self) { avoidagains in
                        for avoidagain in avoidagains {
                            if course.id == avoidagain.courseID {
                                if student.id == avoidagain.studentID {
                                    throw Abort(.custom(code: 601, reasonPhrase: "Already applied this course."))
                                }
                            }
                        }
                        if Date()>course.firstApplyStartTime && Date()<course.firstApplyEndTime{
                            
                            if course.firstNumberOfStudents>applyNumber{
                                _ = student.applyCourses
                                    .attach(course, on: req)
                                    .map(to: ApplyPivot.self){
                                        applypivot in
                                        applypivot.applyResult = 0
                                        course.numberOfApply=applyNumber+1
                                        let _ = course.save(on: req)
                                        let _ = applypivot.save(on: req)
                                        return applypivot
                                }
                                
                                return StructApplyNumbers(state: 0, applyNumbers: applyNumber+1)
                            }
                            else{
                                throw Abort(.custom(code: 606, reasonPhrase: "The apply students are too much."))
                            }
                        }
                        else if Date()>course.secondApplyStartTime && Date()<course.secondApplyEndTime{
                            if course.numberOfApply-course.firstNumberOfStudents >= course.secondNumberOfStudents{
                                _ = student.applyCourses
                                    .attach(course, on: req)
                                    .map(to: ApplyPivot.self){
                                        applypivot in
                                        applypivot.applyResult = 1
                                        course.numberOfApply+=1
                                        let _ = course.save(on: req)
                                        let _ = applypivot.save(on: req)
                                        return applypivot
                                }
                                let _ = course.save(on: req)
                                return StructApplyNumbers(state: 1, applyNumbers: applyNumber+1)
                            }
                            else{
                                _ = student.applyCourses
                                    .attach(course, on: req)
                                    .map(to: ApplyPivot.self){
                                        applypivot in
                                        applypivot.applyResult = 0
                                        course.numberOfApply+=1
                                        let _ = course.save(on: req)
                                        let _ = applypivot.save(on: req)
                                        return applypivot
                                }
                                let _ = course.save(on: req)
                                return StructApplyNumbers(state: 0, applyNumbers: applyNumber+1)
                            }
                        }
                        else{
                            throw Abort(.custom(code: 607, reasonPhrase: "Apply is over."))
                        }
                }
        }
    }
    
    func changeApplyCoursesHandler(_ req:Request)throws -> Future<ApplyPivot>{
        return try flatMap(to: ApplyPivot.self,req.parameters.next(ApplyPivot.self),req.content.decode(ApplyPivot.self)) {
            apply, updatedApply in
            apply.applyResult = updatedApply.applyResult
            
            return apply.save(on: req)
        }
    }
    
    func getApplyCoursesHandler(
        _ req: Request
        ) throws -> Future<[Course]> {
        
        return try req.parameters.next(Student.self)
            .flatMap(to: [Course].self) { student in
                try student.applyCourses.query(on: req).all()
        }
    }
    
    func deleteApplyCoursesHandler(_ req: Request) throws -> Future<StructNumbers> {
        var isApplyCourse = false
        return try flatMap(
            to: StructNumbers.self,
            req.parameters.next(Student.self),
            req.parameters.next(Course.self)
        ) { student, course in
            if Date()>course.firstApplyStartTime && Date()<course.firstApplyEndTime{
                return ApplyPivot.query(on: req).filter(\ApplyPivot.courseID==course.id!).sort(\ApplyPivot.applyTime).all().map(to: StructNumbers.self){
                    applys in
                    for apply in applys{
                        if apply.studentID == student.id{
                            isApplyCourse = true
                            course.numberOfApply-=1
                            let _ = course.save(on: req)
                            let _ = student.applyCourses
                                .detach(course, on: req)
                        }
                    }
                    if isApplyCourse {
                        return StructNumbers(numbers: course.numberOfApply)
                    }
                    else {
                        throw Abort(.custom(code: 617, reasonPhrase: "Didn't apply this course."))
                    }
                }
            }
            else if Date()>course.secondApplyStartTime && Date()<course.secondApplyEndTime{
                return ApplyPivot.query(on: req).filter(\ApplyPivot.courseID==course.id!).sort(\ApplyPivot.applyTime).all().map(to: StructNumbers.self){
                    applys in
                    for apply in applys{
                        if apply.studentID == student.id{
                            isApplyCourse = true
                            if apply.applyResult==0{
                                _ = ApplyPivot.query(on: req).filter(\ApplyPivot.courseID==course.id!).filter(\ApplyPivot.applyResult==1).sort(\ApplyPivot.applyTime).first().map(to: StructNumbers.self){oneApply in
                                    if oneApply != nil{
                                        oneApply?.applyResult = 0
                                        _ = oneApply?.save(on: req)
                                    }
//                                    return 0
                                    return StructNumbers(numbers: course.numberOfApply)
                                }
                            }
                            course.numberOfApply-=1
                            let _ = course.save(on: req)
                            let _ = student.applyCourses
                                .detach(course, on: req)
                           
                        }
                    }
                    if isApplyCourse {
                        return StructNumbers(numbers: course.numberOfApply)
                    }
                    else {
                        throw Abort(.custom(code: 617, reasonPhrase: "Didn't apply this course."))
                    }
                }
            }
            else{
                throw Abort(.custom(code: 607, reasonPhrase: "Apply is over."))
            }
        }
    }
    
    // MARK: Add this favorite course and avoid repeat
    func addFavoriteCoursesHandler(
        _ req: Request
        ) throws -> Future<HTTPStatus> {
        return try flatMap(
            to:HTTPStatus.self,
            req.parameters.next(Student.self),
            req.parameters.next(Course.self)) { student, course in
                return FavoritePivot.query(on: req)
                    .filter(\FavoritePivot.studentID==student.id!)
                    .filter(\FavoritePivot.courseID==course.id!)
                    .all()
                    .flatMap(to: HTTPStatus.self) { avoidagains in
                        for avoidagain in avoidagains {
                            if course.id == avoidagain.courseID {
                                if student.id == avoidagain.studentID {
                                    throw Abort(.custom(code: 601, reasonPhrase: "Already added this favorite course."))
                                }
                            }
                        }
                        return student.favoriteCourses
                            .attach(course, on: req)
                            .transform(to: .created)
                }
        }
    }
    
    func getFavoriteCoursesHandler(
        _ req: Request
        ) throws -> Future<[Course]> {
        
        return try req.parameters.next(Student.self)
            .flatMap(to: [Course].self) { student in
                try student.favoriteCourses.query(on: req).all()
        }
    }
    
    func deleteFavoriteCoursesHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(Student.self),
            req.parameters.next(Course.self)
        ) { student, course in
            return FavoritePivot.query(on: req)
                .filter(\FavoritePivot.studentID==student.id!)
                .filter(\FavoritePivot.courseID==course.id!)
                .first()
                .flatMap(to: HTTPStatus.self) { avoidagains in

                    if avoidagains != nil {
                        
                                return student.favoriteCourses
                                    .detach(course, on: req)
                                    .transform(to: .created)
                    }
                    else {
                        throw Abort(.custom(code: 617, reasonPhrase: "Didn't add this course in favorite list."))
                    }
            }
        }
    }
    
    // MARK: Add this favorite recommend course and avoid repeat
    func addFavRecCoursesHandler(
        _ req: Request
        ) throws -> Future<HTTPStatus> {
        return try flatMap(
            to:HTTPStatus.self,
            req.parameters.next(Student.self),
            req.parameters.next(RecommendCourse.self)) { student, recommendcourse in
                return FavRecPivot.query(on: req)
                    .filter(\FavRecPivot.studentID==student.id!)
                    .filter(\FavRecPivot.recommendcourseID==recommendcourse.id!)
                    .all()
                    .flatMap(to: HTTPStatus.self) { avoidagains in
                        for avoidagain in avoidagains {
                            if recommendcourse.id == avoidagain.recommendcourseID {
                                if student.id == avoidagain.studentID {
                                    throw Abort(.custom(code: 601, reasonPhrase: "Already added this favorite recommend course."))
                                }
                            }
                        }
                        return student.favRecCourses
                            .attach(recommendcourse, on: req)
                            .transform(to: .created)
                }
        }
    }
    
    func getFavRecCoursesHandler(
        _ req: Request
        ) throws -> Future<[RecommendCourse]> {
        
        return try req.parameters.next(Student.self)
            .flatMap(to: [RecommendCourse].self) { student in
                try student.favRecCourses.query(on: req).all()
        }
    }
    
    func deleteFavRecCoursesHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(Student.self),
            req.parameters.next(RecommendCourse.self)
        ) { student, recommendcourse in
            return FavRecPivot.query(on: req)
                .filter(\FavRecPivot.studentID==student.id!)
                .filter(\FavRecPivot.recommendcourseID==recommendcourse.id!)
                .first()
                .flatMap(to: HTTPStatus.self) { avoidagains in
                    
                    if avoidagains != nil {
                        
                        return student.favRecCourses
                            .detach(recommendcourse, on: req)
                            .transform(to: .created)
                    }
                    else {
                        throw Abort(.custom(code: 617, reasonPhrase: "Didn't add this course in favorite list."))
                    }
                    
                    
            }
//            return student.favRecCourses
//                .detach(recommendcourse, on: req)
//                .transform(to: .noContent)
        }
    }
}
struct Result: Content {
    let result: String
}
struct GetCourseWeek: Content, Encodable {
    let courseWeek:[Int]
}

struct StructNumbers: Content, Encodable {
    let numbers: Int
}

struct StructApplyNumbers: Content, Encodable {
    let state: Int //0:正取1:備取
    let applyNumbers: Int
}
