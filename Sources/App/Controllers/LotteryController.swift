import Vapor
import Fluent

struct LotteryController: RouteCollection {
    
    func boot(router: Router) throws {
        
        let lotteryRoute = router.grouped("api", "lottery")
        lotteryRoute.post(Lottery.self, use: createHandler)
        lotteryRoute.get(use: getAllHandler)
        lotteryRoute.get(Lottery.parameter, use: getHandler)
        lotteryRoute.delete("deleteLottery",Lottery.parameter, use: deleteHandler)
        lotteryRoute.get("availableLottery", Student.parameter, use: getIsAvailableHandler)
        lotteryRoute.get("isUsedLottery", Student.parameter, use: getIsUsedHandler)
        lotteryRoute.put("changeToUsed", Lottery.parameter, use: changeToUsedHandler)
        lotteryRoute.post("changePoint", Student.parameter, use: createWithPointHandler)
//        lotteryRoute.post("changePoint2", Student.parameter, use: createWithPoint1Handler)
//        lotteryRoute.post("changePoint3", Student.parameter, use: createWithPoint2Handler)
    }
    
    func createWithPointHandler(
        _ req: Request
        ) throws -> Future<TotalPointsLottery> {
        return try req.parameters.next(Student.self)
            .flatMap(to: TotalPointsLottery.self) { student in
            return Point.query(on: req).filter(\Point.studentID == student.id!).filter(\Point.getPoint != -1).all().flatMap(to: TotalPointsLottery.self){
                points in
                var totalPoint = 0
                var totalLottery = 0
                for point in points{
                    totalPoint += point.getPoint
                }
                if totalPoint >= 100{
                    let newPoint = Point(pointReason: "兌換抽獎券", getPoint: -100, getPointDate: Date(), studentID: student.id!)
                    return newPoint.save(on: req).flatMap(to: TotalPointsLottery.self){
                        reqNewPoint in
                        let newLottery = Lottery(getLotteryDate: Date(), isUsedDate: Date(), isUsed: false, pointID: reqNewPoint.id!, studentID: student.id!)
                        _ = newLottery.save(on: req)
                        return Lottery.query(on: req).filter(\Lottery.studentID == student.id!).filter(\Lottery.isUsed==false).all().map(to: TotalPointsLottery.self){
                            allLottery in
                            for _ in allLottery{
                                totalLottery += 1
                            }
                            totalPoint -= 100
                            return TotalPointsLottery(totalPoints: totalPoint, totalLottery: totalLottery)
                        }
                    }
                }
                else{
                    throw Abort(.custom(code: 613, reasonPhrase: "Not enough points."))
                }
            }
        }
    }
    
//    func createWithPoint1Handler(
//        _ req: Request
//        ) throws -> Future<TotalPointsLottery> {
//        return try req.parameters.next(Student.self)
//            .flatMap(to: TotalPointsLottery.self) { student in
//                return Point.query(on: req).filter(\Point.studentID == student.id!).filter(\Point.getPoint != -1).all().flatMap(to: TotalPointsLottery.self){
//                    points in
//                    var totalPoint = 0
//                    var totalLottery = 0
//                    for point in points{
//                        totalPoint += point.getPoint
//                    }
//                    if totalPoint >= 300{
//                        let newPoint = Point(pointReason: "兌換二級抽獎券", getPoint: -300, getPointDate: Date(), studentID: student.id!)
//                        return newPoint.save(on: req).flatMap(to: TotalPointsLottery.self){
//                            reqNewPoint in
//                            let newLottery = Lottery(getLotteryDate: Date(), isUsedDate: Date(), isUsed: false, pointID: reqNewPoint.id!, studentID: student.id!, type: 1)
//                            _ = newLottery.save(on: req)
//                            return Lottery.query(on: req).filter(\Lottery.studentID == student.id!).filter(\Lottery.isUsed==false).all().map(to: TotalPointsLottery.self){
//                                allLottery in
//                                for _ in allLottery{
//                                    totalLottery += 1
//                                }
//                                totalPoint -= 300
//                                return TotalPointsLottery(totalPoints: totalPoint, totalLottery: totalLottery)
//                            }
//                        }
//                    }
//                    else{
//                        throw Abort(.custom(code: 613, reasonPhrase: "Not enough points."))
//                    }
//                }
//        }
//    }
//
//    func createWithPoint2Handler(
//        _ req: Request
//        ) throws -> Future<TotalPointsLottery> {
//        return try req.parameters.next(Student.self)
//            .flatMap(to: TotalPointsLottery.self) { student in
//                return Point.query(on: req).filter(\Point.studentID == student.id!).filter(\Point.getPoint != -1).all().flatMap(to: TotalPointsLottery.self){
//                    points in
//                    var totalPoint = 0
//                    var totalLottery = 0
//                    for point in points{
//                        totalPoint += point.getPoint
//                    }
//                    if totalPoint >= 500{
//                        let newPoint = Point(pointReason: "兌換特級抽獎券", getPoint: -500, getPointDate: Date(), studentID: student.id!)
//                        return newPoint.save(on: req).flatMap(to: TotalPointsLottery.self){
//                            reqNewPoint in
//                            let newLottery = Lottery(getLotteryDate: Date(), isUsedDate: Date(), isUsed: false, pointID: reqNewPoint.id!, studentID: student.id!, type: 2)
//                            _ = newLottery.save(on: req)
//                            return Lottery.query(on: req).filter(\Lottery.studentID == student.id!).filter(\Lottery.isUsed==false).all().map(to: TotalPointsLottery.self){
//                                allLottery in
//                                for _ in allLottery{
//                                    totalLottery += 1
//                                }
//                                totalPoint -= 500
//                                return TotalPointsLottery(totalPoints: totalPoint, totalLottery: totalLottery)
//                            }
//                        }
//                    }
//                    else{
//                        throw Abort(.custom(code: 613, reasonPhrase: "Not enough points."))
//                    }
//                }
//        }
//    }
    
    func createHandler(
        _ req: Request,
        lottery: Lottery
        ) throws -> Future<Lottery> {

        return Lottery.query(on: req)
            .filter(\Lottery.studentID == lottery.studentID)
            .filter(\Lottery.pointID == lottery.pointID)
            .all()
            .flatMap(to: Lottery.self) {
                checkpoints in
                var isSamePoint = false
                for _ in checkpoints {
                    
                        isSamePoint = true
                    
                }


                if isSamePoint {
                    throw Abort(.custom(code: 601, reasonPhrase: "Already used points to exchange a lottery."))
                }
                else {
                    lottery.getLotteryDate = Date()
                    return lottery.save(on: req)
                }
        }
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Lottery]> {
        return Lottery.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<Lottery> {
        return try req.parameters.next(Lottery.self)
    }
    

    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Lottery.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    
    func getIsAvailableHandler(_ req: Request)
        throws -> Future<[Lottery]> {
            return try req
                .parameters.next(Student.self)
                .flatMap(to: [Lottery].self) { student in
                    try student.lottery.query(on: req).filter(\.isUsed == false).sort(\.getLotteryDate, .descending).all()
            }
    }
    
    func getIsUsedHandler(_ req: Request)
        throws -> Future<[Lottery]> {
            return try req
                .parameters.next(Student.self)
                .flatMap(to: [Lottery].self) { student in
                    try student.lottery.query(on: req).filter(\.isUsed == true).sort(\.getLotteryDate, .descending).all()
            }
    }
    
    func changeToUsedHandler(_ req: Request) throws -> Future<Lottery> {
        return try req.parameters.next(Lottery.self).flatMap(to: Lottery.self){
            lottery in
            lottery.isUsed = true
            lottery.isUsedDate = Date()
            return lottery.save(on: req)
        }
    }
}

struct TotalPointsLottery: Content, Encodable {
    let totalPoints: Int
    let totalLottery: Int
}
