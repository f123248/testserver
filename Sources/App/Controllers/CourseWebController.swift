//
//  CourseWebController.swift
//  App
//
//  Created by hsu on 2019/4/16.
//
import Vapor
import Fluent
import Crypto

struct CourseWebController: RouteCollection {
    
    func boot(router: Router) throws {
        let webRoute = router
        webRoute.get("courseAvailable", use: courseAvailableViewHandler)
        webRoute.get("courseApplyStudents",UUID.parameter, use: courseApplyStudentsViewHandler)//CourseID
        webRoute.get("getApplyStudents",UUID.parameter, use: courseApplyStudentsHandler)//CourseID
        webRoute.put("courseStateChange",Course.parameter, use: courseStateChangeHandler)
        webRoute.get("courseEnd", use: courseEndViewHandler)
        webRoute.get("courseDuring", use: courseDuringViewHandler)
        webRoute.get("courseDetailEdit", Course.parameter, use: courseDetailEditView1Handler)
        webRoute.put("courseDetailEdit",Course.parameter, use: courseDetailUpdateHandler)
        webRoute.get("courseGetStudent", UUID.parameter, use: getInCourseStudentViewHandler)
        webRoute.put("blackList",Student.parameter, use: setupBlackListUpdateHanler)
        webRoute.get("blackList", use: getStudentBlackLastDateExpiredViewHandle)
        webRoute.get("blackListStudent", use:getBlackListStudentViewHandler)
        
        webRoute.get("newCourse", use: editNewCourseViewHandler)
        webRoute.post(Course.self, at: "newCourse", use: createNewCourseHandler)
        webRoute.get("reopenCourse", Course.parameter, use: reopenCourseViewHandler)
        webRoute.post(Course.self, at: "reopenCourse", Course.parameter, use: reopenCourseHandler)
        webRoute.get("privacy", use: privacyHandler)
        
        let courseRoute = router.grouped("api", "course")
        courseRoute.post(Course.self, at: RecommendCourse.parameter, use: createHandler)
        courseRoute.delete("deleteOpenPivot",OpenCoursePivot.parameter, use: deleteOpenPivotHandler)
    }
    func reopenCourseHandler(
        _ req: Request,
        course: Course
        ) throws -> Future<Course> {
        course.courseState = 0
        return course.save(on: req).map({
            oneCourse -> Course in
            _ = try req.parameters.next(Course.self).map(to: Int.self){
                oldCourse in
                _ = oldCourse.reopen(course: oneCourse, on: req)
                return 0
            }
            return course
        })
    }
    
    func createNewCourseHandler(
        _ req: Request,
        course: Course
        ) throws -> Future<Course> {
        return course.save(on: req).map({
            oneCourse -> Course in
            let timeInterval:TimeInterval = 0
            let date = Date(timeIntervalSince1970: timeInterval)
            _ = RecommendCourse(courseName: course.courseName, courseType: course.courseType, campus: course.campus, recommendReason: "教發既定課程", courseIntroduction: course.courseIntroduction, proposerID: UUID(), voteStartTime: date, voteEndTime: date, recommendTime: Date(), verifiedTime: Date(), verifyState: 6, verificationRejectedReason: "", numberOfVoters: 0).save(on: req).map({
                rcm in
                rcm.verifyState = 6
                let _ = rcm.save(on: req)
                _ = rcm.openCourse.attach(oneCourse, on: req)
            })
            
            return course
        })
    }
    func editNewCourseViewHandler(_ req: Request) throws -> Future<View> {
        return try req.view().render("newCourse")
    }
    
    func deleteOpenPivotHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(OpenCoursePivot.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    func courseDetailEditView1Handler(_ req: Request) throws -> Future<View> {
        return try req.view().render("courseDetailEdit")
    }
    func reopenCourseViewHandler(_ req: Request) throws -> Future<View> {
        let course = try req.parameters.next(Course.self)
        return try req.view().render("reopenCourse", course)
    }
    func createHandler(
        _ req: Request,
        course: Course
        ) throws -> Future<Course> {
        return course.save(on: req).map({
            oneCourse -> Course in
            _ = try req.parameters.next(RecommendCourse.self).map({
                rcm in
                rcm.verifyState = 6
                let _ = rcm.save(on: req)
                _ = rcm.openCourse.attach(oneCourse, on: req)
            })
            
            return course
            })
    }
    
    func getInCourseStudentViewHandler(
        _ req: Request
        ) throws -> Future<View> {
        let id = try req.parameters.next(UUID.self)
        return Student.query(on: req)
            .join(\InCoursePivot.studentID, to: \Student.id)
            .filter(\InCoursePivot.courseID==id)
            .all().flatMap({students in
                let context = studentIndexContext(title: "Students", students: students.isEmpty ? nil : students)
                return try req.view().render("courseGetStudent", context)})
    }
    
    func getBlackListStudentViewHandler(_ req: Request) throws -> Future<View> {
        return Student.query(on: req)
            .filter(\.studentBlackList == 1)
            .all().flatMap({students in
                let context = studentIndexContext(title: "Students", students: students.isEmpty ? nil : students)
                return try req.view().render("blackListStudent", context)})
    }
    
    func setupBlackListUpdateHanler(_ req:Request)throws -> Future<Student>{
        
        return try flatMap(to: Student.self,req.parameters.next(Student.self),req.content.decode(Student.self)) {
            
            student, updatedStudent in
            student.studentBlackList = updatedStudent.studentBlackList
            student.blackLastDate = updatedStudent.blackLastDate
            return student.save(on: req)
        }
    }
    
    func getStudentBlackLastDateExpiredViewHandle(
        _ req: Request
        ) throws -> Future<[Student]> {
        return Student.query(on: req).filter(\.studentBlackList == 1).filter(\.blackLastDate <= Date()).all()
    }
}
    
    // MARK: 報名的學生
    func courseApplyStudentsViewHandler(_ req: Request) throws -> Future<View> {
        let courseid = try req.parameters.next(UUID.self)
        var applyData:[applyStudents] = []
        return ApplyPivot.query(on: req)
            .filter(\ApplyPivot.courseID == courseid)
            .join(\Student.id, to: \ApplyPivot.studentID)
            .alsoDecode(Student.self)
            .sort(\ApplyPivot.applyTime)
            .all().flatMap({ applys in
                for apply in applys{
                    let stuUUID = apply.1.id
                    let stuName = apply.1.studentName
                    let stuID = apply.1.studentID
                    let stuPhone = apply.1.studentPhoneNumber
                    let applyTime = apply.0.applyTime
                    let applyState = apply.0.applyResult
                    
                    let context = applyStudents(studentUUID: stuUUID, studentName: stuName, studentID: stuID, studentPhone: stuPhone,applyTime: applyTime ,applyResult: applyState)
                    applyData.append(context)
                }
                let context = applyStudentsContext(title: "ApplyStudents", applys: applyData)
                return try req.view().render("courseApplyStudents", context)
            })
    }
    
    func courseApplyStudentsHandler(_ req: Request) throws -> Future<[ApplyPivot]> {
        let courseid = try req.parameters.next(UUID.self)
        return ApplyPivot.query(on: req)
            .filter(\ApplyPivot.courseID == courseid)
            .filter(\ApplyPivot.applyResult == 0)
            .all()
    }
    
    func courseStateChangeHandler(_ req: Request) throws -> Future<Course> {
        return try flatMap(to: Course.self,
                           req.parameters.next(Course.self),
                           req.content.decode(Course.self)) {
                            Course, updatedCourse in
                            Course.courseState = updatedCourse.courseState
                            return Course.save(on: req)
        }
    }
    func courseDetailUpdateHandler(_ req: Request) throws -> Future<Course> {
        return try flatMap(to: Course.self,
                           req.parameters.next(Course.self),
                           req.content.decode(Course.self)) {
                            Course, updatedCourse in
                            Course.courseDate = updatedCourse.courseDate
                            Course.courseTeacher = updatedCourse.courseTeacher
                            Course.coursePlace = updatedCourse.coursePlace
                            Course.courseCost = updatedCourse.courseCost
                            Course.targetStudent = updatedCourse.targetStudent
                            Course.numberOfApply = updatedCourse.numberOfApply
                            Course.firstNumberOfStudents = updatedCourse.firstNumberOfStudents
                            Course.firstApplyStartTime = updatedCourse.firstApplyStartTime
                            Course.firstApplyEndTime = updatedCourse.firstApplyEndTime
                            Course.secondNumberOfStudents = updatedCourse.secondNumberOfStudents
                            Course.secondApplyStartTime = updatedCourse.secondApplyStartTime
                            Course.secondApplyEndTime = updatedCourse.secondApplyEndTime
                            Course.applyMethod = updatedCourse.applyMethod
                            Course.enrollPost = updatedCourse.enrollPost
                            Course.courseIntroduction = updatedCourse.courseIntroduction
                            Course.other = updatedCourse.other
                            Course.courseContent = updatedCourse.courseContent
                            return Course.save(on: req)
        }
    }
    
    func courseDeclineViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "courseDecline")
    }
    
    func courseDuringViewHandler(_ req: Request) throws -> Future<View> {
        var allCourses:[Course] = []
        InCoursePivot.query(on: req).join(\Course.id, to: \InCoursePivot.courseID).alsoDecode(Course.self).sort(\Course.id).filter(\Course.courseState == 1).all().map(to: Int.self) {
            pivots in
            var lastCourseid:UUID?
            var studentCount = 0
            for pivot in pivots{
                studentCount += 1
                if lastCourseid == pivot.1.id{
                    allCourses.last?.numberOfApply = studentCount
                }
                else if lastCourseid != pivot.1.id{
                    studentCount = 1
                    allCourses.append(pivot.1)
                }
                if lastCourseid == nil || lastCourseid != pivot.1.id{
                    lastCourseid = pivot.1.id
                }
            }

            return 1
        }
        return Course.query(on: req).filter(\Course.courseState == 1).sort(\Course.id).all().flatMap({ courses in
            for course in courses{
                if !(allCourses.contains(where: { $0.id == course.id })) {
                    course.numberOfApply = 0
                    allCourses.append(course)
                }
            }
            let context = IndexContext(title: "Courses", courses: allCourses.isEmpty ? nil : allCourses)
            return try req.view().render("courseDuring", context)
        })
    }
    
    func courseEndViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "courseEnd")
    }

    func privacyHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "privacy")
    }
    
    func courseAvailableViewHandler(_ req: Request) throws -> Future<View> {
        return Course.query(on: req).filter(\Course.courseState == 0).sort(\Course.id).all().flatMap({ courses in
            var allCourses:[Course] = []
            var studentCount = 0
            for course in courses{
                allCourses.append(course)
                _ = ApplyPivot.query(on: req).filter(\ApplyPivot.courseID == course.id!).all().map({
                    applys -> Int in
                    for _ in applys {
                        studentCount += 1
                    }
                    allCourses.last?.numberOfApply = studentCount
                    return 0
                })
            }
            let context = IndexContext(title: "Courses", courses: allCourses.isEmpty ? nil : allCourses)
            return try req.view().render("courseAvailable", context)
        })
    }
    
    func handler(req: Request,source:String) -> Future<View> {
        return Course.query(on: req).all().flatMap({ courses in
            let context = IndexContext(title: "Courses", courses: courses.isEmpty ? nil : courses)
            return try req.view().render(source, context)
        })
    }


struct applyStudents: Encodable {
    let studentUUID: UUID?
    let studentName: String
    let studentID: String
    let studentPhone: String
    let applyTime: Date
    let applyResult: Int
}

struct IndexContext: Encodable {
    let title: String
    let courses: [Course]?
}

struct studentIndexContext: Encodable {
    let title: String
    let students: [Student]?
}

struct applyStudentsContext: Encodable {
    let title: String
    let applys: [applyStudents]?
}


