import Vapor
import Fluent

struct CourseEvaluationController: RouteCollection {
    
    func boot(router: Router) throws {
        
        let courseEvaluationRoute = router.grouped("api", "courseEvaluation")
        
        courseEvaluationRoute.post(CourseEvaluation.self, use: createHandler)
        
        courseEvaluationRoute.get(use: getAllHandler)
        courseEvaluationRoute.get(CourseEvaluation.parameter, use: getHandler)
        courseEvaluationRoute.get(CourseEvaluation.parameter, "course", use: getCourseHandler)
        courseEvaluationRoute.get(CourseEvaluation.parameter, "student", use: getStudentHandler)
        courseEvaluationRoute.put("putCourseEvaluationPublic", CourseEvaluation.parameter, use: putCourseEvaluationPublicHandler)
        
        let webRoute = router
        webRoute.get("courseEvaluationPublic", use: getCourseEvaluationPublicHandler)
        webRoute.get("courseEvaluationPrivate", use: getCourseEvaluationPrivateHandler)
        webRoute.put("putCourseEvaluationPublic", CourseEvaluation.parameter, use: putCourseEvaluationPublicHandler)
        
    }
    
    func createHandler(
        _ req: Request,
        courseEvaluation: CourseEvaluation
        ) throws -> Future<CourseEvaluation> {
        print("\(Date()) [POST]:api/courseEvaluation [FUNC]=createHandler")
        var incourseCheck: Bool = false
        let timeInterval:TimeInterval = 0
        let date1 = Date(timeIntervalSince1970: timeInterval)
        return InCoursePivot.query(on: req)
            .filter(\InCoursePivot.courseID==courseEvaluation.courseID)
            .filter(\InCoursePivot.studentID==courseEvaluation.studentID)
            .first()
            .flatMap(to: CourseEvaluation.self) {
                checkincourses in
                //                let avoidincourse = InCoursePivot.query(on: req)
                //                .all()
//                for checkincourse in checkincourses {
//                    if courseEvaluation.courseID == checkincourse.courseID {
                        if checkincourses == nil {
                            incourseCheck = false
                        }
                        else {
                            incourseCheck = true
                        }
//                    }
//                    else {
//                        incourseCheck = false
//                    }
                    
//                }
                if incourseCheck {
                    courseEvaluation.evaluationTime = Date()
                    courseEvaluation.teacherResponseTime = date1
                    courseEvaluation.teacherResponse = ""
                    let rtn = courseEvaluation.save(on: req)
                    print(rtn)
                    return rtn
                }
                else{
                    throw Abort(.custom(code: 600, reasonPhrase: "Cannot add evaluation request because this student is not in this course."))
                }
        }
    }
    
    
    func getAllHandler(_ req: Request) throws -> Future<[CourseEvaluation]> {
        return CourseEvaluation.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<CourseEvaluation> {
        return try req.parameters.next(CourseEvaluation.self)
    }
    
    func getCourseHandler(_ req: Request) throws -> Future<Course> {
        return try req
            .parameters.next(CourseEvaluation.self)
            .flatMap(to: Course.self) { courseEvaluation in
                courseEvaluation.course.get(on: req)
        }
    }
   
    func getStudentHandler(_ req: Request) throws -> Future<Student> {
        return try req
            .parameters.next(CourseEvaluation.self)
            .flatMap(to: Student.self) { courseEvaluation in
                courseEvaluation.student.get(on: req)
        }
    }
    
    func handler(req: Request, source: String) -> Future<View> {
            var courseArr:[Course]=[]
            var evaluation2: [WebEvaluationOneStruct] = []
            _ = Course.query(on: req).all().map(to: [Course].self) {
                courses in
                for course in courses{
                    let containBool = courseArr.contains(where: { (value) -> Bool in
                        value.id == course.id
                    })
                    if containBool == false{
                        courseArr.append(course)
                    }
                }
                return courseArr
            }
            return CourseEvaluation.query(on: req)
//                .filter(\CourseEvaluation.state == 0)
                .join(\Student.id , to: \CourseEvaluation.studentID)
                .join(\Course.id, to: \CourseEvaluation.courseID)
                .alsoDecode(Student.self)
                .alsoDecode(Course.self)
                .all()
                .flatMap(to: View.self) { evaluations in
                    var pushOne: WebEvaluationOneStruct?
                    for evaluation in evaluations {
                        if evaluation.0.0.id != nil {
                            
                        pushOne = WebEvaluationOneStruct(evaluationID: evaluation.0.0.id!,
                                                         state: evaluation.0.0.state,
                                                         studentContent: evaluation.0.0.studentContent,
                                                         evaluationTime: evaluation.0.0.evaluationTime,
                                                         teacherResponse: evaluation.0.0.teacherResponse,
                                                         teacherResponseTime: evaluation.0.0.teacherResponseTime,
                                                         courseID: evaluation.0.0.courseID,
                                                         studentID: evaluation.0.1.studentID,
                                                         studentName: evaluation.0.1.studentName,
                                                         courseName: evaluation.1.courseName)
                        }
                    
                        if pushOne != nil {
                            evaluation2.append(pushOne!)
                        }
                        
                    }
                    evaluation2 = evaluation2.sorted(by: {$0.evaluationTime.compare($1.evaluationTime) == .orderedDescending})
                    
            let context = WebEvaluationAllStruct(title: "title", courseAll: courseArr.isEmpty ? nil : courseArr, evaluationAll: evaluation2.isEmpty ? nil : evaluation2)
            print(evaluation2)
            return try req.view().render(source, context)
            }
    }
    
    func getCourseEvaluationPublicHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "courseEvaluationPublic")
    }
    
    func getCourseEvaluationPrivateHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "courseEvaluationPrivate")
    }
    
    func putCourseEvaluationPublicHandler(_ req: Request) throws -> Future<CourseEvaluation> {
        return try flatMap(to: CourseEvaluation.self,
                           req.parameters.next(CourseEvaluation.self),
                           req.content.decode(WebEvaluationOneStruct.self)) {
                            evaluation, updatedEvaluation in
                            evaluation.teacherResponse = updatedEvaluation.teacherResponse
                            evaluation.teacherResponseTime = Date()
                            return evaluation.save(on: req)
        }
    }
}


struct WebEvaluationOneStruct: Content {
    var evaluationID: UUID
    var state: Int
    var studentContent: String
    var evaluationTime: Date
    var teacherResponse: String
    var teacherResponseTime: Date
    var courseID: UUID
    var studentID: String
    var studentName: String
    var courseName: String
}

struct WebEvaluationAllStruct: Content {
    let title: String
    var courseAll:[Course]?
    var evaluationAll:[WebEvaluationOneStruct]?
}

