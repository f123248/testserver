import Vapor
import Fluent
import Crypto

struct CourseController: RouteCollection {
    
    func boot(router: Router) throws {
        let courseRoute = router.grouped("api", "course")
        courseRoute.get("getSelect", use: getReopenCourseHandler)
        courseRoute.get(use: getAllHandler)
        courseRoute.get(Course.parameter, use: getHandler)
        courseRoute.put("updatecourse",Course.parameter, use: updateHandler)
        courseRoute.delete("deletecourse",Course.parameter, use: deleteHandler)
        courseRoute.get(Course.parameter,"evaluationPrivate", Student.parameter,  use: getCourseEvaluationPrivateHandler)
        courseRoute.get("evaluationPublic",UUID.parameter,  use: getCourseEvaluationPublicHandler)
        courseRoute.get(Course.parameter, "leave", use: getLeaveHandler)
        courseRoute.get(Course.parameter, "rollcall", use: getRollCallHandler)
        courseRoute.get(Course.parameter, "satisfaction", use: getCourseSatisfactionHandler)
//        courseRoute.get("search", use: search)
        courseRoute.get(Course.parameter,"inCourseStudents", use: getInCourseStudentsHandler)
        courseRoute.get(Course.parameter,"applyStudents", use: getApplyStudentsHandler)
        
        courseRoute.put(Course.parameter,"rollCall",Student.parameter, use:putRollCallTestHandler)
        courseRoute.get(Course.parameter,"getverifystring",Student.parameter, use:getRollCallTestHandler)
        
        
        //此UUID皆為studentID
        //課程一覽
        //上課中
        courseRoute.get("getCourseDuring",Student.parameter,use: getCourseDuringHandler)
        
        //報名中
        courseRoute.get("getCourseApplying",Student.parameter,use: getCourseApplyingHandler)
        
        //投票中
        courseRoute.get("getCourseVoting",Student.parameter,use: getCourseVotingHandler)
        //全部
        courseRoute.get("getCourseAll",Student.parameter,use: getCourseAllHandler)
 
        //我的課程
        //我上過的課
        courseRoute.get("getMyCourses",Student.parameter,use: getMyCoursesHandler)
        //我推薦過的課
        courseRoute.get("getMyRecommendCourses",Student.parameter,use: getMyRecommendHandler)
    
        //我投票的課
        courseRoute.get("getMyVoteCourses",Student.parameter,use: getMyVoteCoursesHandler)
        
        //我的收藏
        courseRoute.get("getMyFavoriteCourses",Student.parameter,use: getMyFavoriteCoursesHandler)
        
        //我的課表
        courseRoute.get("getCourseTable",Student.parameter,String.parameter,use: getMyCourseTableHandler)
        courseRoute.get("getAllCourseTable",Student.parameter,use: getMyAllCourseTableHandler)
        
        
        courseRoute.get("getDate",RecommendCourse.parameter,use: testHandler)
    }
    
    func getReopenCourseHandler(_ req: Request) throws -> Future<[Course]> {
        var allCourse:[Course] = []
        return ReopenCoursePivot.query(on: req).join(\Course.id, to: \ReopenCoursePivot.oldCourseID).alsoDecode(Course.self).all().map(to: [Course].self){
            pivots in
            for pivot in pivots {
                var notContain = true
                for oneCourse in allCourse {
                    if oneCourse.courseName == pivot.1.courseName {
                        notContain = false
                    }
                }
                if notContain {
                    allCourse.append(pivot.1)
                }
            }
            return allCourse
        }
    }
    
    func getHandler(_ req: Request) throws -> Future<Course> {
        print("[GET]:/api/course getAllHandler")
        return try req.parameters.next(Course.self)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Course]> {
        return Course.query(on: req).all()
    }
    
    func testHandler(_ req: Request) throws -> Future<dateString> {
        return try req.parameters.next(RecommendCourse.self).map(to: dateString.self){
            course in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let s = dateFormatter.string(from: course.voteStartTime)
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let dateT = dateFormatter.date(from: s)
            return dateString(firstApplyStartTime: dateT!)
        }
    }
    
    
    // MARK: RollCall
    // Hash verifyString and put it in InCoursePivot
    func putRollCallTestHandler(_ req: Request) throws -> Future<InCoursePivot> {

        return try flatMap(to:InCoursePivot.self,
                           req.parameters.next(Course.self),
                           req.parameters.next(Student.self)) {course,student in
                            let incourse = InCoursePivot.query(on: req)
                                .filter(\InCoursePivot.studentID==student.id!)
                                .filter(\InCoursePivot.courseID==course.id!)
                                .first()
                                .map(to: InCoursePivot.self){ incoursepivots in
                                    let digest = try req.make(BCryptDigest.self)
                                    let date: Date = Date()
                                    let dateFormatter: DateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                                    dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
                                    dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
                                    let dateFormatString: String = dateFormatter.string(from: date)
//                                    print(dateFormatString)
                                    
                                    if (incoursepivots?.verifyString) != nil {
                                        if (incoursepivots?.courseID.uuidString) != nil {
                                            if (incoursepivots?.studentID.uuidString) != nil {
                                                incoursepivots?.verifyString = try digest.hash((incoursepivots?.courseID.uuidString)!+(incoursepivots?.studentID.uuidString)!+dateFormatString)
                                                    _ = incoursepivots?.save(on: req)
                                            }
                                        }
                                    }

                                    
                                    if incoursepivots != nil {
                                        return incoursepivots!
                                    }
                                    else {
                                        throw Abort(.custom(code: 600, reasonPhrase: "Cannot create verifystring because this student not in this course."))
                                    }
                            }
                        return incourse
        }

    }
    
    // Get studentID and verifyString in InCoursePivot
    func getRollCallTestHandler(_ req: Request) throws -> Future<verifyStruct> {
        
        var verifyOneGet: verifyStruct?
        return try flatMap(to:verifyStruct.self,
                           req.parameters.next(Course.self),
                           req.parameters.next(Student.self)) {course,student in
        
            return InCoursePivot.query(on: req)
                .filter(\InCoursePivot.courseID==course.id!)
                .filter(\InCoursePivot.studentID==student.id!)
                .all()
                .map(to: verifyStruct.self) { incoursepivots in
                    
                    for incoursepivot in incoursepivots {
                        verifyOneGet = verifyStruct(studentID: incoursepivot.studentID, verifyString: incoursepivot.verifyString)
                        
                    }
                    if verifyOneGet != nil {
                        return verifyOneGet!
                    }
                    else {
                        throw Abort(.custom(code: 600, reasonPhrase: "Cannot get verifystring because this student not in this course."))
                    }
            }
        }
    }
    
    func getInCourseStudentsHandler(
        _ req: Request
        ) throws -> Future<[Student]> {
        
        return try req.parameters.next(Course.self)
            .flatMap(to: [Student].self) { course in
                
                try course.inCourseStudents.query(on: req).all()
        }
    }
    
    func getApplyStudentsHandler(
        _ req: Request
        ) throws -> Future<[Student]> {
        
        return try req.parameters.next(Course.self)
            .flatMap(to: [Student].self) { course in
                
                try course.applyStudents.query(on: req).all()
        }
    }
    
    
    
    // Update the data of course
    func updateHandler(_ req: Request) throws -> Future<Course> {
        return try flatMap(to: Course.self,
                           req.parameters.next(Course.self),
                           req.content.decode(Course.self)) {
                            course, updatedCourse in
                            course.courseDate = updatedCourse.courseDate
                            course.courseTeacher = updatedCourse.courseTeacher
                            course.coursePlace = updatedCourse.coursePlace
                            course.courseCost = updatedCourse.courseCost
                            course.targetStudent = updatedCourse.targetStudent
                            course.courseState = updatedCourse.courseState
                            course.numberOfApply = updatedCourse.numberOfApply
                            course.firstNumberOfStudents = updatedCourse.firstNumberOfStudents
                            course.firstApplyStartTime = updatedCourse.firstApplyStartTime
                            course.firstApplyEndTime = updatedCourse.firstApplyEndTime
                            course.secondNumberOfStudents = updatedCourse.secondNumberOfStudents
                            course.secondApplyStartTime = updatedCourse.secondApplyStartTime
                            course.secondApplyEndTime = updatedCourse.secondApplyEndTime
                            course.applyMethod = updatedCourse.applyMethod
                            course.enrollPost = updatedCourse.enrollPost
                            course.courseIntroduction = updatedCourse.courseIntroduction
                            course.other = updatedCourse.other
                            return course.save(on: req)
        }
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Course.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    func getCourseEvaluationPrivateHandler(_ req: Request)
        throws -> Future<[EvaluationOneStruct]> {
            return try flatMap(to:[EvaluationOneStruct].self,
                               req.parameters.next(Course.self),
                               req.parameters.next(Student.self)) {course,student in
//            let couID = try req.parameters.next(UUID.self)
            var evaluationAll: [EvaluationOneStruct] = []
            return CourseEvaluation.query(on: req)
                .filter(\CourseEvaluation.courseID == course.id!)
                .filter(\CourseEvaluation.studentID == student.id!)
                .filter(\CourseEvaluation.state == 1)
                .join(\Student.id , to: \CourseEvaluation.studentID)
                .alsoDecode(Student.self)
                .all()
                .map(to: [EvaluationOneStruct].self) { evaluations in
                    var pushOne: EvaluationOneStruct?
                    for evaluation in evaluations {
                        if evaluation.0.id != nil {
                            pushOne = EvaluationOneStruct(evaluationID: evaluation.0.id!, state: evaluation.0.state, studentContent: evaluation.0.studentContent, evaluationTime: evaluation.0.evaluationTime, teacherResponse: evaluation.0.teacherResponse, teacherResponseTime: evaluation.0.teacherResponseTime, courseId: evaluation.0.courseID, studentID: evaluation.1.studentID)
                        }
                        
                        if pushOne != nil {
                            evaluationAll.append(pushOne!)
                        }
                        
                    }
                    evaluationAll = evaluationAll.sorted(by: {$0.evaluationTime.compare($1.evaluationTime) == .orderedDescending})
                    
                    return evaluationAll
            }
            }
    }
    
    func getCourseEvaluationPublicHandler(_ req: Request)
        throws -> Future<[EvaluationOneStruct]> {
            let couID = try req.parameters.next(UUID.self)
            var evaluationAll: [EvaluationOneStruct] = []
            return CourseEvaluation.query(on: req)
                .filter(\CourseEvaluation.courseID == couID)
                .filter(\CourseEvaluation.state == 0)
                .join(\Student.id , to: \CourseEvaluation.studentID)
                .alsoDecode(Student.self)
                .all()
                .map(to: [EvaluationOneStruct].self) { evaluations in
                    var pushOne: EvaluationOneStruct?
                    for evaluation in evaluations {
                        if evaluation.0.id != nil {
                            pushOne = EvaluationOneStruct(evaluationID: evaluation.0.id!, state: evaluation.0.state, studentContent: evaluation.0.studentContent, evaluationTime: evaluation.0.evaluationTime, teacherResponse: evaluation.0.teacherResponse, teacherResponseTime: evaluation.0.teacherResponseTime, courseId: evaluation.0.courseID, studentID: evaluation.1.studentID)
                        }
                        
                        if pushOne != nil {
                            evaluationAll.append(pushOne!)
                        }
                        
                    }
                    evaluationAll = evaluationAll.sorted(by: {$0.evaluationTime.compare($1.evaluationTime) == .orderedDescending})
                    
                    return evaluationAll
            }
    }
    
    func getCourseSatisfactionHandler(_ req: Request)
        throws -> Future<[CourseEvaluation]> {
            return try req
                .parameters.next(Course.self)
                .flatMap(to: [CourseEvaluation].self) { course in
                    try course.courseEvaluation.query(on: req).all()
            }
    }
    
    func getLeaveHandler(_ req: Request)
        throws -> Future<[Leave]> {
            return try req
                .parameters.next(Course.self)
                .flatMap(to: [Leave].self) { course in
                    try course.leave.query(on: req).all()
            }
    }
    
    func getRollCallHandler(_ req: Request)
        throws -> Future<[RollCall]> {
            return try req
                .parameters.next(Course.self)
                .flatMap(to: [RollCall].self) { course in
                    try course.rollCall.query(on: req).all()
            }
    }
    
    // Distinguish different state of courses
    func search(_ req: Request) throws -> Future<[Course]> {
        guard let searchTerm = req.query[Int.self, at: "term"] else {
            throw Abort(.badRequest)
        }
        return Course.query(on: req).filter(\.courseState == searchTerm).all()
    }
    
    //MARK: 我的課表
    func getMyCourseTableHandler(_ req: Request) throws -> Future<[CourseTable]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseTable].self){
            student in
            return Course.query(on: req)
                .filter(\Course.courseState==1)
                .join(\InCoursePivot.courseID, to: \Course.id)
                .filter(\InCoursePivot.studentID==student.id!)
                .all()
                .map(to: [CourseTable].self){
                    courses in
                    let selectDate = try req.parameters.next(String.self)
                    
                    var courseTable:[CourseTable] = []
                    for oneCourse in courses{
                        var oneCourseTable = CourseTable(courseId: oneCourse.id!, courseName: oneCourse.courseName, courseType: oneCourse.courseType, courseDate: "")
                        var getDate = false
                        if oneCourse.courseDate.count >= 2{
                            for dates in oneCourse.courseDate{
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd"
                                let string2date = dateFormatter.date(from: selectDate)
                                let dateAdd = Calendar.current.date(byAdding: .hour, value: +24, to: string2date!)
                                
                                if dates>=string2date! && dates<=dateAdd!{
                                    getDate = true
                                    let datesAdd = Calendar.current.date(byAdding: .hour, value: +8, to: dates)
                                    dateFormatter.locale = Locale(identifier: "zh_TW")
                                    dateFormatter.dateFormat = "ahh:mm-"
                                    let dates2str = dateFormatter.string(from: datesAdd!)
                                    oneCourseTable.courseDate += dates2str
                                }
                            }
                            if getDate{
                                let date = oneCourseTable.courseDate
                                oneCourseTable.courseDate = String(date.dropLast())
                                courseTable.append(oneCourseTable)
                            }
                        }
                    }
                    return courseTable
            }
        }
    }
    
    //MARK: 我全部的課表
    func getMyAllCourseTableHandler(_ req: Request) throws -> Future<[String]> {
        return try req.parameters.next(Student.self).flatMap(to: [String].self){
            student in
            return Course.query(on: req)
                .filter(\Course.courseState==1)
                .join(\InCoursePivot.courseID, to: \Course.id)
                .filter(\InCoursePivot.studentID==student.id!)
                .all()
                .map(to: [String].self){
                    courses in
                    var courseDate:[String] = []
                    for oneCourse in courses{
                        if oneCourse.courseDate.count >= 2{
                            for dates in oneCourse.courseDate{
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd"
                                let datesAdd = Calendar.current.date(byAdding: .hour, value: +8, to: dates)
                                let changeString = dateFormatter.string(from: datesAdd!)
                                if !courseDate.contains(changeString){
                                    courseDate.append(changeString)
                                }
                            }
                        }
                    }
                    return courseDate
            }
        }
        
    }
    
    //MARK: 我的課程
    //MARK: 我上過的課
    func getMyCoursesHandler(_ req: Request) throws -> Future<[CourseAll]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
            var studentState = self.setStudentState(student: stu)
            
            let studentFavorite = FavoritePivot.query(on: req)
                .filter(\FavoritePivot.studentID==stu.id!)
                .all()
            
            let studentApply = ApplyPivot.query(on: req)
                .filter(\ApplyPivot.studentID==stu.id!)
                .all()
            
            let courseQuestion = CourseSatisfaction.query(on: req).all()
            
            let courseAns = CourseSatisfactionAns.query(on: req)
                .filter(\CourseSatisfactionAns.studentID==stu.id!)
                .all()
            
            var courseAll:[CourseAll] = []
            
            _ = Course.query(on: req)
                .join(\InCoursePivot.courseID, to: \Course.id)
                .filter(\InCoursePivot.studentID==stu.id!)
                .all()
                .map(to: [CourseAll].self){
                    courses in
                    for oneCourse in courses{
                        var studentFavoriteBool = 0
                        _ = studentFavorite.flatMap(to: [FavoritePivot].self){
                            stuFavCourses in
                            for stuFavCourse in stuFavCourses{
                                if oneCourse.id == stuFavCourse.courseID{
                                    studentFavoriteBool = 1
                                }
                            }
                            return studentFavorite
                        }
                        var questionBool = 0
                        
                        if let id = oneCourse.id{
                            _ = courseQuestion.flatMap(to: [CourseSatisfaction].self){
                                questions in
                                for quest in questions{
                                    if id == quest.courseID{
                                        questionBool = 1
                                    }
                                }
                                return courseQuestion
                            }
                        }
                        
//                        var fillQuestion = 1
                        _ = courseAns.flatMap(to: [CourseSatisfactionAns].self){
                            answer in
                            for ans in answer{
                                if oneCourse.id == ans.courseID{
                                    questionBool = 0
                                }
                            }
                            return courseAns
                        }
                        
                        
                        
                        let oneCourseState = self.setCourseState(oneCourse: oneCourse)
                        
                        
                        if let index = courseAll.index(where: {$0.id==oneCourse.id}){
                            courseAll[index].studentInCourse = 1
                        }
                        else{
                            courseAll.append(CourseAll(
                                id: oneCourse.id,
                                courseName: oneCourse.courseName,
                                courseType:oneCourse.courseType,
                                campus:oneCourse.campus,
                                courseDate: oneCourse.courseDate,
                                courseTeacher: oneCourse.courseTeacher,
                                coursePlace: oneCourse.coursePlace,
                                courseCost: oneCourse.courseCost,
                                targetStudent: oneCourse.targetStudent,
                                studentState: studentState,
                                status: oneCourseState,
                                numberOfApply: oneCourse.numberOfApply,
                                firstNumberOfStudents: oneCourse.firstNumberOfStudents,
                                firstApplyStartTime: oneCourse.firstApplyStartTime,
                                firstApplyEndTime: oneCourse.firstApplyEndTime,
                                secondNumberOfStudents: oneCourse.secondNumberOfStudents,
                                secondApplyStartTime: oneCourse.secondApplyStartTime,
                                secondApplyEndTime: oneCourse.secondApplyEndTime,
                                applyMethod: oneCourse.applyMethod,
                                enrollPost: oneCourse.enrollPost,
                                courseIntroduction: oneCourse.courseIntroduction,
                                other: oneCourse.other,
                                voteStartTime: self.date1970(),
                                voteEndTime: self.date1970(),
                                numberOfVoters:0,
                                studentFavorite: studentFavoriteBool,
                                studentVote: 0,
                                studentApply: 0,
                                studentInCourse: 1,
                                satisfactionBool: questionBool,
                                applyResult: 0,
                                courseWeek:[0,0,0,0,0,0,0]
                            ))
                        }
                    }
                    return courseAll
            }
            let studentApplyCourse = Course.query(on: req)
                .join(\ApplyPivot.courseID, to: \Course.id)
                .filter(\ApplyPivot.studentID==stu.id!)
                .all()
                .map(to: [CourseAll].self){
                    courses in
                    for oneCourse in courses{
                        var studentFavoriteBool = 0
                        
                        _ = studentFavorite.flatMap(to: [FavoritePivot].self){
                            stuFavCourses in
                            for stuFavCourse in stuFavCourses{
                                if oneCourse.id == stuFavCourse.courseID{
                                    studentFavoriteBool = 1
                                }
                            }
                            return studentFavorite
                        }
                        
                        var applyResult = -1
                        _ = studentApply.flatMap(to: [ApplyPivot].self){
                            studentCourses in
                            for studentCourse in studentCourses{
                                if oneCourse.id == studentCourse.courseID{
                                    applyResult = studentCourse.applyResult
                                }
                            }
                            return studentApply
                        }
                        
                        var questionBool = 0
                        
                        if let id = oneCourse.id{
                            _ = courseQuestion.flatMap(to: [CourseSatisfaction].self){
                                questions in
                                for quest in questions{
                                    if id == quest.courseID{
                                        questionBool = 1
                                    }
                                }
                                return courseQuestion
                            }
                        }
                        
                        _ = courseAns.flatMap(to: [CourseSatisfactionAns].self){
                            answer in
                            for ans in answer{
                                if oneCourse.id == ans.courseID{
                                    questionBool = 0
                                }
                            }
                            return courseAns
                        }
                        
                        if oneCourse.firstApplyStartTime < Date() && oneCourse.firstApplyEndTime > Date() {
                            studentState = 1
                        }
                        
                        let oneCourseState = self.setCourseState(oneCourse: oneCourse)
                        
                        if let index = courseAll.index(where: {$0.id==oneCourse.id}){
                            courseAll[index].studentApply = 1
                        }
                        else{
                            if oneCourse.courseState == 0 {
                                courseAll.append(CourseAll(
                                    id: oneCourse.id,
                                    courseName: oneCourse.courseName,
                                    courseType:oneCourse.courseType,
                                    campus:oneCourse.campus,
                                    courseDate: oneCourse.courseDate,
                                    courseTeacher: oneCourse.courseTeacher,
                                    coursePlace: oneCourse.coursePlace,
                                    courseCost: oneCourse.courseCost,
                                    targetStudent: oneCourse.targetStudent,
                                    studentState: studentState,
                                    status: oneCourseState,
                                    numberOfApply: oneCourse.numberOfApply,
                                    firstNumberOfStudents: oneCourse.firstNumberOfStudents,
                                    firstApplyStartTime: oneCourse.firstApplyStartTime,
                                    firstApplyEndTime: oneCourse.firstApplyEndTime,
                                    secondNumberOfStudents: oneCourse.secondNumberOfStudents,
                                    secondApplyStartTime: oneCourse.secondApplyStartTime,
                                    secondApplyEndTime: oneCourse.secondApplyEndTime,
                                    applyMethod: oneCourse.applyMethod,
                                    enrollPost: oneCourse.enrollPost,
                                    courseIntroduction: oneCourse.courseIntroduction,
                                    other: oneCourse.other,
                                    voteStartTime: self.date1970(),
                                    voteEndTime: self.date1970(),
                                    numberOfVoters:0,
                                    studentFavorite: studentFavoriteBool,
                                    studentVote: 0,
                                    studentApply: 1,
                                    studentInCourse: 0,
                                    satisfactionBool: questionBool,
                                    applyResult: applyResult,
                                    courseWeek:[0,0,0,0,0,0,0]
                                ))
                            }
                            
                        }
                    }
                    return courseAll
            }
            return studentApplyCourse
        }
    }
    
    //MARK: 我推薦過的課
    func getMyRecommendHandler(_ req: Request) throws -> Future<[CourseAll]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
            let studentState = self.setStudentState(student: stu)
            
            let studentFavorite = FavRecPivot.query(on: req)
                .filter(\FavRecPivot.studentID==stu.id!)
                .all()
            let studentVote = VotePivot.query(on: req)
                .filter(\VotePivot.studentID==stu.id!)
                .all()
            
            
            var courseAll:[CourseAll] = []
            
            let select = RecommendCourse.query(on: req)
                .filter(\RecommendCourse.proposerID==stu.id!)
                .all()
                .map(to: [CourseAll].self){
                    courses in
                    for oneCourse in courses{
                        var studentFavoriteBool = 0
                        _ = studentFavorite.flatMap(to: [FavRecPivot].self){
                            stuFavCourses in
                            for stuFavCourse in stuFavCourses{
                                if oneCourse.id == stuFavCourse.recommendcourseID{
                                    studentFavoriteBool = 1
                                }
                            }
                            return studentFavorite
                        }
                        
                        var studentVoteBool = 0
                        var courseWeek=[0,0,0,0,0,0,0]
                        _ = studentVote.flatMap(to: [VotePivot].self){
                            studentCourses in
                            for studentCourse in studentCourses{
                                if oneCourse.id == studentCourse.recommendcourseID{
                                    studentVoteBool = 1
                                    courseWeek = studentCourse.courseWeek
                                }
                            }
                            return studentVote
                        }
                        
                        
                        let oneCourseState = self.setRecCourseState(oneCourse: oneCourse)
                        
                        courseAll.append(CourseAll(
                            id: oneCourse.id,
                            courseName: oneCourse.courseName,
                            courseType:oneCourse.courseType,
                            campus:oneCourse.campus,
                            courseDate: [Date](),
                            courseTeacher: "",
                            coursePlace: "",
                            courseCost: -1,
                            targetStudent: -1,
                            studentState: studentState,
                            status: oneCourseState,
                            numberOfApply: -1,
                            firstNumberOfStudents: -1,
                            firstApplyStartTime: self.date1970(),
                            firstApplyEndTime: self.date1970(),
                            secondNumberOfStudents: -1,
                            secondApplyStartTime: self.date1970(),
                            secondApplyEndTime: self.date1970(),
                            applyMethod: -1,
                            enrollPost: -1,
                            courseIntroduction: oneCourse.courseIntroduction,
                            other: "",
                            voteStartTime: oneCourse.voteStartTime,
                            voteEndTime: oneCourse.voteEndTime,
                            numberOfVoters:oneCourse.numberOfVoters,
                            studentFavorite: studentFavoriteBool,
                            studentVote: studentVoteBool,
                            studentApply: 0,
                            studentInCourse: 0,
                            satisfactionBool: 0,
                            applyResult: -1,
                            courseWeek: courseWeek
                        ))
                        
                    }
                    return courseAll
            }
            return select
        }
    }
    
    //MARK: 我投過票的課
    func getMyVoteCoursesHandler(_ req: Request) throws -> Future<[CourseAll]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
            let studentState = self.setStudentState(student: stu)
            
            let studentFavorite = FavRecPivot.query(on: req)
                .filter(\FavRecPivot.studentID==stu.id!)
                .all()
            let studentVote = VotePivot.query(on: req)
                .filter(\VotePivot.studentID==stu.id!)
                .all()
            var courseAll:[CourseAll] = []
            
            let select = RecommendCourse.query(on: req)
                .join(\VotePivot.recommendcourseID, to: \RecommendCourse.id)
                .filter(\VotePivot.studentID==stu.id!)
                .all()
                .map(to: [CourseAll].self){
                    courses in
                    for oneCourse in courses{
                        var studentFavoriteBool = 0
                        _ = studentFavorite.flatMap(to: [FavRecPivot].self){
                            stuFavCourses in
                            for stuFavCourse in stuFavCourses{
                                if oneCourse.id == stuFavCourse.recommendcourseID{
                                    studentFavoriteBool = 1
                                }
                            }
                            return studentFavorite
                        }
                        
                        var studentVoteBool = 0
                        var courseWeek=[0,0,0,0,0,0,0]
                        _ = studentVote.flatMap(to: [VotePivot].self){
                            studentCourses in
                            for studentCourse in studentCourses{
                                if oneCourse.id == studentCourse.recommendcourseID{
                                    studentVoteBool = 1
                                    courseWeek = studentCourse.courseWeek
                                }
                            }
                            return studentVote
                        }
                        
                        let oneCourseState = self.setRecCourseState(oneCourse: oneCourse)
                        
                        courseAll.append(CourseAll(
                            id: oneCourse.id,
                            courseName: oneCourse.courseName,
                            courseType:oneCourse.courseType,
                            campus:oneCourse.campus,
                            courseDate: [Date](),
                            courseTeacher: "",
                            coursePlace: "",
                            courseCost: -1,
                            targetStudent: -1,
                            studentState: studentState,
                            status: oneCourseState,
                            numberOfApply: -1,
                            firstNumberOfStudents: -1,
                            firstApplyStartTime: self.date1970(),
                            firstApplyEndTime: self.date1970(),
                            secondNumberOfStudents: -1,
                            secondApplyStartTime: self.date1970(),
                            secondApplyEndTime: self.date1970(),
                            applyMethod: -1,
                            enrollPost: -1,
                            courseIntroduction: oneCourse.courseIntroduction,
                            other: "",
                            voteStartTime:oneCourse.voteStartTime,
                            voteEndTime:oneCourse.voteEndTime,
                            numberOfVoters:oneCourse.numberOfVoters,
                            studentFavorite: studentFavoriteBool,
                            studentVote: studentVoteBool,
                            studentApply: 0,
                            studentInCourse: 0,
                            satisfactionBool: 0,
                            applyResult: -1,
                            courseWeek: courseWeek
                        ))
                    }
                    return courseAll
            }
            return select
        }
    }
    
    //MARK: 我收藏的課
    func getMyFavoriteCoursesHandler(_ req: Request) throws -> Future<[CourseAll]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
            
            var studentState = self.setStudentState(student: stu)

            let studentVote = VotePivot.query(on: req)
                .filter(\VotePivot.studentID==stu.id!)
                .all()
            let studentApply = ApplyPivot.query(on: req)
                .filter(\ApplyPivot.studentID==stu.id!)
                .all()
            let studentInCourse = InCoursePivot.query(on: req)
                .filter(\InCoursePivot.studentID==stu.id!)
                .all()
            let courseQuestion = CourseSatisfaction.query(on: req).all()
            
            var courseAll:[CourseAll] = []
            
            _ = Course.query(on: req)
                .join(\FavoritePivot.courseID, to: \Course.id)
                .filter(\FavoritePivot.studentID==stu.id!)
                .all()
                .map(to: [CourseAll].self){
                    courses in
                    for oneCourse in courses{
                        let courseWeek = [0,0,0,0,0,0,0]
                        let studentFavoriteBool = 1
                        let studentVoteBool = 0
                        var studentApplyBool = 0
                        var applyResult = -1
                        _ = studentApply.flatMap(to: [ApplyPivot].self){
                            studentCourses in
                            for studentCourse in studentCourses{
                                if oneCourse.id == studentCourse.courseID{
                                    studentApplyBool = 1
                                    applyResult = studentCourse.applyResult
                                }
                            }
                            return studentApply
                        }
                        
                        var studentInCourseBool = 0
                        _ = studentInCourse.flatMap(to: [InCoursePivot].self){
                            studentCourses in
                            for studentCourse in studentCourses{
                                if oneCourse.id == studentCourse.courseID{
                                    studentInCourseBool = 1
                                }
                            }
                            return studentInCourse
                        }
                        
                        if oneCourse.firstApplyStartTime < Date() && oneCourse.firstApplyEndTime > Date() {
                            studentState = 1
                        }
                        
                        let oneCourseState = self.setCourseState(oneCourse: oneCourse)
                        var questionBool = 0
                        
                        if let id = oneCourse.id{
                            _ = courseQuestion.flatMap(to: [CourseSatisfaction].self){
                                questions in
                                for quest in questions{
                                    if id == quest.courseID{
                                        questionBool = 1
                                    }
                                }
                                return courseQuestion
                            }
                        }
                        
                        courseAll.append(CourseAll(
                            id: oneCourse.id,
                            courseName: oneCourse.courseName,
                            courseType:oneCourse.courseType,
                            campus:oneCourse.campus,
                            courseDate: oneCourse.courseDate,
                            courseTeacher: oneCourse.courseTeacher,
                            coursePlace: oneCourse.coursePlace,
                            courseCost: oneCourse.courseCost,
                            targetStudent: oneCourse.targetStudent,
                            studentState: studentState,
                            status: oneCourseState,
                            numberOfApply: oneCourse.numberOfApply,
                            firstNumberOfStudents: oneCourse.firstNumberOfStudents,
                            firstApplyStartTime: oneCourse.firstApplyStartTime,
                            firstApplyEndTime: oneCourse.firstApplyEndTime,
                            secondNumberOfStudents: oneCourse.secondNumberOfStudents,
                            secondApplyStartTime: oneCourse.secondApplyStartTime,
                            secondApplyEndTime: oneCourse.secondApplyEndTime,
                            applyMethod: oneCourse.applyMethod,
                            enrollPost: oneCourse.enrollPost,
                            courseIntroduction: oneCourse.courseIntroduction,
                            other: oneCourse.other,
                            voteStartTime:self.date1970(),
                            voteEndTime:self.date1970(),
                            numberOfVoters:0,
                            studentFavorite: studentFavoriteBool,
                            studentVote: studentVoteBool,
                            studentApply: studentApplyBool,
                            studentInCourse: studentInCourseBool,
                            satisfactionBool: questionBool,
                            applyResult: applyResult,
                            courseWeek: courseWeek
                        ))
                        
                    }
                    return courseAll
            }
        
        let select = RecommendCourse.query(on: req)
            .join(\FavRecPivot.recommendcourseID, to: \RecommendCourse.id)
            .filter(\FavRecPivot.studentID==stu.id!)
            .all()
            .map(to: [CourseAll].self){
                courses in
                for oneCourse in courses{
                    let studentFavoriteBool = 1
                    var studentVoteBool = 0
                    var courseWeek = [0,0,0,0,0,0,0]
                    _ = studentVote.flatMap(to: [VotePivot].self){
                        studentCourses in
                        for studentCourse in studentCourses{
                            if oneCourse.id == studentCourse.recommendcourseID{
                                studentVoteBool = 1
                                courseWeek = studentCourse.courseWeek
                            }
                        }
                        return studentVote
                    }
                    
                    let oneCourseState = self.setRecCourseState(oneCourse: oneCourse)
                    
                    courseAll.append(CourseAll(
                        id: oneCourse.id,
                        courseName: oneCourse.courseName,
                        courseType:oneCourse.courseType,
                        campus:oneCourse.campus,
                        courseDate: [Date](),
                        courseTeacher: "",
                        coursePlace: "",
                        courseCost: -1,
                        targetStudent: -1,
                        studentState: studentState,
                        status: oneCourseState,
                        numberOfApply: -1,
                        firstNumberOfStudents: -1,
                        firstApplyStartTime: self.date1970(),
                        firstApplyEndTime: self.date1970(),
                        secondNumberOfStudents: -1,
                        secondApplyStartTime: self.date1970(),
                        secondApplyEndTime: self.date1970(),
                        applyMethod: -1,
                        enrollPost: -1,
                        courseIntroduction: oneCourse.courseIntroduction,
                        other: "",
                        voteStartTime:oneCourse.voteStartTime,
                        voteEndTime:oneCourse.voteEndTime,
                        numberOfVoters:oneCourse.numberOfVoters,
                        studentFavorite: studentFavoriteBool,
                        studentVote: studentVoteBool,
                        studentApply: 0,
                        studentInCourse: 0,
                        satisfactionBool: 0,
                        applyResult: -1,
                        courseWeek: courseWeek
                    ))
                }
                return courseAll
        }
        return select
        }
    }
    
    //MARK: 課程一覽
    //MARK: 上課中
    func getCourseDuringHandler(_ req: Request) throws -> Future<[CourseAll]> {
//        let stuID = try req.parameters.next(UUID.self)
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
            let studentInCourse = InCoursePivot.query(on: req)
                .filter(\InCoursePivot.studentID==stu.id!)
                .all()
            let studentFavorite = FavoritePivot.query(on: req)
                .filter(\FavoritePivot.studentID==stu.id!)
                .all()
            let courseAll:[CourseAll] = []
            
            return self.inCourseSelect(courses: courseAll, req: req, studentInCourse:studentInCourse, studentFavorite: studentFavorite,student: stu)
        }
    }
    
    //MARK: 報名中
    func getCourseApplyingHandler(_ req: Request) throws -> Future<[CourseAll]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
//            var openCourse:[Course?] = []
            let openCourse = try stu.voteCourses.query(on: req)
                .all()
            let studentApply = ApplyPivot.query(on: req)
                .filter(\ApplyPivot.studentID==stu.id!)
                .all()
            let studentFavorite = FavoritePivot.query(on: req)
                .filter(\FavoritePivot.studentID==stu.id!)
                .all()
            
            let courseAll:[CourseAll] = []
            
            return self.applyCourseSelect(courses: courseAll, req: req, studentApply: studentApply, studentFavorite: studentFavorite, student: stu, studentVote: openCourse)
        }
    }
    
    //MARK: 投票中
    func getCourseVotingHandler(_ req: Request) throws -> Future<[CourseAll]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
            let studentVote = VotePivot.query(on: req)
                .filter(\VotePivot.studentID==stu.id!)
                .all()
            let studentFavorite = FavRecPivot.query(on: req)
                .filter(\FavRecPivot.studentID==stu.id!)
                .all()
            
            let courseAll:[CourseAll] = []
            
            return self.recommendSelect(courses: courseAll, req: req, studentVote: studentVote, studentFavorite: studentFavorite,student: stu)
        }
    }
    
    //MARK: 全部
    func getCourseAllHandler(_ req: Request) throws -> Future<[CourseAll]> {
        return try req.parameters.next(Student.self).flatMap(to: [CourseAll].self){
            stu in
            let studentVote = VotePivot.query(on: req)
                .filter(\VotePivot.studentID==stu.id!)
                .all()
            let studentApply = ApplyPivot.query(on: req)
                .filter(\ApplyPivot.studentID==stu.id!)
                .all()
            let studentInCourse = InCoursePivot.query(on: req)
                .filter(\InCoursePivot.studentID==stu.id!)
                .all()
            let studentFavorite = FavoritePivot.query(on: req)
                .filter(\FavoritePivot.studentID==stu.id!)
                .all()
            let studentRecFavorite = FavRecPivot.query(on: req)
                .filter(\FavRecPivot.studentID==stu.id!)
                .all()
            var courseAll:[CourseAll] = []
            
//            var openCourse:[Course?] = []
            let openCourse = try stu.voteCourses.query(on: req)
                .all()
        
            _ = self.recommendSelect(courses: courseAll, req: req, studentVote: studentVote, studentFavorite: studentRecFavorite,student: stu).map(to: [CourseAll].self){
                coursealls in
                for course in coursealls{
                    courseAll.append(course)
                }
                return courseAll
            }
            
            _ = self.inCourseSelect(courses: courseAll, req: req, studentInCourse:studentInCourse, studentFavorite: studentFavorite, student: stu).map(to:[CourseAll].self){
                coursealls in
                for course in coursealls{
                    courseAll.append(course)
                }
                return courseAll
            }
                let select1 = self.applyCourseSelect(courses: courseAll, req: req, studentApply: studentApply, studentFavorite: studentFavorite, student: stu, studentVote: openCourse).map(to:[CourseAll].self){
                coursealls in
                for course in coursealls{
                    courseAll.append(course)
                }
                return courseAll
            }
            return select1
        }
    }
    
    func inCourseSelect(courses:[CourseAll],req:Request,studentInCourse:Future<[InCoursePivot]>,studentFavorite:Future<[FavoritePivot]>,student:Student) -> Future<[CourseAll]> {
        var courseAll = courses
        let courseQuestion = CourseSatisfaction.query(on: req).all()
        let select = Course.query(on: req).filter(\Course.courseState==1).sort(\Course.courseDate[0]).all().map(to: [CourseAll].self){
            course in
            for oneCourse in course{
                let courseWeek = [0,0,0,0,0,0,0]
                var studentInCourseBool = 0
                _ = studentInCourse.flatMap(to: [InCoursePivot].self){
                    studentCourses in
                    for studentCourse in studentCourses{
                        if oneCourse.id == studentCourse.courseID{
                            studentInCourseBool = 1
                        }
                    }
                    return studentInCourse
                }
                var studentFavoriteBool = 0
                _ = studentFavorite.flatMap(to: [FavoritePivot].self){
                    stuFavCourses in
                    for stuFavCourse in stuFavCourses{
                        if oneCourse.id == stuFavCourse.courseID{
                            studentFavoriteBool = 1
                        }
                    }
                    return studentFavorite
                }
                let oneCourseState = self.setCourseState(oneCourse: oneCourse)
                let studentState = self.setStudentState(student: student)
                
                var questionBool = 0
                
                if let id = oneCourse.id{
                    _ = courseQuestion.flatMap(to: [CourseSatisfaction].self){
                        questions in
                        for quest in questions{
                            if id == quest.courseID{
                                questionBool = 1
                            }
                        }
                        return courseQuestion
                    }
                }
                
                if let index = courseAll.index(where: {$0.id==oneCourse.id}){
                    courseAll[index].studentInCourse = studentInCourseBool
                }
                else{
                    courseAll.append(CourseAll(
                        id: oneCourse.id,
                        courseName: oneCourse.courseName,
                        courseType:oneCourse.courseType,
                        campus:oneCourse.campus,
                        courseDate: oneCourse.courseDate,
                        courseTeacher: oneCourse.courseTeacher,
                        coursePlace: oneCourse.coursePlace,
                        courseCost: oneCourse.courseCost,
                        targetStudent: oneCourse.targetStudent,
                        studentState: studentState,
                        status: oneCourseState,
                        numberOfApply: oneCourse.numberOfApply,
                        firstNumberOfStudents: oneCourse.firstNumberOfStudents,
                        firstApplyStartTime: oneCourse.firstApplyStartTime,
                        firstApplyEndTime: oneCourse.firstApplyEndTime,
                        secondNumberOfStudents: oneCourse.secondNumberOfStudents,
                        secondApplyStartTime: oneCourse.secondApplyStartTime,
                        secondApplyEndTime: oneCourse.secondApplyEndTime,
                        applyMethod: oneCourse.applyMethod,
                        enrollPost: oneCourse.enrollPost,
                        courseIntroduction: oneCourse.courseIntroduction,
                        other: oneCourse.other,
                        voteStartTime: self.date1970(),
                        voteEndTime: self.date1970(),
                        numberOfVoters:0,
                        studentFavorite: studentFavoriteBool,
                        studentVote: 0,
                        studentApply: 0,
                        studentInCourse: studentInCourseBool,
                        satisfactionBool: questionBool,
                        applyResult: -1,
                        courseWeek: courseWeek
                    ))
                }
            }
            return courseAll
        }
        return select
    }
    
    func applyCourseSelect(courses:[CourseAll],req:Request,studentApply:Future<[ApplyPivot]>,studentFavorite:Future<[FavoritePivot]>,student:Student, studentVote: Future<[RecommendCourse]>) -> Future<[CourseAll]> {

        var courseAll = courses
        var select = Course.query(on: req).filter(\Course.courseState==0).sort(\Course.firstApplyStartTime).all().map(to: [CourseAll].self){
            course in
            for oneCourse in course{
                var studentApplyBool = 0
                let courseWeek = [0,0,0,0,0,0,0]
                var applyResult = -1
                _ = studentApply.flatMap(to: [ApplyPivot].self){
                    studentCourses in
                    for studentCourse in studentCourses{
                        if oneCourse.id == studentCourse.courseID{
                            studentApplyBool = 1
                            applyResult = studentCourse.applyResult
                        }
                    }
                    return studentApply
                }
                var studentFavoriteBool = 0
                _ = studentFavorite.flatMap(to: [FavoritePivot].self){
                    stuFavCourses in
                    for stuFavCourse in stuFavCourses{
                        if oneCourse.id == stuFavCourse.courseID{
                            studentFavoriteBool = 1
                        }
                    }
                    return studentFavorite
                }
                let oneCourseState = self.setCourseState(oneCourse: oneCourse)
                let studentState = self.setStudentState(student: student)
                
                if let index = courseAll.index(where: {$0.id==oneCourse.id}){
                    courseAll[index].studentApply = studentApplyBool
                }
                else{
                    courseAll.append(CourseAll(
                        id: oneCourse.id,
                        courseName: oneCourse.courseName,
                        courseType:oneCourse.courseType,
                        campus:oneCourse.campus,
                        courseDate: oneCourse.courseDate,
                        courseTeacher: oneCourse.courseTeacher,
                        coursePlace: oneCourse.coursePlace,
                        courseCost: oneCourse.courseCost,
                        targetStudent: oneCourse.targetStudent,
                        studentState: studentState,
                        status: oneCourseState,
                        numberOfApply: oneCourse.numberOfApply,
                        firstNumberOfStudents: oneCourse.firstNumberOfStudents,
                        firstApplyStartTime: oneCourse.firstApplyStartTime,
                        firstApplyEndTime: oneCourse.firstApplyEndTime,
                        secondNumberOfStudents: oneCourse.secondNumberOfStudents,
                        secondApplyStartTime: oneCourse.secondApplyStartTime,
                        secondApplyEndTime: oneCourse.secondApplyEndTime,
                        applyMethod: oneCourse.applyMethod,
                        enrollPost: oneCourse.enrollPost,
                        courseIntroduction: oneCourse.courseIntroduction,
                        other: oneCourse.other,
                        voteStartTime:self.date1970(),
                        voteEndTime:self.date1970(),
                        numberOfVoters:0,
                        studentFavorite: studentFavoriteBool,
                        studentVote: 0,
                        studentApply: studentApplyBool,
                        studentInCourse: 0,
                        satisfactionBool: 0,
                        applyResult: applyResult,
                        courseWeek: courseWeek
                    ))
                }
            }
            return courseAll
        }
    
        select = OpenCoursePivot.query(on: req).join(\RecommendCourse.id, to: \OpenCoursePivot.rcmCourseID).alsoDecode(RecommendCourse.self).all().map(to: [CourseAll].self){
            rcmCourses in
            for rcmCourse in rcmCourses{
                _ = studentVote.map(to: Int.self){
                    vots in
                    for vot in vots{
                        if vot.id == rcmCourse.1.id{
                            if let index = courseAll.index(where: {$0.id == rcmCourse.0.courseID}) {
                                courseAll[index].studentState = 1
                            }
                        }
                    }
                    return 0
                }
            }
            return courseAll
        }
        
        return select
    }
    func recommendSelect(courses: [CourseAll],req:Request,studentVote:Future<[VotePivot]>,studentFavorite:Future<[FavRecPivot]>,student:Student) -> Future<[CourseAll]> {
        print("In recommendSelect")
        var courseAll = courses
        let select = RecommendCourse.query(on: req).filter(\RecommendCourse.verifyState==3).sort(\RecommendCourse.voteStartTime).all().map(to: [CourseAll].self){
            course in
            print("RCMCourse: \(course)")
            for oneCourse in course{
                var courseWeek = [0,0,0,0,0,0,0]
                var studentVoteBool = 0
                _ = studentVote.flatMap(to: [VotePivot].self){
                    studentCourses in
                    for studentCourse in studentCourses{
                        if oneCourse.id == studentCourse.recommendcourseID{
                            studentVoteBool = 1
                            courseWeek = studentCourse.courseWeek
                        }
                    }
                    return studentVote
                }
                var studentFavoriteBool = 0
                _ = studentFavorite.flatMap(to: [FavRecPivot].self){
                    stuFavCourses in
                    for stuFavCourse in stuFavCourses{
                        if oneCourse.id == stuFavCourse.recommendcourseID{
                            studentFavoriteBool = 1
                        }
                    }
                    return studentFavorite
                }
                let oneCourseState = self.setRecCourseState(oneCourse: oneCourse)
                let studentState =  self.setStudentState(student: student)
            
                
                if let index = courseAll.index(where: {$0.id==oneCourse.id}){
                    courseAll[index].studentVote = studentVoteBool
                    courseAll[index].numberOfVoters = oneCourse.numberOfVoters
                }
                else{
                    courseAll.append(CourseAll(
                        id: oneCourse.id,
                        courseName: oneCourse.courseName,
                        courseType:oneCourse.courseType,
                        campus:oneCourse.campus,
                        courseDate: [Date](),
                        courseTeacher: "",
                        coursePlace: "",
                        courseCost: -1,
                        targetStudent: -1,
                        studentState: studentState,
                        status: oneCourseState,
                        numberOfApply: -1,
                        firstNumberOfStudents: -1,
                        firstApplyStartTime: self.date1970(),
                        firstApplyEndTime: self.date1970(),
                        secondNumberOfStudents: -1,
                        secondApplyStartTime: self.date1970(),
                        secondApplyEndTime: self.date1970(),
                        applyMethod: -1,
                        enrollPost: -1,
                        courseIntroduction: oneCourse.courseIntroduction,
                        other: "",
                        voteStartTime:oneCourse.voteStartTime,
                        voteEndTime:oneCourse.voteEndTime,
                        numberOfVoters:oneCourse.numberOfVoters,
                        studentFavorite: studentFavoriteBool,
                        studentVote: studentVoteBool,
                        studentApply: 0,
                        studentInCourse: 0,
                        satisfactionBool: 0,
                        applyResult: -1,
                        courseWeek: courseWeek
                    ))
                }
            }
            return courseAll
        }
        print("CourseAll: \(courseAll)")
        return select
    }
    
    func setCourseState(oneCourse: Course) -> Int {
        if oneCourse.courseState == 0{
            if oneCourse.firstApplyStartTime>Date(){
                return 3
            }
            else if oneCourse.firstApplyStartTime<Date() && oneCourse.firstApplyEndTime>Date(){
                return 4
            }
            else if oneCourse.firstApplyEndTime<Date() && oneCourse.secondApplyStartTime>Date(){
                return 5
            }
            else if oneCourse.secondApplyStartTime<Date() && oneCourse.secondApplyEndTime>Date(){
                return 6
            }
            else if oneCourse.secondApplyEndTime<Date(){
                return 7
            }
        }
        else if oneCourse.courseState == 1{
            guard let startTime = oneCourse.courseDate.first, let endTime = oneCourse.courseDate.last else {
                return -1
            }
            if startTime>Date(){
                return 8
            }
            else if endTime>Date(){
                return 9
            }
            else{
                if startTime==endTime {
                    return 8
                }
                else {
                    return 10
                }
                
            }
        }
        else if oneCourse.courseState == 2{
            return 10
        }
        return -1
    }
    
    func setRecCourseState(oneCourse: RecommendCourse) -> Int {
        switch oneCourse.verifyState {
            case 0:
                return 11
            case 1:
                return 12
            case 2:
                return 13
            case 3:
                if oneCourse.voteStartTime>Date(){
                    return 0
                }
                else if oneCourse.voteEndTime>Date(){
                    return 1
                }
                else{
                    return 2
                }
            case 4:
                return 14
            case 5:
                return 15
            case 6:
                return 14
            default:
                return -1
        }
    }
    
    func setStudentState(student: Student) -> Int {
        if student.studentBlackList == 1{
            return 3
        }
        switch student.studentType {
        case 0:
            return 0
        case 1:
            return 2
        default:
            return -1
        }
    }
    
    func setStudentState2(student: UUID,req: Request) -> Int {
        var rtn = 0
        _ = Student.query(on: req).filter(\Student.id==student).first().map(to: Int.self){
            stu in
            if stu?.studentBlackList == 1{
                rtn = 3
                return 3
            }
            switch stu?.studentType {
            case 0:
                rtn = 0
                return 0
            case 1:
                rtn = 2
                return 2
            default:
                rtn = -1
                return -1
            }
        }
        return rtn
        
    }
    
    func date1970() -> Date {
        let timeInterval:TimeInterval = 0
        return Date(timeIntervalSince1970: timeInterval)
    }
}

struct CourseAll: Encodable,Content {
    var id: UUID?
    var courseName: String
    var courseType: Int // 0:語言 1:證照 2:其他
    var campus: Int // 0:博愛校區 1:天母校區
    var courseDate: [Date] // 課程日期+時間
    var courseTeacher: String
    var coursePlace: String
    var courseCost: Int
    var targetStudent: Int // 0:一般生
    var studentState: Int //學生身份 0:一般人 1:一階段報名 2:陽光種子 3:黑名單(可以投票、不能報名)
    var status: Int // 0:尚未開始投票 1:投票中 2:投票截止 3:尚未開始報名 4:一階段報名中 5:一階段報名截止 6:二階段報名中 7:報名截止
    //8:尚未開始上課 9:上課中 10:上課結束 11:未審核 12:審核通過 13:審核未通過 14:投票通過 15:投票未通過
    var numberOfApply: Int // 錄取學生數
    var firstNumberOfStudents: Int // 第一階段招生人數
    var firstApplyStartTime: Date // 第一階段報名開始時間
    var firstApplyEndTime: Date // 第一階段報名結束時間
    var secondNumberOfStudents: Int // 第二階段招生人數
    var secondApplyStartTime: Date // 第二階段報名開始時間
    var secondApplyEndTime: Date // 第二階段報名結束時間
    var applyMethod: Int // 報名方式 0:透過 push pull app
    var enrollPost: Int // 錄取公告  0:透過 push pull app
    var courseIntroduction: String
    var other: String
    var voteStartTime: Date //投票開始時間
    var voteEndTime: Date //投票結束時間
    var numberOfVoters: Int
    var studentFavorite: Int
    var studentVote: Int
    var studentApply: Int
    var studentInCourse: Int
    var satisfactionBool: Int
    var applyResult: Int //0:正取 1:備取
    var courseWeek: [Int] //[0,0,0,0,0,0,0] //學生希望上課的星期 0:無 1:有 ex: [1,1,1,1,1,0,0] 希望平日一到五上課
}

struct verifyStruct: Encodable, Content {
    var studentID: UUID
    var verifyString: String
}

struct dateString: Encodable, Content {
    var firstApplyStartTime: Date
}

struct CourseTable: Encodable,Content {
    var courseId: UUID
    var courseName: String
    var courseType: Int
    var courseDate: String
}

struct EvaluationOneStruct: Content {
    var evaluationID: UUID
    var state: Int
    var studentContent: String
    var evaluationTime: Date
    var teacherResponse: String
    var teacherResponseTime: Date
    var courseId: UUID
    var studentID: String    
}
