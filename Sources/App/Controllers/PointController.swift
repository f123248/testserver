import Vapor
import Fluent

struct PointController: RouteCollection {
    
    func boot(router: Router) throws {
        let pointRoute = router.grouped("api", "point")
        pointRoute.post(Point.self, use: createHandler)
        pointRoute.get(use: getAllHandler)
        pointRoute.get(Point.parameter, use: getHandler)
        pointRoute.get(Point.parameter, "student", use: getStudentHandler)
        pointRoute.delete("deletePoint",Point.parameter, use: deleteHandler)
        pointRoute.get("getAllPoints", UUID.parameter, use:getAllPointHandler)
        pointRoute.get("getLuckyDay", UUID.parameter, use: getLuckyDayPointHandler)
        
        let webRoute = router
        webRoute.get("point",use: pointViewHandler)
        webRoute.get("createPoint",use: createPointViewHandler)
        webRoute.post(CreatePoint.self, at: "createPoint", use: createPointHandler)
    }
    func createPointHandler(
        _ req: Request,
        point: CreatePoint
        ) throws -> Future<Point> {
        print("\(Date()) [POST]:createPoint [FUNC]=createPointHandler")
        return Student.query(on: req).filter(\Student.studentID == point.studentID).first().flatMap(to: Point.self){
            student in
            if student != nil {
                let newPoint = Point(pointReason: point.reason, getPoint: point.point, getPointDate: Date(), studentID: student!.id!)
                return newPoint.save(on: req)
            }
            else {
                throw Abort(.custom(code: 604, reasonPhrase: "Not found StudentID"))
            }
        }
        
    }
    
    func createPointViewHandler(_ req: Request) throws -> Future<View> {
        return try req.view().render("createPoint")
    }
    
    func pointViewHandler(_ req: Request) -> Future<View> {
        return Point.query(on: req).join(\Student.id, to: \Point.studentID).alsoDecode(Student.self).sort(\Point.getPointDate, ._descending).all().flatMap({ points in
            var pointStudents:[PointStudent] = []
            for point in points{
                let pointStudent = PointStudent(id: point.0.id, studentID: point.1.studentID, studentName: point.1.studentName, getPoint: point.0.getPoint, pointReason: point.0.pointReason, getPointDate: point.0.getPointDate)
                pointStudents.append(pointStudent)
            }
            let context = PointContext(title: "Point", points: pointStudents.isEmpty ? nil : pointStudents)
            return try req.view().render("point", context)
        })
    }
    
    func getLuckyDayPointHandler(_ req: Request) throws -> Future<GetLuckyDay> {
        print("\(Date()) [GET]:api/point/getLuckyDay [FUNC]=getLuckyDayPointHandler")
        var initPoint = [-1,0,10,30]
//        var getIndex = Int.random(in: 0...3)
//        var getPoint = initPoint[getIndex]
//        var initPoint:[Int] = []
        var getIndex = Int.random(in: 0...initPoint.count-1)
        var getPoint = initPoint[getIndex]
        let stuID = try req.parameters.next(UUID.self)
        print("stuID=\(stuID)")
        let calendar = Calendar.current
        var isGetLuckday = false
        return Point.query(on: req).filter(\Point.studentID==stuID).filter(\Point.pointReason == "LuckyDay").all().flatMap(to: GetLuckyDay.self){
            points in
            for point in points {
                var pointDate = point.getPointDate
                pointDate = Calendar.current.date(byAdding: .hour, value: +8, to: pointDate) ?? pointDate
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd"
                let pointDateString = dateFormatter.string(from: pointDate)
                var todayDate = Date()
                todayDate = Calendar.current.date(byAdding: .hour, value: +8, to: todayDate) ?? todayDate
                let todayString = dateFormatter.string(from: todayDate)
                if pointDateString == todayString {
                    isGetLuckday = true
                }
                print("\(Date()) pointDate=\(pointDateString) is today \(todayString) ? isGetLuckday=\(isGetLuckday)")

            }
            if isGetLuckday {
                throw Abort(.custom(code: 612, reasonPhrase: "Already got luckday points."))
            }
            else {
                return self.getLuckyDayPoint(req).flatMap(to: GetLuckyDay.self){
                    point in
                    print("key count = \(point.perPoint.keys.count)")
                    if point.perPoint.keys.count > 0 {
                        var numberRange:[Int] = []
                        var number = 0.0
                        for key in point.perPoint.keys {
                            let getPercent = point.perPoint[key] ?? 0.0
                            number +=  getPercent * 100
                            numberRange.append(Int(number))
                        }
                        print(numberRange)
                        let random = Int.random(in: 0...100)
                        print(random)
                        if random <= Int(number) {
                            for (index, element) in numberRange.enumerated() {
                                if random <= element {
                                    getPoint = Array(point.perPoint)[index].key
                                    getIndex = initPoint.index(of: getPoint) ?? 1
                                }
                            }
                        }
                        else{
                            if point.initPoint.count <= 0 {
                                getIndex = 1
                                getPoint = 0
                            }
                            else {
                                getPoint = point.initPoint[Int.random(in: 0...point.initPoint.count-1)]
                                getIndex = initPoint.index(of: getPoint) ?? 1
                            }
                        }
                    }
                    else {
                        if point.initPoint.count <= 0 {
                            getIndex = 1
                            getPoint = 0
                        }
                        else {
                            getPoint = point.initPoint[Int.random(in: 0...point.initPoint.count-1)]
                            getIndex = initPoint.index(of: getPoint) ?? 1
                        }
                    }
                    
                    
                    let pointTable = Point(pointReason: "LuckyDay", getPoint: getPoint, getPointDate: Date(), studentID: stuID)
                    let savePoint = pointTable.save(on: req)
                    
                    if getPoint == -1{
                        _ = savePoint.flatMap(to: Response.self){
                            rtnpoint in
                            let lottery = Lottery(getLotteryDate: Date(), isUsedDate: Date(), isUsed: false, pointID: rtnpoint.id!, studentID: stuID)
//                            ,type: 0)
                            return try req.client().post("http://localhost:8080/api/lottery"){ post in
                                try post.content.encode(lottery)
                            }
                        }
                    }
                    
                    var totalP: Int = 0
                    return Point.query(on: req)
                        .filter(\Point.getPoint != -1)
                        .filter(\Point.studentID == stuID)
                        .all()
                        .map(to: GetLuckyDay.self){ allPoints in
                            for ontPoint in allPoints {
                                totalP = totalP+ontPoint.getPoint
                            }
                            let luckyDay = GetLuckyDay(point: getPoint, index: getIndex, totalPoint: totalP)
                            print(luckyDay)
                            return luckyDay
                    }
                }
            }
        }
        
    }
    
    func getLuckyDayPoint(_ req: Request) -> Future<GetGamePoint>{
        var initPoint:[Int] = []
//        var initPoint = [Int:Double]()
        var perPoint = [Int:Double]()
//        var perPoint:[Int] = []
//        var perMonthTotal:[Int] = []
        var perMonthTotal = [Int:Int]()
        print("\(Date()) [GET]:api/point/getLuckyDay [FUNC]=getLuckyDayPointHandler-getLuckyDayPoint")
        return LuckyDay.query(on: req).sort(\LuckyDay.percent, .ascending).all().flatMap(to: GetGamePoint.self){
            luckyDayPoints in
            for luckyDay in luckyDayPoints {
                if luckyDay.percent < 0 {
                    initPoint.append(luckyDay.points)
//                    initPoint.updateValue(luckyDay.percent, forKey: luckyDay.points)
                }
                else {
                    perPoint.updateValue(luckyDay.percent, forKey: luckyDay.points)
                    perMonthTotal.updateValue(luckyDay.monthTotal, forKey: luckyDay.points)
//                    perPoint.append(luckyDay.points)
//                    perMonthTotal.append(luckyDay.monthTotal)
                }
            }
            
            //判斷當月中獎人數 超過就不在perPoint裡
            var countTotal = 0
            var checkPoint = -100
            return Point.query(on: req).filter(\Point.pointReason=="LuckyDay").sort(\Point.getPoint, .descending).all().map(to: GetGamePoint.self){
                points in
                if perPoint.count <= 0 {
                    return GetGamePoint(initPoint: [0,10], perPoint: [:])
                }
                for point in points {
                    if perPoint[point.getPoint] != nil {
                        
//                        if perPoint.contains(point.getPoint) {
                        if checkPoint == -100 {
                            checkPoint = point.getPoint
                        }
                        else if checkPoint != point.getPoint {
                            if countTotal >= perMonthTotal[checkPoint] ?? 0 {
                                perPoint.removeValue(forKey: checkPoint)
                                perMonthTotal.removeValue(forKey: checkPoint)
                            }
                            checkPoint = point.getPoint
                            countTotal = 0
                        }
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyyMM"
                        let getDate = dateFormatter.string(from: point.getPointDate)
                        let now = dateFormatter.string(from: Date())
                        if getDate == now {
                            countTotal += 1
                        }
                    }
                }
                if countTotal >= perMonthTotal[checkPoint] ?? 0 {
                    perPoint.removeValue(forKey: checkPoint)
                    perMonthTotal.removeValue(forKey: checkPoint)
                }
                
                return GetGamePoint(initPoint: initPoint, perPoint: perPoint)
            }
        }
    }
    
    func createHandler(
        _ req: Request,
        point: Point
        ) throws -> Future<Point> {
        print("\(Date()) [POST]:api/point [FUNC]=createHandler")
        point.getPointDate = Date()
        return point.save(on: req)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Point]> {
        return Point.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<Point> {
        return try req.parameters.next(Point.self)
    }
    
    func getStudentHandler(_ req: Request) throws -> Future<Student> {
        return try req
            .parameters.next(Point.self)
            .flatMap(to: Student.self) { point in
                point.student.get(on: req)
        }
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Point.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    func getAllPointHandler(_ req: Request) throws -> Future<GetPointStruct> {
        let stuID = try req.parameters.next(UUID.self)
        print("\(Date()) [GET]:api/point/getAllPoints/\(stuID) [FUNC]=getAllPointHandler")
        var totalP: Int = 0
        let fpushall = Point.query(on: req)
            .filter(\Point.getPoint != -1)
            .filter(\Point.studentID == stuID)
            .all()
            .map(to: GetPointStruct.self){ points in
                for point in points {
                    totalP = totalP+point.getPoint
                }
//                var context = GetPointStruct(point: points.isEmpty ? nil : points, totalPoint: totalP)
                var context = GetPointStruct(point: points, totalPoint: totalP)
                context.point = context.point.sorted(by: {$0.getPointDate.compare($1.getPointDate) == .orderedDescending})
                return context
            }
    return fpushall
    }
}

struct GetPointStruct: Content, Encodable {
    var point: [Point]
    let totalPoint: Int
}

struct GetLuckyDay: Content, Encodable {
    var point: Int
    var index: Int
    var totalPoint: Int
}

struct GetGamePoint: Content, Encodable {
    let initPoint:[Int]
    let perPoint : Dictionary<Int, Double>
}
struct PointContext: Encodable {
    let title: String
    let points: [PointStudent]?
}
struct PointStudent: Encodable {
    let id: UUID?
    let studentID: String
    let studentName: String
    let getPoint: Int
    let pointReason: String
    let getPointDate: Date
}
struct CreatePoint: Content, Encodable {
    let studentID: String
    let point: Int
    let reason: String
}
