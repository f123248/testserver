import Vapor
import Fluent

struct RecommendCourseController: RouteCollection {
    func boot(router: Router) throws {
        let recommendCourseRoute = router.grouped("api","recommendcourse")
        recommendCourseRoute.post(RecommendCourse.self, use: createHandler)
        recommendCourseRoute.get(use: getAllHandler)
        recommendCourseRoute.get(RecommendCourse.parameter, use:getHandler)
        recommendCourseRoute.put("updaterecommendcourse",RecommendCourse.parameter, use:updateRecommendCourseHandler)
        recommendCourseRoute.delete("deleterecommendcourse",RecommendCourse.parameter, use: deleteHandler)
        recommendCourseRoute.get("search", use: search)
        recommendCourseRoute.post(RecommendCourse.parameter,"voteStudents",Student.parameter, use: addVoteStudentsHandler)
        recommendCourseRoute.get(RecommendCourse.parameter,"voteStudents", use: getVoteStudentsHandler)
        
        let webRoute = router
        webRoute.get("pushCourse", use: pushCourseViewHandler)
        webRoute.get("passCourse", use: passCourseViewHandler)
        webRoute.get("passCourseEdit",RecommendCourse.parameter, use: passCourseEditViewHandler)
        webRoute.put("passCourseEdit",RecommendCourse.parameter, use: updatePassCourseHandler)
        webRoute.put("passCourse",RecommendCourse.parameter, use: updateStateHandler)
        webRoute.get("iVoting", use: iVotingViewHandler)
        webRoute.put("iVoting", RecommendCourse.parameter, use: updateStateHandler)
        webRoute.get("iVotingView", RecommendCourse.parameter, use: iVotingViewVoteHandler)
        webRoute.get("iVotingEdit",RecommendCourse.parameter, use: iVotingEditViewHandler)
        webRoute.put("iVotingEdit",RecommendCourse.parameter, use: updatePassCourseHandler)
        webRoute.get("iVotingReject", RecommendCourse.parameter, use: iVotingRejectViewHandler)
        webRoute.put("iVotingReject", RecommendCourse.parameter, use: updateRejectReasonHandler)
        webRoute.get("votingResult", use: votingResultViewHandler)
        webRoute.get("votingResultEdit", RecommendCourse.parameter, use: votingResultEditViewHandler)
        webRoute.get("pushReject", RecommendCourse.parameter, use: pushRejectViewHandler)
        webRoute.put("pushCourse", RecommendCourse.parameter, use: updateHandler)
        webRoute.get("courseDecline", use: courseDeclineViewHandler)
        webRoute.put("pushReject", RecommendCourse.parameter, use: updateRejectReasonHandler)
        webRoute.patch("changeState", RecommendCourse.parameter, use: changeVerifyStateHandler)
    }
    
 
    
    func updateHandler(_ req: Request) throws -> Future<RecommendCourse> {
        let now = Date()
        return try flatMap(to: RecommendCourse.self,
                           req.parameters.next(RecommendCourse.self),
                           req.content.decode(RecommendCourse.self)) {
                            recommendCourse, updatedCourse in
                            recommendCourse.verifyState = updatedCourse.verifyState
                            recommendCourse.verifiedTime = now
                            return recommendCourse.save(on: req)
        }
    }
    
    func updateStateHandler(_ req: Request) throws -> Future<RecommendCourse> {
        return try flatMap(to: RecommendCourse.self,
                           req.parameters.next(RecommendCourse.self),
                           req.content.decode(RecommendCourse.self)) {
                            recommendCourse, updatedCourse in
                            recommendCourse.verifyState = updatedCourse.verifyState
                            return recommendCourse.save(on: req)
        }
    }

    
    func updatePassCourseHandler(_ req: Request) throws -> Future<RecommendCourse> {
        return try flatMap(to: RecommendCourse.self,
                           req.parameters.next(RecommendCourse.self),
                           req.content.decode(RecommendCourse.self)) {
                            recommendCourse, updatedCourse in
                            recommendCourse.courseName = updatedCourse.courseName
                            recommendCourse.courseIntroduction = updatedCourse.courseIntroduction
                            recommendCourse.voteStartTime = updatedCourse.voteStartTime
                            recommendCourse.voteEndTime = updatedCourse.voteEndTime
                            return recommendCourse.save(on: req)
        }
    }
    
    func updateRejectReasonHandler(_ req: Request) throws -> Future<RecommendCourse> {
        let now = Date()
        return try flatMap(to: RecommendCourse.self,
                           req.parameters.next(RecommendCourse.self),
                           req.content.decode(RecommendCourse.self)) {
                            recommendCourse, updatedCourse in
                            recommendCourse.verifyState = updatedCourse.verifyState
                            recommendCourse.verificationRejectedReason = updatedCourse.verificationRejectedReason
                            recommendCourse.verifiedTime = now
                            return recommendCourse.save(on: req)
        }
    }
    
//    func dateNow() -> Date{
//        let now = Date()
//
//        let dateformatter = DateFormatter()
//        dateformatter.dateFormat = "yyyy-MM-dd HH:mm"
//        let dateString = dateformatter.string(from: now)
//        guard let val = dateformatter.date(from: dateString) else{return now}
//        guard let date = Calendar.current.date(byAdding: .hour, value: 8, to: val) else{return val}
//
//        return now
//    }
    
    func changeVerifyStateHandler(req: Request) throws -> Future<RecommendCourse> {
        let course = try req.parameters.next(RecommendCourse.self)
        let content = try req.content.decode(RecommendCourse.self)
        
        return flatMap(to: RecommendCourse.self, course, content) { (course, content) in
            course.verifyState = content.verifyState
            return course.save(on: req)
        }
    }
    
    
    
    
    
    func pushCourseViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "pushCourse")
    }
    func passCourseViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "passCourse")
    }
    func passCourseEditViewHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(RecommendCourse.self).flatMap({ recommendCourse in
            return try req.view().render("passCourseEdit", recommendCourse)
        })
    }
    func iVotingEditViewHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(RecommendCourse.self).flatMap({ recommendCourse in
            return try req.view().render("iVotingEdit", recommendCourse)
        })
    }
    func iVotingRejectViewHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(RecommendCourse.self).flatMap({ recommendCourse in
            return try req.view().render("iVotingReject", recommendCourse)
        })
    }
    func iVotingViewHandler(_ req: Request) throws -> Future<View> {
        return RecommendCourse.query(on: req).sort(\RecommendCourse.id).all().flatMap({ courses in
            var allCourses:[RecommendCourse] = []
            var studentCount = 0
            for course in courses{
                allCourses.append(course)
                _ = VotePivot.query(on: req).filter(\VotePivot.recommendcourseID == course.id!).all().map({
                    votes -> Int in
                    for _ in votes {
                        studentCount += 1
                    }
                    allCourses.last?.numberOfVoters = studentCount
                    return 0
                })
            }
            let context = RecommendCourseContext(title: "RecommendCourse", recommendCourses: allCourses.isEmpty ? nil : allCourses)
            return try req.view().render("iVoting", context)
        })
    }
    func votingResultViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "votingResult")
    }
    func votingResultEditViewHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(RecommendCourse.self).flatMap({ recommendCourse in
            return try req.view().render("votingResultEdit", recommendCourse)
        })
    }
    func courseDeclineViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "courseDecline")
    }
    
    func handler(req: Request,source:String) -> Future<View> {
        return RecommendCourse.query(on: req).all().flatMap({ recommendCourses in
            let context = RecommendCourseContext(title: "RecommendCourse", recommendCourses: recommendCourses.isEmpty ? nil : recommendCourses)
            return try req.view().render(source, context)
        })
    }
    
    func iVotingViewVoteHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(RecommendCourse.self).flatMap({ recommendCourse in
            var voteResults:[VoteResult] = []
            var totalCourseWeek = [0,0,0,0,0,0,0]
            return try recommendCourse.voteStudents.pivots(on: req).join(\Student.id, to: \VotePivot.studentID).alsoDecode(Student.self).all().flatMap({
                pivots in
                for pivot in pivots{
                    let studentID = pivot.1.studentID
                    let studentName = pivot.1.studentName
                    let courseWeek = pivot.0.courseWeek
                    let voteTime = pivot.0.voteTime
                    var i = 0
                    for weekDay in courseWeek{
                        totalCourseWeek[i] += weekDay
                        i += 1
                    }
                    voteResults.append(VoteResult(studentID: studentID, studentName: studentName, courseWeek: courseWeek, voteTime: voteTime))
                }
                let context = VoteResultContext(title: "VoteResult", courseName: recommendCourse.courseName ,totalCourseWeek: totalCourseWeek, voteResults: voteResults)
                return try req.view().render("iVotingView", context)
            })
        })
    }
    func createHandler(
        _ req: Request,
        recommendCourse: RecommendCourse
        ) throws -> Future<RecommendCourse> {
        
        //app 推薦之後 初始投票時間、審核時間
//        let timeInterval:TimeInterval = 0
//        let date = Date(timeIntervalSince1970: timeInterval)
//        recommendCourse.verifiedTime = date
//        recommendCourse.voteStartTime = date
//        recommendCourse.voteEndTime = date

        let newstring = recommendCourse.recommendReason.trimmingCharacters(in: .newlines)
        recommendCourse.recommendReason = newstring
        recommendCourse.recommendTime = Date()
        return recommendCourse.save(on: req)
    }
    
//    func addRecommendCoursesHandler(
//        _ req: Request
//        ) throws -> Future<HTTPStatus> {
//        return try flatMap(
//            to:HTTPStatus.self,
//            req.parameters.next(Student.self),
//            req.parameters.next(RecommendCourse.self)) { student, recommendcourse in
//                return student.recommendCourses
//                    .attach(recommendcourse, on: req)
//                    .transform(to: .created)
//        }
//    }
    
    func getAllHandler(_ req:Request) throws -> Future<[RecommendCourse]> {
        return RecommendCourse.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<RecommendCourse> {
        return try req.parameters.next(RecommendCourse.self)
    }
    
    func pushRejectViewHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(RecommendCourse.self).flatMap({ recommendCourse in
            return try req.view().render("pushReject", recommendCourse)
        })
    }

    
    func updateRecommendCourseHandler(_ req: Request) throws -> Future<RecommendCourse> {
        return try flatMap(to: RecommendCourse.self,
                           req.parameters.next(RecommendCourse.self),
                           req.content.decode(RecommendCourse.self)) {
                            recommendcourse, updatedRecommendCourse in
                            recommendcourse.courseName = updatedRecommendCourse.courseName
                            recommendcourse.courseType = updatedRecommendCourse.courseType
                            recommendcourse.recommendReason = updatedRecommendCourse.recommendReason
                            recommendcourse.courseIntroduction = updatedRecommendCourse.courseIntroduction
                            recommendcourse.proposerID = updatedRecommendCourse.proposerID
                            recommendcourse.voteStartTime = updatedRecommendCourse.voteStartTime
                            recommendcourse.voteEndTime = updatedRecommendCourse.voteEndTime
                            recommendcourse.recommendTime = updatedRecommendCourse.recommendTime
                            recommendcourse.verifiedTime = updatedRecommendCourse.verifiedTime
                            recommendcourse.verifyState = updatedRecommendCourse.verifyState
                            recommendcourse.verificationRejectedReason = updatedRecommendCourse.verificationRejectedReason
                            
                            recommendcourse.numberOfVoters = updatedRecommendCourse.numberOfVoters
                            return recommendcourse.save(on: req)
        }
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
            return try req.parameters.next(RecommendCourse.self)
                .delete(on: req)
                .transform(to: HTTPStatus.noContent)
    }
    
    func search(_ req: Request) throws -> Future<[RecommendCourse]> {
        
        guard let searchTerm:Int = req.query[Int.self, at: "term"] else {
            throw Abort(.badRequest)
        }
        
        return RecommendCourse.query(on: req).filter(\.verifyState == searchTerm ).all()
    }
    
    func addVoteStudentsHandler(
        _ req: Request
        ) throws -> Future<HTTPStatus> {
        
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(RecommendCourse.self),
            req.parameters.next(Student.self)) { recommendcourse, student in
                
                return recommendcourse.voteStudents
                    .attach(student, on: req)
                    .transform(to: .created)
        }
    }
    
    
    func getVoteStudentsHandler(
        _ req: Request
        ) throws -> Future<[Student]> {
        
        return try req.parameters.next(RecommendCourse.self)
            .flatMap(to: [Student].self) { recommendcourse in
                
                try recommendcourse.voteStudents.query(on: req).all()
        }
    }

}

struct RecommendCourseContext: Encodable {
    let title: String
//    let courseName: String
//    let courseType: Int
//    let campus: Int
//    let recommendReason: String
//    let courseIntroduction: String
//    let proposerID: UUID?
//    let voteStartTime: Date
//    let voteEndTime: Date
//    let recommendTime: Date
//    let verifiedTime: Date
//    let verifyState: Int
//    let verificationRejectedReason: String
//    let numberOfVoters: Int
    let recommendCourses: [RecommendCourse]?
//    let studentID: String
//    let studentName: String
}

struct VoteResult: Encodable {
    var studentID: String
    var studentName: String
    var courseWeek: [Int]
    var voteTime: Date
}

struct VoteResultContext: Encodable {
    var title: String
    var courseName: String
    var totalCourseWeek:[Int]
    var voteResults: [VoteResult]?
}
