//
//  AdminController.swift
//  App
//
//  Created by hsu on 2019/2/12.
//

import Vapor
import Fluent
import CryptoSwift

struct AdminController: RouteCollection {
    func boot(router: Router) throws {
        let adminRoute = router.grouped("api", "admin")
        adminRoute.post(Admin.self, use: createHandler)
//        let webRoute = router
//        webRoute.post("loginCheck", Admin.parameter, use: loginHandler)
        let loginRoutes = router.grouped("loginCheck")
        loginRoutes.post(Admin.self, use: loginHandler)
        
    }
    
    func createHandler(_ req: Request,newAdmin: Admin) throws -> Future<Admin> {
        print("\(Date()) [POST]:/api/admin newAdmin-account=\(newAdmin.account) password=\(newAdmin.password)")
        return Admin.query(on: req).all().flatMap(to: Admin.self) { allAdmins in
            for admin in allAdmins {
                if newAdmin.account == admin.account {
                    throw Abort(.custom(code: 602, reasonPhrase: "a user with this account already exists"))
                }
            }
            let hashedPassword = newAdmin.password.md5()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMdd"
            let dateString = dateFormatter.string(from: Date())
            let token = dateString.md5()
            let persistedUser = Admin(account: newAdmin.account, password: hashedPassword, token: token)
            return persistedUser.save(on: req)
        }
    }
    
    func loginHandler(_ req: Request, userInput: Admin) throws -> Future<View> {
        print("\(Date()) [POST]:/loginCheck userInput-account=\(userInput.account) password=\(userInput.password)")
        let account = userInput.account
        let password = userInput.password
        var isSuccess:Bool = false
        return Admin.query(on : req).all().flatMap(to: View.self) { allAdmins in
            for admin in allAdmins {
                if account == admin.account {
                    if password.md5() == admin.password {
                        isSuccess = true
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyyMMdd"
                        let dateString = dateFormatter.string(from: Date())
                        let token = dateString.md5()
                        admin.token = token
                        _ = admin.save(on: req)
                    }
                }
            }
            if isSuccess{
                
                return try req.view().render("loginSuccess")
            }
            else{
                return try req.view().render("loginFail")
            }
        }
    }

}
