//
//  NotificationController.swift
//  App
//
//  Created by hsu on 2019/5/21.
//

import Vapor
import Fluent
import CryptoSwift

struct NotificationController: RouteCollection {
    func boot(router: Router) throws {
        let noticeRoute = router.grouped("api", "notification")
        noticeRoute.post(Notification.self, use: createAllHandler)//一般公告
        noticeRoute.post(Notification.self,at: "course",Course.parameter, use: createWithCourseHandler) //課程群組
        noticeRoute.post(Notification.self,at: "student",String.parameter, use: createWithStudentHandler) //個人通知
        
        let webRoute = router
        webRoute.get("noticeNormal", use: noticeNormalViewHandler)
        webRoute.get("noticeCourse", use: noticeCourseViewHandler)
        webRoute.get("noticeStudent", use: noticeStudentViewHandler)
        webRoute.get("noticeNormalCreate", use: noticeNormalCreateViewHandler)
        webRoute.get("noticeCourseCreate", use: noticeCourseCreateViewHandler)
        webRoute.get("noticeStudentCreate", use: noticeStudentCreateViewHandler)
        webRoute.get("helloworld", use: testHttpHandler)
        
        let studentRoute = router.grouped("api", "student")
        studentRoute.get("getNormalNotice", use: getNormalNoticeHandler)
        studentRoute.get("getCourseNotice",Student.parameter, use: getCourseNoticeHandler)
        studentRoute.get("getStudentNotice",Student.parameter, use: getStudentNoticeHandler)
    }
    func testHttpHandler(
        _ req: Request
        ) throws -> Future<Response> {
        let sendToke = SendToken(token: "asdf", title: "123", message: "hello world", type: "0")
        let res = try req.client().post("http://localhost:3000/token"){ post in
            try post.content.encode(sendToke)
        }
        return res
    }
    func noticeNormalViewHandler(req: Request) -> Future<View> {
        return Notification.query(on: req).filter(\Notification.type==0).sort(\Notification.pushTime, ._descending).all().flatMap({ notices in
            let context = NoticeContext(title: "一般公告", notices: notices.isEmpty ? nil : notices)
            return try req.view().render("notification",context)
        })
    }
    
    func noticeCourseViewHandler(req: Request) -> Future<View> {
        return Notification.query(on: req).filter(\Notification.type==1).sort(\Notification.pushTime, ._descending).all().flatMap({ notices in
            let context = NoticeContext(title: "課程群組", notices: notices.isEmpty ? nil : notices)
            return try req.view().render("notification",context)
        })
    }
    
    func noticeStudentViewHandler(req: Request) -> Future<View> {
        return NoticeStudentPivot.query(on: req).join(\Notification.id, to: \NoticeStudentPivot.notificationID).join(\Student.id, to: \NoticeStudentPivot.studentID ).filter(\Notification.type==2).alsoDecode(Notification.self).alsoDecode(Student.self).sort(\Notification.pushTime, ._descending).all().flatMap({ notices in
            var noticeStudent:[NoticeStudent] = []
            for notice in notices {
                let id = notice.0.1.id
                let title = notice.0.1.title
                let content = notice.0.1.content
                let type = notice.0.1.type
                let pushTime = notice.0.1.pushTime
                let studentID = notice.1.studentID
                let studentName = notice.1.studentName
                let oneStudent = NoticeStudent(id: id, title: title, content: content, type: type, pushTime: pushTime, studentID: studentID, studentName: studentName)
                noticeStudent.append(oneStudent)
            }
            let context = NoticeStudentContext(title: "個人通知", notices: noticeStudent.isEmpty ? nil : noticeStudent)
            return try req.view().render("noticeStudent",context)
        })
    }
    
    func noticeNormalCreateViewHandler(req: Request) -> Future<View> {
        return Notification.query(on: req).filter(\Notification.type==0).all().flatMap({ notices in
            let context = NoticeContext(title: "一般公告", notices: notices.isEmpty ? nil : notices)
            return try req.view().render("noticeNormalCreate",context)
        })
    }
    
    func noticeCourseCreateViewHandler(req: Request) -> Future<View> {
        return Course.query(on: req).filter(\Course.courseState==1).all().flatMap({ courses in
            let context = IndexContext(title: "Courses", courses: courses.isEmpty ? nil : courses)
            return try req.view().render("noticeCourseCreate", context)
        })
    }
    
    func noticeStudentCreateViewHandler(req: Request) -> Future<View> {
        return Course.query(on: req).filter(\Course.courseState==2).all().flatMap({ courses in
            let context = IndexContext(title: "Courses", courses: courses.isEmpty ? nil : courses)
            return try req.view().render("noticeStudentCreate",context)
        })
    }
    
    func createAllHandler(
        _ req: Request,
        notice: Notification
        ) throws -> Future<Notification> {
        return notice.save(on: req).map({
            oneNotice -> Notification in
            _ = Student.query(on: req).all().map({
                students -> Notification in
                for student in students{
                    if student.pushToken != "" {
                        var type = "0"
                        if student.pushToken.contains(":") {
                            type = "1"
                        }
                        let sendToke = SendToken(token: student.pushToken, title: notice.title, message: notice.content, type: type)
                        _ = try req.client().post("http://localhost:3000/token"){ post in
                            try post.content.encode(sendToke)
                        }
                    }
                    _ = student.noticeStudent.attach(oneNotice, on: req)
                }
                return oneNotice
            })
            return notice
        })
    }
    
    func createWithCourseHandler(
        _ req: Request,
        notice: Notification
        ) throws -> Future<Notification> {
        return notice.save(on: req).map({
            oneNotice -> Notification in
            _ = try req.parameters.next(Course.self).map({
                course in
                print("\(Date()) [POST]:api/notification/course/\(String(describing: course.id)) [FUNC]=createWithCourseHandler")
                notice.courseType = course.courseType
                _ = notice.save(on: req)
                _ = try course.inCourseStudents.query(on: req).all().map({
                    students -> Course in
                    for student in students{
                        if student.pushToken != "" {
                            var type = "0"
                            if student.pushToken.contains(":") {
                                type = "1"
                            }
                            
                            let sendToke = SendToken(token: student.pushToken, title: notice.title, message: notice.content, type: type)
                            let rtn = try req.client().post("http://localhost:3000/token"){ post in
                                try post.content.encode(sendToke)
                            }
                            print("\(Date()) [POST] localhost:300/token student=\(student.studentID) rtn=\(rtn)")
                        }
                        else {
                            print("[ERROR] student \(student.studentID) pushToken is nill.")
                        }
                        
                        _ = student.noticeStudent.attach(oneNotice, on: req)
                    }
                    return course
                })
            })
            return notice
        })
    }
    
    func createWithStudentHandler(
        _ req: Request,
        notice: Notification
        ) throws -> Future<HTTPStatus> {
        return notice.save(on: req).flatMap(to: HTTPStatus.self){
            oneNotice in
            let studentID = try req.parameters.next(String.self)
            var success = false
            print("\(Date()) [POST]:api/notification/student/\(studentID) [FUNC]=createWithStudentHandler")
            return Student.query(on: req)
                .filter(\Student.studentID==studentID)
                .all()
                .map(to: HTTPStatus.self){
                    students in
                    for student in students{
                        if student.studentID == studentID{
                            success = true
                            if student.pushToken != "" {
                                var type = "0"
                                if student.pushToken.contains(":") {
                                    type = "1"
                                }
                                
//                                var testToken1 = "5a16914b728a9e9aa24895b9dfc1d6f1fe6831af2f2d7a3c8289714a1d25cddf"
//                                type="0"
                                let sendToke = SendToken(token: student.pushToken, title: notice.title, message: notice.content, type: type)
                                let rtn = try req.client().post("http://localhost:3000/token"){ post in
                                    try post.content.encode(sendToke)
                                }
                                print("\(Date()) [POST] localhost:3000/token rtn=\(rtn)")
                            }
                            else {
                                print("[ERROR] student \(student.studentID) pushToken is nill.")
                            }
                            _ = student.noticeStudent.attach(oneNotice, on: req)
                        }
                    }
                    if success{
                        throw Abort(.custom(code: 200, reasonPhrase: "Success"))
                    }
                    else{
                        throw Abort(.custom(code: 604, reasonPhrase: "Not found StudentID"))
                    }
            }
        }
    }
    func getNormalNoticeHandler(
        _ req: Request
        ) throws -> Future<[Notification]> {
        return Notification.query(on: req).filter(\Notification.type==0).sort(\Notification.pushTime, ._descending).all()
    }
    
    func getCourseNoticeHandler(
        _ req: Request
        ) throws -> Future<[Notification]> {
        return try req.parameters.next(Student.self)
            .flatMap(to: [Notification].self) { student in
                try student.noticeStudent.query(on: req).filter(\Notification.type==1).sort(\Notification.pushTime, ._descending).all()
        }
    }
    
    func getStudentNoticeHandler(
        _ req: Request
        ) throws -> Future<[Notification]> {
        // 2
        return try req.parameters.next(Student.self)
            .flatMap(to: [Notification].self) { student in
                // 3
                try student.noticeStudent.query(on: req).filter(\Notification.type==2).sort(\Notification.pushTime, ._descending).all()
        }
    }
}

struct NoticeContext: Encodable {
    let title: String
    let notices: [Notification]?
}

struct NoticeStudentContext: Encodable {
    let title: String
    let notices: [NoticeStudent]?
}

struct NoticeStudent: Encodable {
    let id: UUID?
    let title: String
    let content: String
    let type: Int
    let pushTime: Date
    let studentID: String
    let studentName: String
}

struct SendToken: Encodable, Content {
    let token: String
    let title: String
    let message: String
    let type: String
}
