import Vapor
import Fluent
import CryptoSwift

struct LicenseGradeTimeController: RouteCollection {
    func boot(router: Router) throws {
        let lgtRoute = router.grouped("api", "setTime")
        lgtRoute.get("getLicenseTime", use: getLicenseTimeHandler)
        lgtRoute.get("getGradeTime", use: getGradeTimeHandler)
        
        let webRoute = router
        
        webRoute.get("setLicenseTime", use: setLicenseTimeViewHandler)
        webRoute.get("setLicenseTimeEdit", use: setLicenseTimeEditViewHandler)
        webRoute.get("setGradeTime", use: setGradeTimeViewHandler)
        webRoute.get("setGradeTimeEdit", use: setGradeTimeEditViewHandler)
        webRoute.post(LicenseGradeTime.self, at: "lgSetTime", use: createHandler)
        webRoute.delete("deleteSetTime",LicenseGradeTime.parameter, use: deleteHandler)
        
    }
    
    // setLicenseTime
    func setLicenseTimeViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "setLicenseTime")
    }
    
    // setLicenseTimeEdit
    func setLicenseTimeEditViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "setLicenseTimeEdit")
    }
    
    // setGradeTime
    func setGradeTimeViewHandler(_ req: Request) throws -> Future<View> {
        return handler1(req: req, source: "setGradeTime")
    }
    
    // setGradeTimeEdit
    func setGradeTimeEditViewHandler(_ req: Request) throws -> Future<View> {
        return handler1(req: req, source: "setGradeTimeEdit")
    }
    
        
    func createHandler(
        _ req: Request,
        licenseGradeTime: LicenseGradeTime
        ) throws -> Future<LicenseGradeTime> {
        return licenseGradeTime.save(on: req)
    }
    
    func handler(req: Request,source:String) -> Future<View> {
        return LicenseGradeTime.query(on: req)
            .filter(\LicenseGradeTime.state == 0)
            .all().flatMap({ lgt in
            let context = SetLicenseGradeStruct(title: "title", setLicenseGrade: lgt.isEmpty ? nil : lgt)
            return try req.view().render(source, context)
        })
    }
    
    func handler1(req: Request,source:String) -> Future<View> {
        return LicenseGradeTime.query(on: req)
            .filter(\LicenseGradeTime.state == 1)
            .all().flatMap({ lgt in
                let context = SetLicenseGradeStruct(title: "title", setLicenseGrade: lgt.isEmpty ? nil : lgt)
                return try req.view().render(source, context)
            })
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(LicenseGradeTime.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    // get license time
    func getLicenseTimeHandler(_ req: Request)
        throws -> Future<GetLicenseTimeStruct> {
            
            let timeInterval:TimeInterval = 0
            let date1 = Date(timeIntervalSince1970: timeInterval)
            
            return LicenseGradeTime.query(on: req)
                .filter(\LicenseGradeTime.state == 0)
                .filter(\LicenseGradeTime.endTime >= Date())
                .first()
                .map(to: GetLicenseTimeStruct.self) { lgt in
                    
                    if lgt == nil {
                        let pushOne = GetLicenseTimeStruct(startTime: date1, endTime: date1, status: 1)
                        return pushOne
                    }
                    else {
                        if(lgt!.startTime <= Date()) {
                            let pushOne = GetLicenseTimeStruct(startTime: lgt!.startTime, endTime: lgt!.endTime, status: 0)
                            return pushOne
                        }
                        else {
                            let pushOne = GetLicenseTimeStruct(startTime: lgt!.startTime, endTime: lgt!.endTime, status: 1)
                            return pushOne
                        }
                    }
            }
    }
    
    // get grade time
    func getGradeTimeHandler(_ req: Request)
        throws -> Future<GetLicenseTimeStruct> {
            
            let timeInterval:TimeInterval = 0
            let date1 = Date(timeIntervalSince1970: timeInterval)
            
            return LicenseGradeTime.query(on: req)
                .filter(\LicenseGradeTime.state == 1)
                .filter(\LicenseGradeTime.endTime >= Date())
                .first()
                .map(to: GetLicenseTimeStruct.self) { lgt in
                    
                    if lgt == nil {
                        let pushOne = GetLicenseTimeStruct(startTime: date1, endTime: date1, status: 1)
                        return pushOne
                    }
                    else {
                        if(lgt!.startTime <= Date()) {
                            let pushOne = GetLicenseTimeStruct(startTime: lgt!.startTime, endTime: lgt!.endTime, status: 0)
                            return pushOne
                        }
                        else {
                            let pushOne = GetLicenseTimeStruct(startTime: lgt!.startTime, endTime: lgt!.endTime, status: 1)
                            return pushOne
                        }
                    }
            }
    }
}

struct SetLicenseGradeStruct: Content, Encodable {
    var title: String
    var setLicenseGrade: [LicenseGradeTime]?
}

struct GetLicenseTimeStruct: Content, Encodable {
    var startTime: Date
    var endTime: Date
    var status: Int // 0: 可申請 1: 非申請時間
}
