
import Vapor
import Fluent

struct BannerController: RouteCollection {
    func boot(router: Router) throws {
        let webRoute = router
        webRoute.get("editBanner", use: editBannerViewHandler)
        webRoute.post(BannerImage.self, at: "editBanner", use: uploadBannerHandler)
        webRoute.get("index", use: indexViewHandler)
//        webRoute.delete("deleteBanner",String.parameter, use: deleteHandler)
        webRoute.put("changeIndex",Banner.parameter,"to",Int.parameter, use: changeIndexHandler)
        webRoute.post(NewIndex.self, at: "changeAllIndex", use: changeAllIndexHandler)
        webRoute.put("resetIndex", use: resetIndexHandler)
        webRoute.put("updateBanner", Banner.parameter, use: updateBannerHandler)
        webRoute.delete("deleteBanner",Banner.parameter, use: deleteHandler)
        
        let bannerRoute = router.grouped("api", "banner")
        bannerRoute.get(use: getAllHandler)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Banner2App]> {
        print("\(Date()) [GET]:/api/banner [FUNC]=getAllHandler")
        return Banner.query(on: req).filter(\Banner.sort >= 0).sort(\Banner.sort).all().map(to: [Banner2App].self){
            banners in
            var banner2App:[Banner2App] = []
            for banner in banners {
                let file = banner.pictureName
//                let folder = "Public/documents/images/banner/"
//                let directory = DirectoryConfig.detect()
//                let workPath = directory.workDir
                
                let bannerPath = "http://163.21.117.108/documents/images/banner/"
                let bannerfile = bannerPath+file
                
//                let saveURL = URL(fileURLWithPath: workPath+folder+file)
//                do{
                
//                    let imgData = try Data(contentsOf: saveURL)
                    let newBanner = Banner2App(id: banner.id!, title: banner.title, content: banner.content, picture: bannerfile, sort: banner.sort, webPage: banner.webPage)
                    banner2App.append(newBanner)
//                }
//                catch{
//                    throw Abort(.custom(code: 608, reasonPhrase: "Can't get image"))
//                }
            }
            print(banner2App)
            return banner2App
        }
    }
    
    func indexViewHandler(_ req: Request) throws -> Future<View> {
        print("\(Date()) [GET]:index [FUNC]=indexViewHandler")
        let folder = "/documents/images/banner/"
        
        return Banner.query(on: req).filter(\Banner.sort>=0).sort(\Banner.sort).all().flatMap(to: View.self){
            banners in
            var fileName:[BannerName] = []
            for banner in banners{
                let bannerName = BannerName(id: banner.id, title: banner.title, content: banner.content, filename: folder+banner.pictureName, sort: banner.sort, webPage: banner.webPage)
                fileName.append(bannerName)
            }
            let context = BannerImages(fileName: fileName.isEmpty ? [] : fileName)
            print(fileName)
            return try req.view().render("index",context)
        }
    }
    
    func editBannerViewHandler(_ req: Request) throws -> Future<View> {
        print("\(Date()) [GET]:editBanner [FUNC]=editBannerViewHandler")
        let folder = "/documents/images/banner/"
        return Banner.query(on: req).sort(\Banner.sort).all().flatMap(to: View.self){
            banners in
            var fileName:[BannerName] = []
            for banner in banners{
                let bannerName = BannerName(id: banner.id, title: banner.title, content: banner.content, filename: folder+banner.pictureName, sort: banner.sort, webPage: banner.webPage)
                fileName.append(bannerName)
            }
            let context = BannerImages(fileName: fileName.isEmpty ? [] : fileName)
            return try req.view().render("editBanner",context)
        }
    }
    
    func uploadBannerHandler(_ req: Request, bannerImage: BannerImage) throws -> Future<Banner> {
        print("\(Date()) [PUT]:editBanner [FUNC]=uploadBannerHandler")
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let folder = "Public/documents/images/banner"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddhhmmssSSS"
        let date = dateFormatter.string(from: Date())
        let file = "banner"+date+".jpg"
        
        let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
        
        do {
            print(saveURL)
            try bannerImage.image.write(to: saveURL)
            return Banner(title: bannerImage.title, content: bannerImage.content, pictureName: file, sort: bannerImage.sort, webPage: bannerImage.webPage).save(on: req)
        }
        catch {
            print("Cannot save Image")
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post image."))
        }
    }
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let folder = "Public/documents/images/banner"
        
        let fileManager = FileManager.default
        let path = workPath+folder
        
        return try req.parameters.next(Banner.self).map(to: Banner.self){
            banner in
            print("\(Date()) [DELETE]:deleteBanner/\(banner) [FUNC]=deleteHandler")
            print("content=\(banner.content) title=\(banner.title) sort=\(banner.sort) webPage=\(banner.webPage)")
            let filename = banner.pictureName
            try fileManager.removeItem(atPath: path+"/"+filename)
            return banner
            }
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    func changeIndexHandler(_ req: Request) throws -> Future<Banner> {
        return try req.parameters.next(Banner.self).flatMap(to: Banner.self){
            banner in
            banner.sort = try req.parameters.next(Int.self)
            print("\(Date()) [PUT]:change/\(banner)/to/\(banner.sort) [FUNC]=changeIndexHandler")
            return banner.save(on: req)
        }
    }
    
    func changeAllIndexHandler(_ req: Request, newIndex: NewIndex) throws -> Future<HTTPStatus> {
        print("\(Date()) [PUT]:changeAllIndex/\(newIndex) [FUNC]=changeAllIndexHandler")
        return Banner.query(on: req).sort(\Banner.sort).all().map(to: HTTPStatus.self){
            banners in
            var count = 0
            for banner in banners{
                banner.sort = Int(newIndex.newIndex[count]) ?? 0
                _ = banner.save(on: req)
                count += 1
            }
            return HTTPStatus.ok
        }
    }
    
    func resetIndexHandler(_ req: Request) throws -> HTTPStatus{
        print("\(Date()) [PUT]:resetIndex [FUNC]=resetIndexHandler")
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let folder = "Public/documents/images/banner"
        
        let path = workPath+folder
        
        let documentDirectory = URL(fileURLWithPath: path)
        
        if var allItems = try? FileManager.default.contentsOfDirectory(atPath: path) {
            allItems = allItems.sorted { $0 < $1 }
            for name in allItems{
                let originPath = documentDirectory.appendingPathComponent(name)
                let nameIndex = allItems.firstIndex(of: name)
                let index = String(describing: nameIndex ?? 0)
                let destinationPath = documentDirectory.appendingPathComponent("banner"+index+".jpg")
                try FileManager.default.moveItem(at: originPath, to: destinationPath)
            }
        }
        return HTTPStatus.ok
    }
    
    func updateBannerHandler(_ req:Request)throws -> Future<Banner>{
        return try flatMap(to: Banner.self,req.parameters.next(Banner.self),req.content.decode(Banner.self)) {
            banner, updatedBanner in
            print("\(Date()) [PUT]:updateBanner/\(updatedBanner) [FUNC]=updateBannerHandler")
            print("content=\(updatedBanner.content) title=\(updatedBanner.title) sort=\(updatedBanner.sort) webPage=\(updatedBanner.webPage)")
            banner.content = updatedBanner.content
            banner.title = updatedBanner.title
            banner.sort = updatedBanner.sort
            banner.webPage = updatedBanner.webPage
            return banner.save(on: req)
        }
    }
}

struct BannerImage: Content {
    let title: String
    let content: String
    let image: Data
    let sort: Int
    let webPage: String
}

struct BannerImages: Content {
    let fileName: [BannerName]
}

struct BannerName: Content {
    let id: UUID?
    let title: String
    let content: String
    let filename: String
    let sort: Int
    let webPage: String
}

struct NewIndex: Content,Encodable {
    let newIndex: [String]
}

struct Banner2App: Content {
    let id: UUID
    let title: String
    let content: String
//    let picture: Data
    let picture: String
    let sort: Int
    let webPage: String
}
