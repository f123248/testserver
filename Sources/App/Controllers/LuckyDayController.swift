
import Vapor
import Fluent

struct LuckyDayController: RouteCollection {
    func boot(router: Router) throws {
        let lucyDayRoute = router.grouped("api", "luckyDay")
        lucyDayRoute.post(LuckyDay.self, use: createHandler)
        lucyDayRoute.put("updateLuckyDay",LuckyDay.parameter, use: updateHandler)
        lucyDayRoute.delete("deleteLuckyDay", LuckyDay.parameter, use: deleteHandler)
        
        let webRoute = router
        webRoute.get("setLuckyDay", use: getAllLuckyDayHandler)
        webRoute.get("editLuckyDay", LuckyDay.parameter, use: editLuckyDayHandler)
    }
    
    func editLuckyDayHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(LuckyDay.self).flatMap({ luckyDay in
            return try req.view().render("editLuckyDay", luckyDay)
        })
    }
    
    func getAllLuckyDayHandler(_ req: Request) throws -> Future<View> {
        return LuckyDay.query(on: req).all().flatMap({ luckyDays in
            let context = LuckyDayContext(title: "LuckyDay", luckyDays: luckyDays.isEmpty ? nil : luckyDays)
            return try req.view().render("setLuckyDay", context)
        })
    }
    
    
    func createHandler(_ req: Request,newLuckyDay: LuckyDay) throws -> Future<LuckyDay> {
        return newLuckyDay.save(on: req)
    }
    
    func updateHandler(_ req: Request) throws -> Future<LuckyDay> {
        return try flatMap(to: LuckyDay.self,
                           req.parameters.next(LuckyDay.self),
                           req.content.decode(LuckyDay.self)) {
                            luckyDay, updatedLuckyDay in
                            luckyDay.monthTotal = updatedLuckyDay.monthTotal
                            luckyDay.percent = updatedLuckyDay.percent
                            luckyDay.points = updatedLuckyDay.points
                            return luckyDay.save(on: req)
        }
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(LuckyDay.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
}

struct LuckyDayContext: Encodable {
    let title: String
    let luckyDays: [LuckyDay]?
}
