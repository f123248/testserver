//
//  CourseSatisfactionAnsController.swift
//  App
//
//  Created by hsu on 2019/3/22.
//


import Vapor
import Fluent

struct CourseSatisfactionAnsController: RouteCollection {
    
    func boot(router: Router) throws {
        let courseSatisfactionAnsRoute = router.grouped("api", "courseSatisfactionAns")
        courseSatisfactionAnsRoute.post(Answer.self, use: createHandler)
        courseSatisfactionAnsRoute.get(UUID.parameter, use: getAnswerHandler)//Course.ID
        courseSatisfactionAnsRoute.delete(CourseSatisfactionAns.parameter, use: deleteHandler)
        
    }
    
    func createHandler(_ req: Request,answer: Answer)
        throws -> Future<CourseSatisfactionAns> {
            print("\(Date()) [POST]:api/courseSatisfactionAns [FUNC]=createHandler")
            let directory = DirectoryConfig.detect()
            let workPath = directory.workDir
            
            var file = "answer"+answer.courseID.uuidString+answer.studentID.uuidString+".json"
            let folder = "Public/documents/answers"
            let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
            
            let jsonData = try JSONEncoder().encode(answer)
            
            let test11 = CourseSatisfactionAns.query(on: req)
                .filter(\CourseSatisfactionAns.courseSatisfactionAnsUrl == file).first().flatMap(to: CourseSatisfactionAns.self) {
                answers in
                    if answers == nil {
                        if let jsonString = String(data: jsonData, encoding: .utf8){
                            
                            do {
                                try jsonString.write(to: saveURL, atomically: false, encoding: .utf8)
                            }
                            catch {
                                file = ""
                                print(error)
                                
                            }
                        }
                        else{
                            file = ""
                            print("Not write error")
                        }
                        let courseQuestion = CourseSatisfactionAns(courseSatisfactionAnsUrl: file,courseID: answer.courseID ,studentID: answer.studentID)
                        print("file=\(courseQuestion.courseSatisfactionAnsUrl) courseID=\(courseQuestion.courseID) studentID=\(courseQuestion.studentID)")
                        return courseQuestion.save(on: req)
                    }
                    else {
                        throw Abort(.custom(code: 601, reasonPhrase: "Cannot add request because this student already completed satisfaction of this course."))
                    }
                    
            }
            return test11
    }
    
    func getAnswerHandler(_ req: Request) throws -> Future<[Answer]> {
        let courseid = try req.parameters.next(UUID.self)
        print("\(Date()) [GET]:api/courseSatisfactionAns/\(courseid) [FUNC]=getAnswerHandler")
        var answerArray:[Answer] = []
        let answers = CourseSatisfactionAns.query(on: req)
            .filter(\CourseSatisfactionAns.courseID==courseid)
            .all()
            .map(to:[Answer].self){
                satisfactions in
                for satisfaction in satisfactions{
                    let file = satisfaction.courseSatisfactionAnsUrl
                    
                    let directory = DirectoryConfig.detect()
                    let workPath = directory.workDir
                    
                    let folder = "Public/documents/answers"
                    let saveURL = URL(fileURLWithPath: workPath+folder+"/"+file)
                    print(saveURL)
                    do{
                        let text2 = try String(contentsOf: saveURL, encoding: .utf8)
                        var jsonData = try JSONDecoder().decode(Answer.self, from: text2)
                        for (index,kind) in jsonData.kind.enumerated() {
                            if kind == 2 {
                                jsonData.answer[index] = jsonData.answer[index] + ":" + jsonData.scale[index][Int(jsonData.answer[index]) ?? 0]
                            }
                            else if kind == 3 {
                                let answers = jsonData.answer[index]
                                let answerArr = answers.split(separator: ",")
                                jsonData.answer[index] = ""
                                for ans in answerArr {
                                    jsonData.answer[index] += ans+":"+jsonData.scale[index][Int(ans) ?? 0]+","
                                }
                                jsonData.answer[index] = String(jsonData.answer[index].dropLast())
                            }
                        }
                        answerArray.append(jsonData)
                    }
                    catch{
                        print("Can't read answer")
                    }
                    
                }
                return answerArray
        }
        return answers
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(CourseSatisfactionAns.self)
            .map(to: CourseSatisfactionAns.self){
                answer in
                print("\(Date()) [DELETE]:api/courseSatisfactionAns/\(String(describing: answer.id)) [FUNC]=deleteHandler")
                print("courseID=\(answer.courseID) studentID=\(answer.studentID)")
                let file = answer.courseSatisfactionAnsUrl
                let directory = DirectoryConfig.detect()
                let workPath = directory.workDir
                
                let folder = "Public/documents/answers"
                let saveURL = URL(fileURLWithPath: workPath+folder+"/"+file)
                do{
                    try FileManager.default.removeItem(at: saveURL)
                }
                catch{
                    print("Can't remove")
                }
                return answer
            }
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
}

struct SatisfactionAnsContext: Encodable {
    let title: String
    let satisfactions: [CourseSatisfactionAns]?
}

struct Answer: Encodable,Content{
    var courseID:UUID
    var quest:[String] //題目
    var answer:[String] //答案
    var kind:[Int] //0:量表 1:問答
    var scale:[[String]] //量表的敘述 同意/不同意 一次兩組 若無量表題=>""
    var studentID:UUID
}

//struct AnswerBox: Encodable, Content {
//    var answerbox: [String]
//}
