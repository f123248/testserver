import Vapor
import Fluent

struct RCAccountController: RouteCollection {
    func boot(router: Router) throws {
        let rcRoute = router
        rcRoute.get("rcAccount", use: rcAccountViewHandler)
        rcRoute.get("rcaCoursePivot", RCAccount.parameter, use: getRCACoursePivotHandler)
        rcRoute.put("rcAccount",RCAccount.parameter, use: updateHandler)
        rcRoute.get("rcaStudentPivot", RCAccount.parameter, use: getRCAStudentPivotHandler)
        rcRoute.get("rcAccountCreate", use: rcaCreateViewHandler)
        rcRoute.get("rcAccountEdit", RCAccount.parameter ,use: rcaEditViewHandler)
        rcRoute.post(RCAccount.self, at: "rcAccount", use: createHandler)
        rcRoute.delete("deleteAccount", RCAccount.parameter, use: deleteHandler)
        rcRoute.delete("deleteRCACourse", RCAccount.parameter, Course.parameter,use: deleteRCACourseHandler)
        rcRoute.delete("deleteRCAStudent", RCAccount.parameter, Student.parameter,use: deleteRCAStudentHandler)
        
        rcRoute.post("rcAccount",RCAccount.parameter,"RCACourse",Course.parameter,use: addRCACourseHandler)
        rcRoute.post("rcAccount",RCAccount.parameter,"RCAStudent",String.parameter,use: addRCAStudentHandler)
        let rcAppRoute = router.grouped("api", "rcAccount")
        rcAppRoute.post(RCALogin.self, use: rcLoginHandler)
        rcAppRoute.post(RCALogin.self,at: Student.parameter, use: rcLoginWithStuHandler)
    }
    func updateHandler(_ req: Request) throws -> Future<RCAccount> {
        return try flatMap(to: RCAccount.self,
                           req.parameters.next(RCAccount.self),
                           req.content.decode(RCAccount.self)) {
                            rcaccount, updatedRCA in
                            rcaccount.account = updatedRCA.account
                            rcaccount.password = updatedRCA.password
                            return rcaccount.save(on: req)
        }
    }
    func rcLoginHandler(
        _ req: Request,
        rcAccount: RCALogin
        ) throws -> Future<RCALogin> {
        print("\(Date()) [POST]:api/rcAccount [FUNC]=rcLoginHandler")
        print("account=\(rcAccount.account) password=\(rcAccount.password)")
        var accountID:UUID?
        var acc2courseBool = -1
        var pswError = 0
        var accountError = 1
        _ = RCAccount.query(on: req).filter(\RCAccount.account==rcAccount.account).all().map(to: RCALogin.self){
            rcAccs in
            for rcAcc in rcAccs{
                accountError = 0
                if rcAcc.password != rcAccount.password{
                    pswError = 1
                }
                else{
                    pswError = 0
                    accountID = rcAcc.id
                }
            }
           return rcAccount
        }
        
        return RCACoursePivot.query(on: req).all().map(to: RCALogin.self){
            pivots in
            for pivot in pivots{
                if pivot.rcAccountID == accountID{
                    acc2courseBool = 1
                    if pivot.courseCode == rcAccount.courseCode{
                        return rcAccount
                    }
                }
            }
            if accountError == 1{
                throw Abort(.custom(code: 604, reasonPhrase: "This account not exist."))
            }
            else if pswError == 1{
                throw Abort(.custom(code: 603, reasonPhrase: "This password not correct."))
            }
            else if acc2courseBool == 1{
                throw Abort(.custom(code: 610, reasonPhrase: "CourseCode is incorrect."))
            }
            else{
                throw Abort(.custom(code: 609, reasonPhrase: "This account can't roll call this course."))
            }
        }
    }
    
    func rcLoginWithStuHandler(
        _ req: Request,
        rcAccount: RCALogin
        ) throws -> Future<RCALogin> {
        var denyBool = 1
        var stuID:UUID?
        var accountID:UUID?
        var acc2courseBool = -1
        var pswError = 0
        var accountError = 1
        var success = 0
        _ = try req.parameters.next(Student.self).map(to: Int.self){
            student in
            print("\(Date()) [POST]:api/rcAccount/\(String(describing: student.id)) [FUNC]=rcLoginWithStuHandler")
            stuID = student.id
            return 0
        }
        _ = RCAccount.query(on: req).filter(\RCAccount.account==rcAccount.account).all().map(to: RCALogin.self){
            rcAccs in
            for rcAcc in rcAccs{
                accountError = 0
                if rcAcc.password != rcAccount.password{
                    pswError = 1
                }
                else{
                    pswError = 0
                    accountID = rcAcc.id
                }
            }
            return rcAccount
        }
        
        _ = RCAStudentPivot.query(on: req).all().map(to: RCALogin.self){
            pivots in
            for pivot in pivots{
                if pivot.studentID == stuID && accountID==pivot.rcAccountID{
                    denyBool = 0
                }
            }
            return rcAccount
        }
        
        return RCACoursePivot.query(on: req).all().map(to: RCALogin.self){
            pivots in
            for pivot in pivots{
                if pivot.rcAccountID == accountID{
                    acc2courseBool = 1
                    if pivot.courseCode == rcAccount.courseCode{
                        success = 1
                        
                    }
                }
            }
            if accountError == 1{
                throw Abort(.custom(code: 604, reasonPhrase: "This account not exist."))
            }
            else if pswError == 1{
                throw Abort(.custom(code: 603, reasonPhrase: "This password not correct."))
            }
            else if acc2courseBool == 1 && success == 0{
                throw Abort(.custom(code: 610, reasonPhrase: "CourseCode is incorrect."))
            }
            else if denyBool==1{
                throw Abort(.custom(code: 609, reasonPhrase: "Permission deny."))
            }
            else if success == 1{
                return rcAccount
            }
            else {
                throw Abort(.custom(code: 609, reasonPhrase: "This account can't roll call this course."))
            }
        }
    }
    
    func createHandler(
        _ req: Request,
        rcAccount: RCAccount
        ) throws -> Future<RCAccount> {
        print("\(Date()) [POST]:rcAccount [FUNC]=createHandler")
        print("account=\(rcAccount.account) password=\(rcAccount.password)")
        return rcAccount.save(on: req)
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(RCAccount.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    func deleteRCACourseHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(RCAccount.self),
            req.parameters.next(Course.self)
        ) { account, course in
            return account.rcaCourse
                .detach(course, on: req)
                .transform(to: .noContent)
        }
    }
    
    func deleteRCAStudentHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try flatMap(
            to: HTTPStatus.self,
            req.parameters.next(RCAccount.self),
            req.parameters.next(Student.self)
        ) { account, student in
            return account.rcaStudent
                .detach(student, on: req)
                .transform(to: .noContent)
        }
    }
    
    func addRCACourseHandler(
        _ req: Request
        ) throws -> Future<HTTPStatus> {
        return try flatMap(
            to:HTTPStatus.self,
            req.parameters.next(RCAccount.self),
            req.parameters.next(Course.self)) { account, course in
                print("\(Date()) [POST]:rcAccount/\(String(describing: account.id))/RCACourse/\(String(describing: course.id)) [FUNC]=addRCACourseHandler")
                var courseCodes:[String] = []
                _ = RCACoursePivot.query(on: req).filter(\RCACoursePivot.rcAccountID==account.id!).all().map(to: Int.self){
                    pivots in
                    for pivot in pivots{
                        courseCodes.append(pivot.courseCode)
                    }
                    return 0
                }
                var number = Int.random(in: 1000..<10000)
                while(courseCodes.contains(String(number))){
                    number = Int.random(in: 1000..<10000)
                }
                return RCACoursePivot.query(on: req)
                    .filter(\RCACoursePivot.courseID==course.id!)
                    .filter(\RCACoursePivot.rcAccountID==account.id!)
                    .first()
                    .flatMap(to: HTTPStatus.self) {
                        checkacc in
                        if checkacc == nil {
                            return account.rcaCourse
                                .attach(course, on: req)
                                .map(to: RCACoursePivot.self){
                                    pivot in
                                    pivot.courseCode = String(number)
                                    let _ = pivot.save(on: req)
                                    return pivot
                                }
                                .transform(to: .created)
                            
                        }
                        else {
                            
                            throw Abort(.custom(code: 601, reasonPhrase: "Already add this course"))
                        }
                }
                
        }
    }
    
    func addRCAStudentHandler(
        _ req: Request
        ) throws -> Future<HTTPStatus> {
        let account = try req.parameters.next(RCAccount.self)
        let studentID = try req.parameters.next(String.self)
        
        
        
        var success = false
        return account.flatMap(to: HTTPStatus.self){
            acc in
            
           
            return Student.query(on: req)
                .filter(\Student.studentID==studentID)
                .all()
                .flatMap(to: HTTPStatus.self){
                    students in
                    for student in students{
                        if student.studentID == studentID{
                            return RCAStudentPivot.query(on: req)
                                .filter(\RCAStudentPivot.studentID==student.id!)
                                .filter(\RCAStudentPivot.rcAccountID==acc.id!)
                                .first()
                                .flatMap(to: HTTPStatus.self) {
                                    checkacc in
                                    if checkacc == nil {
                                        success = true
                                        return acc.rcaStudent
                                            .attach(student, on: req)
                                            .transform(to: .created)
                                    }
                                    else {
                                        
                                        throw Abort(.custom(code: 601, reasonPhrase: "Already add this student"))
                                    }
                            }
                        }
                    }
                    if success{
                        throw Abort(.custom(code: 200, reasonPhrase: "Success"))
                    }
                    else{
                        throw Abort(.custom(code: 604, reasonPhrase: "Not found StudentID"))
                    }
            }
        }
    }
    func getRCACoursePivotHandler(_ req: Request) throws -> Future<CourseStudentPivot> {
        return try req.parameters.next(RCAccount.self).flatMap(to: CourseStudentPivot.self){
            account in
            var courseStudentPivot = CourseStudentPivot(courseid:[],courseCode: [],courseName: [],stuid:[] ,studentName: [], studentID: [])
            _ = try account.rcaCourse.pivots(on: req).join(\Course.id, to: \RCACoursePivot.courseID).alsoDecode(Course.self).all().map(to: CourseStudentPivot.self){
                pivots in
                for pivot in pivots{
                    courseStudentPivot.courseid.append(pivot.0.courseID)
                    courseStudentPivot.courseCode.append(pivot.0.courseCode)
                    courseStudentPivot.courseName.append(pivot.1.courseName)
                }
                return courseStudentPivot
            }
            return try account.rcaStudent.query(on: req).all().map(to: CourseStudentPivot.self){
                students in
                for student in students{
                    courseStudentPivot.stuid.append(student.id)
                    courseStudentPivot.studentName.append(student.studentName)
                    courseStudentPivot.studentID.append(student.studentID)
                }
                return courseStudentPivot
            }
        }
    }
    func getRCAStudentPivotHandler(_ req: Request) throws -> Future<[Student]> {
        return try req.parameters.next(RCAccount.self).flatMap(to: [Student].self){
            account in
            return try account.rcaStudent.query(on: req).all()
        }
    }
    func rcaEditViewHandler(_ req: Request) throws -> Future<View> {
        return try req.parameters.next(RCAccount.self).flatMap(to: View.self){
            account in
            var showAccount = ShowAccount(account: account.account,password: account.password,courseCode: [],courseName: [], studentName: [], studentID: [])
            _ = try account.rcaCourse.pivots(on: req).join(\Course.id, to: \RCACoursePivot.courseID).alsoDecode(Course.self).all().map(to: ShowAccount.self){
                pivots in
                for pivot in pivots{
                    showAccount.courseCode.append(pivot.0.courseCode)
                    showAccount.courseName.append(pivot.1.courseName)
                }
                return showAccount
            }
            _ = try account.rcaStudent.query(on: req).all().map(to: ShowAccount.self){
                students in
                for student in students{
                    showAccount.studentName.append(student.studentName)
                    showAccount.studentID.append(student.studentID)
                }
                return showAccount
            }
            let context = showAccount
            return try req.view().render("rcAccountEdit", context)
        }
    }

    func rcAccountViewHandler(_ req: Request) throws -> Future<View> {
        return RCAccount.query(on: req).all().flatMap({ accounts in
            let context = RCAccountsContext(title: "RCAccounts", rcAccounts: accounts.isEmpty ? nil : accounts)
            return try req.view().render("rcAccount", context)
        })
    }
    func rcaCreateViewHandler(_ req: Request) throws -> Future<View> {
        return Course.query(on: req).all().flatMap({ courses in
            let context = IndexContext(title: "RCAccount", courses: courses.isEmpty ? nil : courses)
            return try req.view().render("rcAccountCreate", context)
        })
    }
}
struct RCALogin: Content {
    let account: String
    let password: String
    let courseCode: String
}
struct RCAccountsContext: Encodable {
    let title: String
    let rcAccounts: [RCAccount]?
}
struct RCAccountsEditContext: Encodable {
    let title: String
    let rcAccounts: Account?
    let courses: [Course]?
}
struct Account: Encodable {
    let account: String
    let password: String
    let verifyString: String
    let courseName: String
    let studentName: String
    let studentID: String
}
struct ShowAccount: Encodable {
    var account: String
    var password: String
    var courseCode: [String]
    var courseName: [String]
    var studentName: [String]
    var studentID: [String]
}

struct CourseStudentPivot: Encodable, Content {
    var courseid: [UUID?]
    var courseCode: [String]
    var courseName: [String]
    var stuid: [UUID?]
    var studentName: [String]
    var studentID: [String]
}
