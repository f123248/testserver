//
//  CourseSatisfactionController.swift
//  App
//
//  Created by 徐兆陽 on 2019/3/5.
//

import Vapor
import Fluent

struct CourseSatisfactionController: RouteCollection {
    
    func boot(router: Router) throws {
        let webRoute = router
        webRoute.get("viewSatisfaction", use:viewSatisfactionHandler)
        webRoute.get("createSatisfaction", use:createSatisfactionHandler)
        
        let courseSatisfactionRoute = router.grouped("api", "courseSatisfaction")
     
        courseSatisfactionRoute.post(Question.self, use: createHandler)
        courseSatisfactionRoute.get(UUID.parameter, use: getQuestionHandler)//CourseID
        courseSatisfactionRoute.delete(CourseSatisfaction.parameter, use: deleteHandler)
        
    }

    func createSatisfactionHandler(_ req: Request) throws -> Future<View> {
        return Course.query(on: req).all().flatMap({ courses in
            let context = IndexContext(title: "Courses", courses: courses.isEmpty ? nil : courses)
            return try req.view().render("createSatisfaction", context)
        })
        //return handler(req: req, source: "createSatisfaction")
    }
    
    func viewSatisfactionHandler(_ req: Request) throws -> Future<View> {
        var courseArr:[Course]=[]
        return Course.query(on: req).join(\CourseSatisfactionAns.courseID, to: \Course.id).all().flatMap({
            courses in
            for course in courses{
                let containBool = courseArr.contains(where: { (value) -> Bool in
                    value.id == course.id
                })
                if containBool == false{
                    courseArr.append(course)
                }
            }
            
            let context = IndexContext(title: "Courses", courses: courses.isEmpty ? nil : courseArr)
            return try req.view().render("viewSatisfaction", context)
        })
    }
    
    func createHandler(_ req: Request,question: Question)
        throws -> Future<CourseSatisfaction> {
            print("\(Date()) [POST]:api/courseSatisfaction [FUNC]=createHandler")
            let directory = DirectoryConfig.detect()
            let workPath = directory.workDir
            
            var file = "question"+question.courseID.uuidString+".json"
            let folder = "Public/documents/questions"
            let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)

            let jsonData = try JSONEncoder().encode(question)
            if let jsonString = String(data: jsonData, encoding: .utf8){
                do {
                    try jsonString.write(to: saveURL, atomically: false, encoding: .utf8)
                }
                catch {
                    file=""
                }
            }
            else{
                file = ""
            }
            
            let courseQuestion = CourseSatisfaction(courseSatisfactionEmptyUrl: file, courseID: question.courseID)
            print("file=\(courseQuestion.courseSatisfactionEmptyUrl) courseID=\(courseQuestion.courseID)")
            return courseQuestion.save(on: req)
    }
    
    func getQuestionHandler(_ req: Request) throws -> Future<Question> {
        let courseid = try req.parameters.next(UUID.self)
        let question = CourseSatisfaction.query(on: req)
            .filter(\CourseSatisfaction.courseID==courseid)
            .all()
            .map(to:Question.self)
            {
                satisfactions in
                for satisfaction in satisfactions{
                    let file = satisfaction.courseSatisfactionEmptyUrl
                    
                    let directory = DirectoryConfig.detect()
                    let workPath = directory.workDir
                    
                    let folder = "Public/documents/questions"
                    let saveURL = URL(fileURLWithPath: workPath+folder+"/"+file)
                    print(saveURL)
                    do{
                        let text2 = try String(contentsOf: saveURL, encoding: .utf8)
                        let jsonData = try JSONDecoder().decode(Question.self, from: text2)
                        print(jsonData)
                        return jsonData
                    }
                    catch{
                        print("Can't read question")
                    }
                    
                }
                return Question.init(courseID: courseid, title:[], quest: [], kind: [], scale: [])
        }
        print(question)
        return question
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(CourseSatisfaction.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
}

struct SatisfactionContext: Encodable {
    let title: String
    let satisfactions: [CourseSatisfaction]?
}

struct Question: Encodable,Content{
    var courseID:UUID
    var title:[String] //問題1,問題2
    var quest:[String] //題目
    var kind:[Int] //0:量表 1:問答
//    var scale:[QuestionBox] //量表的敘述 同意/不同意 一次兩組 若無量表題=>""
    var scale:[[String]] //量表的敘述 同意/不同意 一次兩組 若無量表題=>""

}

//struct QuestionBox: Encodable, Content {
//    var questionbox: [String]
//}

