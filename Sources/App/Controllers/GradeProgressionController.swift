import Vapor
import Fluent

struct GradeProgressionController: RouteCollection {
    
    func boot(router: Router) throws {
        
        let gradeRoute = router.grouped("api", "grade")
        gradeRoute.post(GradeWithImageStruct.self, use: createHandler)
        gradeRoute.get(use: getAllHandler)
        gradeRoute.get(GradeProgression.parameter, use: getHandler)
        gradeRoute.get(GradeProgression.parameter, "student", use: getStudentHandler)
        
        let webRoute = router
        webRoute.get("pushGrade", use: pushGradeViewHandler)
        webRoute.get("passGrade", use: passGradeViewHandler)
        webRoute.get("gradeDecline", use: gradeDeclineViewHandler)
        webRoute.get("gradeDecline",UUID.parameter, use: gradeCheckViewHandler)
        webRoute.get("gradeCheck",UUID.parameter, use: gradeCheckViewHandler)        
        webRoute.get("gradePassCheck",UUID.parameter, use: gradePassCheckViewHandler)
        webRoute.get("gradeDeclineCheck",UUID.parameter, use: gradeDeclineCheckViewHandler)
        webRoute.put("gradeCheckEdit",GradeProgression.parameter, use: gradeStatePassHandler)
        webRoute.put("gradeCheckEditDecline",GradeProgression.parameter, use: gradeStateDeclineHandler)
    }
    
    func createHandler(
        _ req: Request,
        gradeProgression: GradeWithImageStruct
        ) throws -> Future<GradeProgression> {
        let timeInterval:TimeInterval = 0
        let date1 = Date(timeIntervalSince1970: timeInterval)
        
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let gradeImagesPath: String = "http://163.21.117.108/documents/images/gradeImages/"
        let folder = "Public/documents/images/gradeImages"
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
        let file = "gradeImages"+gradeProgression.studentID.uuidString+dateFormatString+".jpg"

        let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
        
        
        //studentIdCard
//        let directoryStudentIdCard = DirectoryConfig.detect()
//        let workPathStudentIdCard = directoryStudentIdCard.workDir
        let studentIdCardPath: String = "http://163.21.117.108/documents/images/studentIdCard/"
        let studentIdCardfolder = "Public/documents/images/studentIdCard"
        let studentIdCardfile = "studentIdCard"+gradeProgression.studentID.uuidString+".jpg"
        let studentIdCardsaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(studentIdCardfolder, isDirectory: true).appendingPathComponent(studentIdCardfile, isDirectory: false)

        //idCard
        let idCardPath: String = "http://163.21.117.108/documents/images/idCard/"
        let idfolder = "Public/documents/images/idCard"
        let idfile = "idCard"+gradeProgression.studentID.uuidString+".jpg"
        let idsaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(idfolder, isDirectory: true).appendingPathComponent(idfile, isDirectory: false)
        
        //idbackCard
        let sidebackIdCardPath: String = "http://163.21.117.108/documents/images/idCardBackSide/"
        let sidebackIdfolder = "Public/documents/images/idCardBackSide"
        let sidebackIdfile = "idCardBackSide"+gradeProgression.studentID.uuidString+".jpg"
        let sidebackIdsaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(sidebackIdfolder, isDirectory: true).appendingPathComponent(sidebackIdfile, isDirectory: false)
        
        //bankbook
        let bankbookPath: String = "http://163.21.117.108/documents/images/bankbook/"
        let bankbookfolder = "Public/documents/images/bankbook"
        let bankbookfile = "bankbook"+gradeProgression.studentID.uuidString+".jpg"
        let bankbooksaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(bankbookfolder, isDirectory: true).appendingPathComponent(bankbookfile, isDirectory: false)
        
        do {
            try gradeProgression.image.write(to: saveURL)

        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post grade progression image."))
        }
        
        do {
            try gradeProgression.studentIdCard.write(to: studentIdCardsaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post studentIdCard."))
        }
        
        do {
            try gradeProgression.idCard.write(to: idsaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post idcard."))
        }
        
        do {
            try gradeProgression.idCardBackSide.write(to: sidebackIdsaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post idCardBackSide."))
        }


        do {
            try gradeProgression.bankbook.write(to: bankbooksaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post bankbook."))
        }
        
        let newGrade = GradeProgression(applyYear: gradeProgression.applyYear,
                                        applySemester: gradeProgression.applySemester,
                                        gradePicture: gradeImagesPath+file,
                                        gradeDescription: gradeProgression.gradeDescription,
                                        studentID: gradeProgression.studentID,
                                        gradeState: 0,
                                        verificationRejectedReason: "",
                                        applyDate: Date(),
                                        verifiedDate: date1)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy.MM"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateChange = dateFormatterGet.date(from: gradeProgression.studentRegisterDate)
        
        
        _ = Student.query(on: req).filter(\Student.id==gradeProgression.studentID)
            .first().flatMap(to: Student.self) {
            student in
                student?.studentDepartment=gradeProgression.studentDepartment
                if let dateTrue = dateChange{
                   student?.studentRegisterDate=dateTrue
                }
                
                student?.studentAddress=gradeProgression.studentAddress
                student?.studentPhoneNumber=gradeProgression.studentPhoneNumber
                
                student?.studentIdCard=studentIdCardPath+studentIdCardfile
                student?.idCard=idCardPath+idfile
                student?.idCardBackSide=sidebackIdCardPath+sidebackIdfile
                student?.bankbook=bankbookPath+bankbookfile
                
                return student!.save(on: req)
        }
        return newGrade.save(on: req)
    
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[GradeProgression]> {
        return GradeProgression.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<GradeProgression> {
        return try req.parameters.next(GradeProgression.self)
    }
    
    func getStudentHandler(_ req: Request) throws -> Future<Student> {
        return try req
            .parameters.next(GradeProgression.self)
            .flatMap(to: Student.self) { grade in
                grade.student.get(on: req)
        }
    }
    
    // MARK: GradeProgression for back-end
    // handler for grade progression
    func handler(req: Request, source: String) -> Future<View> {
        var gradeAll:[GradeOneStruct] = []
        return GradeProgression.query(on: req)
            
            .join(\Student.id, to: \GradeProgression.studentID)
            .alsoDecode(Student.self)
            .all()
            .flatMap(to: View.self){ grades in
                var gradeOne: GradeOneStruct?
                for grade in grades {
                    if grade.0.id != nil {
                        gradeOne = GradeOneStruct(studentID: grade.1.studentID,
                                                  studentName: grade.1.studentName,
                                                  gradeDescription: grade.0.gradeDescription,
                                                  gradeState: grade.0.gradeState,
                                                  gradeID: grade.0.id!,
                                                  studentDepartment: grade.1.studentDepartment,
                                                  studentRegisterDate: grade.1.studentRegisterDate,
                                                  studentAddress: grade.1.studentAddress,
                                                  studentPhoneNumber: grade.1.studentPhoneNumber,
                                                  studentIdCard: grade.1.studentIdCard,
                                                  idCard: grade.1.idCard,
                                                  idCardBackSide: grade.1.idCardBackSide,
                                                  bankbook: grade.1.bankbook,
                                                  gradePicture: grade.0.gradePicture,
                                                  verificationRejectedReason: grade.0.verificationRejectedReason,
                                                  applyYearSemester: grade.0.applyYear+grade.0.applySemester,
                                                  applyDate: grade.0.applyDate,
                                                  verifiedDate: grade.0.verifiedDate)
                    }
                    if gradeOne != nil {
                        gradeAll.append(gradeOne!)
                    }
                }
                let context = GradeAllStruct(title: "title", oneAll: gradeAll)
                return try req.view().render(source, context)
        }
    }
    
    // pushGrade
    func pushGradeViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "pushGrade")
    }
    
    // passGrade
    func passGradeViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "passGrade")
    }
    
    // gradeDecline
    func gradeDeclineViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "gradeDecline")
    }
    
    func handlerCheckViewHandler(req: Request, source: String) throws -> Future<View> {
        let gradeID = try req.parameters.next(UUID.self)
        var gradeStruct: GradeOneStruct?
        return GradeProgression.query(on: req)
            .filter(\GradeProgression.id==gradeID)
            .join(\Student.id, to: \GradeProgression.studentID)
            .alsoDecode(Student.self)
            .first()
            .flatMap(to: View.self){ grade in
                
                //                if license?.0.id != nil {
                if grade != nil {
                    gradeStruct = GradeOneStruct(studentID: grade!.1.studentID,
                                                 studentName: grade!.1.studentName,
                                                 gradeDescription: grade!.0.gradeDescription,
                                                 gradeState: grade!.0.gradeState,
                                                 gradeID: grade!.0.id!,
                                                 studentDepartment: grade!.1.studentDepartment,
                                                 studentRegisterDate: grade!.1.studentRegisterDate,
                                                 studentAddress: grade!.1.studentAddress,
                                                 studentPhoneNumber: grade!.1.studentPhoneNumber,
                                                 studentIdCard: grade!.1.studentIdCard,
                                                 idCard: grade!.1.idCard,
                                                 idCardBackSide: grade!.1.idCardBackSide,
                                                 bankbook: grade!.1.bankbook,
                                                 gradePicture: grade!.0.gradePicture,
                                                 verificationRejectedReason: grade!.0.verificationRejectedReason,
                                                 applyYearSemester: grade!.0.applyYear+grade!.0.applySemester,
                                                 applyDate: grade!.0.applyDate,
                                                 verifiedDate: grade!.0.verifiedDate)
                }
                
                let context = GradeAllStruct2(title: "title", oneAll: gradeStruct!)
                
                return try req.view().render(source, context)
                
                
        }
    }
    
    // GradeCheck
    func gradeCheckViewHandler(_ req: Request) throws -> Future<View> {
        return try handlerCheckViewHandler(req: req, source: "gradeCheck")
    }
    
    // GradePassCheck
    func gradePassCheckViewHandler(_ req: Request) throws -> Future<View> {
        return try handlerCheckViewHandler(req: req, source: "gradePassCheck")
    }
    
    // GradeDeclineCheck
    func gradeDeclineCheckViewHandler(_ req: Request) throws -> Future<View> {
        return try handlerCheckViewHandler(req: req, source: "gradeDeclineCheck")
    }
    
    // Put api for gradePass button
    func gradeStatePassHandler(_ req: Request) throws -> Future<GradeProgression> {
        return try flatMap(to: GradeProgression.self,
                           req.parameters.next(GradeProgression.self),
                           req.content.decode(GradeOneStruct.self)) {
                            grade, updatedGrade in
                            grade.gradeState = updatedGrade.gradeState
                            grade.verifiedDate = Date()
                            return grade.save(on: req)
        }
    }
    
    // Put api for gradeDecline button
    func gradeStateDeclineHandler(_ req: Request) throws -> Future<GradeProgression> {
        return try flatMap(to: GradeProgression.self,
                           req.parameters.next(GradeProgression.self),
                           req.content.decode(GradeOneStruct.self)) {
                            grade, updatedGrade in
                            grade.gradeState = updatedGrade.gradeState
                            grade.verificationRejectedReason = updatedGrade.verificationRejectedReason
                            grade.verifiedDate = Date()
                            
                            return grade.save(on: req)
        }
    }
    
}

struct GradeOneStruct: Content {
    let studentID: String
    let studentName: String
    let gradeDescription: String
    let gradeState: Int
    let gradeID: UUID
    let studentDepartment: String
    let studentRegisterDate: Date
    let studentAddress: String
    let studentPhoneNumber: String
    let studentIdCard: String
    let idCard: String
    let idCardBackSide: String
    let bankbook: String
    let gradePicture: String
    let verificationRejectedReason: String
    let applyYearSemester: String
    let applyDate: Date
    let verifiedDate: Date
}

struct GradeAllStruct: Content {
    let title: String
    let oneAll: [GradeOneStruct]
}

struct GradeAllStruct2: Content,Encodable {
    let title: String
    let oneAll: GradeOneStruct
}

struct GradeWithImageStruct: Content,Encodable {
    var applyYear: String
    var applySemester: String
    var gradeDescription: String
    var studentID: Student.ID
    var image: Data

    var studentDepartment: String
        var studentRegisterDate: String
    var studentAddress: String
    var studentPhoneNumber: String
    
    var studentIdCard: Data
    var idCard: Data
    var idCardBackSide: Data
    var bankbook: Data
}
