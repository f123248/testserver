import Vapor
import Fluent
import FluentPostgreSQL
import CryptoSwift

struct StudentController: RouteCollection {
    
    func boot(router: Router) throws {
        let studentRoute = router.grouped("api", "student")
        studentRoute.post(Student.self, use: createHandler)
        studentRoute.put("update",Student.parameter, use: updateStudentData)
        studentRoute.put("updatestudent",Student.parameter, use: updateStudent)
        studentRoute.get(use: getAllHandler)
        studentRoute.get(Student.parameter, use: getHandler)
        studentRoute.delete("deletestudent",Student.parameter, use: deleteHandler)
        studentRoute.get("search", use: search)
        studentRoute.get(Student.parameter, "evaluation", use: getCourseEvaluationHandler)
        studentRoute.get(Student.parameter, "leave", use: getLeaveHandler)
        studentRoute.get(Student.parameter, "rollcall", use: getRollCallHandler)
//        studentRoute.get(Student.parameter, "satisfaction", use: getCourseSatisfactionHandler)
        studentRoute.get("test",Student.parameter, use: getJoinCourseHandler)
        studentRoute.put("updatePushToken", Student.parameter,"token", String.parameter, use: upDateTokenHandler)
        
        let checkRoute = router.grouped("api", "loginCheck")
//        checkRoute.post(Login.self, use: loginHandler)
        checkRoute.post(Login.self, use: loginXMLHandler)
        
        // post student images
        let imagesRoute = router.grouped("api", "studentImages")
        imagesRoute.post(StudentImageStruct.self, at: "studentIdCard", use: studentIdCardHandler)
        imagesRoute.post(StudentImageStruct.self, at: "idCard", use: idCardHandler)
        imagesRoute.post(StudentImageStruct.self, at: "idCardBackSide", use: idCardBackSideHandler)
        imagesRoute.post(StudentImageStruct.self, at: "bankbook", use: bankbookHandler)
        imagesRoute.post(StudentImageStruct.self, at: "portrait", use: portraitHandler)

        
        let imageRoute = router.grouped("api", "getImages")
        imageRoute.get(String.parameter, Int.parameter, use: getStudentImagesHandler)
//        imageRoute.get(String.parameter, use:getImagesNameHandler)
        
        
        let webRoute = router
        webRoute.get("studentOverview", use:getStudentOverviewViewHandler)
        webRoute.put("studentOverview", Student.parameter, use:transformToSunSeedHandler)
        webRoute.get("sunSeedStudent", use:getSunSeedViewHandler)
        webRoute.put("sunSeedStudent", Student.parameter, use:transformToNormalHandler)
    }
    
    func upDateTokenHandler(_ req:Request)throws -> Future<Student>{
        return try req.parameters.next(Student.self).flatMap(to: Student.self){
            student in
            let newToken = try req.parameters.next(String.self)
            student.pushToken = newToken
            return student.save(on: req)
        }
       
    }
    
    func updateStudentData(_ req:Request)throws -> Future<Student>{
        
        return try flatMap(to: Student.self,req.parameters.next(Student.self),req.content.decode(Student.self)) {
            
            student, updatedStudent in
            student.password = updatedStudent.password
            student.studentName = updatedStudent.studentName
            student.studentType = updatedStudent.studentType
            student.studentDepartment = updatedStudent.studentDepartment
            student.studentAddress = updatedStudent.studentAddress
            student.studentPhoneNumber = updatedStudent.studentPhoneNumber
            student.studentQRcode = updatedStudent.studentQRcode
            student.studentIdCard = updatedStudent.studentIdCard
            student.idCard = updatedStudent.idCard
            student.idCardBackSide = updatedStudent.idCardBackSide
            student.bankbook = updatedStudent.bankbook
            student.portrait = updatedStudent.portrait
            student.studentBlackList = updatedStudent.studentBlackList
            student.blackLastDate = updatedStudent.blackLastDate
            student.token = updatedStudent.token
            return student.save(on: req)
        }
    }
    
    func updateStudent(_ req:Request)throws -> Future<Student>{
        return try flatMap(to: Student.self,req.parameters.next(Student.self),req.content.decode(Student.self)) {
            student, updatedStudent in
            student.studentRegisterDate = updatedStudent.studentRegisterDate
            student.studentDepartment = updatedStudent.studentDepartment
            student.studentAddress = updatedStudent.studentAddress
            student.studentPhoneNumber = updatedStudent.studentPhoneNumber
            return student.save(on: req)
        }
    }
    
    func createHandler(
        _ req: Request,
        student: Student
        ) throws -> Future<Student> {
        return Student.query(on: req).all().flatMap(to: Student.self) { students in
            for stu in students {
                if student.studentID == stu.studentID {
                    throw Abort(.custom(code: 602, reasonPhrase: "a user with this account already exists"))
                }
            }
            
            let url = "http://210.71.24.143/WebService/WebService.asmx"
            
            let xmlPWDstr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><PwdDes xmlns=\"http://tempuri.org/\"><pToEncrypt>"+student.password+"</pToEncrypt></PwdDes></soap:Body></soap:Envelope>"
        
            var headers: HTTPHeaders = .init()
            headers.add(name: .host, value: "210.71.24.143")
            headers.add(name: .contentType, value: "text/xml; charset=utf-8")
            headers.add(name: .contentLength, value: String(xmlPWDstr.count))
            headers.add(name: .soapAction, value: "http://tempuri.org/PwdDes")
            
            let body = HTTPBody(string: xmlPWDstr)
            let httpPWDReq = HTTPRequest(
                method: .POST,
                url: URL(string: url)!,
                headers: headers,
                body: body)
            
            var encrypwd = ""
            return HTTPClient.connect(hostname: "210.71.24.143", on: req).flatMap(to: Student.self) { client in
                return client.send(httpPWDReq).map(to: Student.self) { httpResponse in
                    let response = Response(http: httpResponse, using: req)
                    let pwdDesrtn = response.description
                    
                    var encrypted = pwdDesrtn.components(separatedBy: "<PwdDesResult>")
                    encrypted = encrypted[1].components(separatedBy: "</PwdDesResult>")
                    encrypwd = encrypted[0]
                    let date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let dateString = dateFormatter.string(from: date)
                    
                    let timeInterval:TimeInterval = 0
                    let date1 = Date(timeIntervalSince1970: timeInterval)
                    
                    student.password = encrypwd
                    student.token = dateString.md5()
                    student.blackLastDate = date1
                    return student
                }
            }
        }.save(on: req)
    }
    // MARK: Student login
    func loginHandler(_ req: Request, userInput: Login) throws -> Future<LoginRtn> {
        let account = userInput.studentID
        var successInt: Int = -1
        let password = userInput.password
        var studentUUID = ""
        var studentName = ""
        return Student.query(on : req)
            .filter(\Student.studentID==account)
            .all().map(to: LoginRtn.self) { allAdmins in
            for admin in allAdmins {
//                if account == admin.studentID{
                if password.md5() == admin.password {
                    studentUUID = (admin.id?.uuidString)!
                    studentName = admin.studentName
                    successInt = 0
                }
                else {
                    successInt = 1
                }
//                }
            }

            if successInt == 0 {
                return LoginRtn(studentUUID: studentUUID, studentName: studentName)
            }
            else if successInt == 1 {
                throw Abort(.custom(code: 603, reasonPhrase: "This password not correct."))
            }
            else {
                throw Abort(.custom(code: 604, reasonPhrase: "This account not exist."))
            }
        }
    }
    
    func loginXMLHandler(_ req: Request, userInput: Login) throws -> Future<LoginRtn> {
        let account = userInput.studentID
        let password = userInput.password
        var studentUUID = ""
        var studentName = ""
        let url = "http://210.71.24.143/WebService/WebService.asmx"
        
        let xmlPWDstr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><PwdDes xmlns=\"http://tempuri.org/\"><pToEncrypt>"+password+"</pToEncrypt></PwdDes></soap:Body></soap:Envelope>"
        
        var headers: HTTPHeaders = .init()
        headers.add(name: .host, value: "210.71.24.143")
        headers.add(name: .contentType, value: "text/xml; charset=utf-8")
        headers.add(name: .contentLength, value: String(xmlPWDstr.count))
        headers.add(name: .soapAction, value: "http://tempuri.org/PwdDes")

        let body = HTTPBody(string: xmlPWDstr)
        let httpPWDReq = HTTPRequest(
            method: .POST,
            url: URL(string: url)!,
            headers: headers,
            body: body)
        
        var encrypwd = ""
        return HTTPClient.connect(hostname: "210.71.24.143", on: req).flatMap(to: LoginRtn.self) { client in
            return client.send(httpPWDReq).flatMap(to: LoginRtn.self) { httpResponse in
                let response = Response(http: httpResponse, using: req)
                let pwdDesrtn = response.description
                
                var encrypted = pwdDesrtn.components(separatedBy: "<PwdDesResult>")
                encrypted = encrypted[1].components(separatedBy: "</PwdDesResult>")
                encrypwd = encrypted[0]
                
                let xmlLoginstr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><CheckUsr xmlns=\"http://tempuri.org/\"><uid>"+account+"</uid><pwd>"+encrypwd+"</pwd></CheckUsr></soap:Body></soap:Envelope>"
                var headers: HTTPHeaders = .init()
                headers.add(name: .host, value: "210.71.24.143")
                headers.add(name: .contentType, value: "text/xml; charset=utf-8")
                headers.add(name: .contentLength, value: String(xmlLoginstr.count))
                headers.add(name: .soapAction, value: "http://tempuri.org/CheckUsr")
                
                let bodyLogin = HTTPBody(string: xmlLoginstr)
                let httpLoginReq = HTTPRequest(
                    method: .POST,
                    url: URL(string: url)!,
                    headers: headers,
                    body: bodyLogin)
                return client.send(httpLoginReq).flatMap(to: LoginRtn.self) { httpResponse in
                    let response = Response(http: httpResponse, using: req)
                    var loginrtn = response.description
                    let data = loginrtn.data(using: .isoLatin1)
                    loginrtn = String(data: data!, encoding: .utf8) ?? ""
                    print(loginrtn)
                    if loginrtn.contains("status&gt;Y&lt;/status&gt;"){
                        var namerrtn = loginrtn.components(separatedBy: "&lt;name&gt;")
                        namerrtn = namerrtn[1].components(separatedBy: "&lt;/name&gt;")
                        studentName = namerrtn[0]
                        print(studentName)
                        return Student.query(on: req).filter(\Student.studentID==account).first().map(to: LoginRtn.self){
                            stu in
                            if stu == nil {
                                let date = Date()
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyyMMddHHmmssSSS"
                                let dateString = dateFormatter.string(from: date)
                                let token = dateString.md5()
                                dateFormatter.dateFormat = "yyyy-MM-dd"
                                let date1970 = dateFormatter.date(from: "1970-01-01")
                                let newStu = Student(studentID: account, password: encrypwd, studentName: studentName, studentType: 0, studentDepartment: "", studentRegisterDate: Date(), studentAddress: "", studentPhoneNumber: "", studentQRcode: "", studentIdCard: "", idCard: "", idCardBackSide: "", bankbook: "", portrait: "", studentBlackList: 0, blackLastDate: date1970 ?? Date(), token: token, pushToken: "")
                                _ = newStu.save(on: req)
                                throw Abort(.custom(code: 616, reasonPhrase: "Create student success please login again."))
                            }
                            else {
                                studentUUID = stu?.id?.uuidString ?? ""
                                return LoginRtn(studentUUID: studentUUID, studentName: studentName)
                            }
                        }
                    }
                    else {
                        return Student.query(on: req).filter(\Student.studentID==account).first().map(to: LoginRtn.self){
                            stu in
                            if stu == nil {
                                throw Abort(.custom(code: 604, reasonPhrase: "This account not exist."))
                            }
                            else {
                                if stu?.password == encrypwd{
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyyMMddHHmmssSSS"
                                    let dateString = dateFormatter.string(from: Date())
                                    let token = dateString.md5()
                                    stu?.token = token
                                    _ = stu?.save(on: req)
                                    studentName = stu?.studentName ?? ""
                                    return LoginRtn(studentUUID: stu?.id?.uuidString ?? "", studentName: studentName)
                                }
                                else {
                                    throw Abort(.custom(code: 603, reasonPhrase: "This password not correct."))
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[Student]> {
        return Student.query(on: req).all()
    }
    
    
    func getHandler(_ req: Request) throws -> Future<Student> {
        return try req.parameters.next(Student.self)
    }
    
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Student.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    
    func search(_ req: Request) throws -> Future<[Student]> {

        guard let searchTerm = req.query[String.self, at: "term"] else {
            throw Abort(.badRequest)
        }
        return Student.query(on: req).filter(\.studentID == searchTerm).all()
    }
    
    func getCourseEvaluationHandler(_ req: Request)
        throws -> Future<[CourseEvaluation]> {
            return try req
                .parameters.next(Student.self)
                .flatMap(to: [CourseEvaluation].self) { student in
                    try student.courseEvaluation.query(on: req).all()
            }
    }
    
    func getLeaveHandler(_ req: Request)
        throws -> Future<[Leave]> {
            return try req
                .parameters.next(Student.self)
                .flatMap(to: [Leave].self) { student in
                    try student.leave.query(on: req).all()
            }
    }
    
    func getRollCallHandler(_ req: Request)
        throws -> Future<[RollCall]> {
            return try req
                .parameters.next(Student.self)
                .flatMap(to: [RollCall].self) { student in
                    try student.rollCall.query(on: req).all()
            }
    }
    
    func getJoinCourseHandler(_ req: Request) throws -> Int {
        let student = try req.query.decode(Student.self)

        return student.studentType
    }
    
    // MARK: Create student images
    // studentIdCard
    func studentIdCardHandler(_ req: Request,studentImageStruct: StudentImageStruct) throws -> Future<HTTPStatus> {
            let directory = DirectoryConfig.detect()
            let workPath = directory.workDir
            let studentIdCardPath: String = "http://163.21.117.108/documents/images/studentIdCard/"
//        let studentIdCardPath: String = "file:///Users/betty/Documents/PushPull/pushpull-server/Public/documents/images/studentIdCard/"
            let folder = "Public/documents/images/studentIdCard"
            let file = "studentIdCard"+studentImageStruct.studentID+".jpg"
            let returnAll = Student.query(on: req)
                .filter(\Student.studentID == studentImageStruct.studentID)
                .first()
                .map(to: HTTPStatus.self){ student in
                    student?.studentIdCard = studentIdCardPath+file
                    if student != nil {
                        _ = student!.save(on: req)
                        let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
                        do {
                            print(saveURL)
                            try studentImageStruct.image.write(to: saveURL)
                        }
                        catch {
                            print("Cannot save Image")
                            throw Abort(.custom(code: 608, reasonPhrase: "Can't post image."))
                        }
                        return HTTPStatus.ok
                    }
                    else {
                        throw Abort(.custom(code: 608, reasonPhrase: "Can't post image. Because this studentID is incorrect."))
                    }
            }
            return returnAll
    }
    
    // idCard
    func idCardHandler(_ req: Request,studentImageStruct: StudentImageStruct) throws -> Future<HTTPStatus> {
            let directory = DirectoryConfig.detect()
            let workPath = directory.workDir
            let idCardPath: String = "http://163.21.117.108/documents/images/idCard/"
            let folder = "Public/documents/images/idCard"
            let file = "idCard"+studentImageStruct.studentID+".jpg"
            let returnAll = Student.query(on: req)
                .filter(\Student.studentID == studentImageStruct.studentID)
                .first()
                .map(to: HTTPStatus.self){ student in
                    student?.idCard = idCardPath+file
                    if student != nil {
                        _ = student!.save(on: req)
                        let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
                        do {
                            print(saveURL)
                            try studentImageStruct.image.write(to: saveURL)
                        }
                        catch {
                            print("Cannot save Image")
                            throw Abort(.custom(code: 608, reasonPhrase: "Can't post image."))
                        }
                        return HTTPStatus.ok
                    }
                    else {
                        throw Abort(.custom(code: 608, reasonPhrase: "Can't post image. Because this studentID is incorrect."))
                    }
            }
            return returnAll
    }
    
    // idCardBackSide
    func idCardBackSideHandler(_ req: Request,studentImageStruct: StudentImageStruct) throws -> Future<HTTPStatus> {
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let idCardBackSidePath: String = "http://163.21.117.108/documents/images/idCardBackSide/"
        let folder = "Public/documents/images/idCardBackSide"
        let file = "idCardBackSide"+studentImageStruct.studentID+".jpg"
        let returnAll = Student.query(on: req)
            .filter(\Student.studentID == studentImageStruct.studentID)
            .first()
            .map(to: HTTPStatus.self){ student in
                student?.idCardBackSide = idCardBackSidePath+file
                if student != nil {
                    _ = student!.save(on: req)
                    let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
                    do {
                        print(saveURL)
                        try studentImageStruct.image.write(to: saveURL)
                    }
                    catch {
                        print("Cannot save Image")
                        throw Abort(.custom(code: 608, reasonPhrase: "Can't post image."))
                    }
                    return HTTPStatus.ok
                }
                else {
                    throw Abort(.custom(code: 608, reasonPhrase: "Can't post image. Because this studentID is incorrect."))
                }
        }
        return returnAll
    }
    
    // bankbook
    func bankbookHandler(_ req: Request,studentImageStruct: StudentImageStruct) throws -> Future<HTTPStatus> {
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let bankbookPath: String = "http://163.21.117.108/documents/images/bankbook/"
        let folder = "Public/documents/images/bankbook"
        let file = "bankbook"+studentImageStruct.studentID+".jpg"
        let returnAll = Student.query(on: req)
            .filter(\Student.studentID == studentImageStruct.studentID)
            .first()
            .map(to: HTTPStatus.self){ student in
                student?.bankbook = bankbookPath+file
                if student != nil {
                    _ = student!.save(on: req)
                    let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
                    do {
                        print(saveURL)
                        try studentImageStruct.image.write(to: saveURL)
                    }
                    catch {
                        print("Cannot save Image")
                        throw Abort(.custom(code: 608, reasonPhrase: "Can't post image."))
                    }
                    return HTTPStatus.ok
                }
                else {
                    throw Abort(.custom(code: 608, reasonPhrase: "Can't post image. Because this studentID is incorrect."))
                }
        }
        return returnAll
    }
    
    // portrait
    func portraitHandler(_ req: Request,studentImageStruct: StudentImageStruct) throws -> Future<HTTPStatus> {
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let portraitPath: String = "http://163.21.117.108/documents/images/portrait/"
        let folder = "Public/documents/images/portrait"
        let file = "portrait"+studentImageStruct.studentID+".jpg"
        let returnAll = Student.query(on: req)
            .filter(\Student.studentID == studentImageStruct.studentID)
            .first()
            .map(to: HTTPStatus.self){ student in
                student?.portrait = portraitPath+file
                if student != nil {
                    _ = student!.save(on: req)
                    let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
                    do {
                        print(saveURL)
                        try studentImageStruct.image.write(to: saveURL)
                    }
                    catch {
                        print("Cannot save Image")
                        throw Abort(.custom(code: 608, reasonPhrase: "Can't post image."))
                    }
                    return HTTPStatus.ok
                }
                else {
                    throw Abort(.custom(code: 608, reasonPhrase: "Can't post image. Because this studentID is incorrect."))
                }
        }
        return returnAll
    }

    
    func getStudentImagesHandler(_ req: Request) throws -> GetImageStruct {
        let fileName = try req.parameters.next(String.self)
        let numberType = try req.parameters.next(Int.self)
        var data: Data?
        var folder: String = ""
        switch numberType {
        case 0:
            folder = "Public/documents/images/studentIdCard"
        case 1:
            folder = "Public/documents/images/idCard"
        case 2:
            folder = "Public/documents/images/idCardBackSide"
        case 3:
            folder = "Public/documents/images/bankbook"
        case 4:
            folder = "Public/documents/images/portrait"
        case 5:
            folder = "Public/documents/images/licenseImages"
        case 6:
            folder = "Public/documents/images/gradeImages"
        default:
            print("Please choose which type")
        }
        let file = fileName+".jpg"
        
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        
        //        let folder = "Public/documents/images/licensePicture"
        let saveURL = URL(fileURLWithPath: workPath+folder+"/"+file)
//        print(saveURL)
        do{
            
            data = try Data(contentsOf: saveURL)
            print("success")
            let returnval = GetImageStruct(
                image: data!)
//            print(returnval)
            return returnval
        }
        catch{
//            print("Can't get image")
            throw Abort(.custom(code: 608, reasonPhrase: "Can't get image"))
        }
        
        //                }
        //                return ImageStruct(studentID: studentid, image: data)
        //        }
        
    }
    
    func getStudentOverviewViewHandler(_ req: Request) throws -> Future<View> {
        return Student.query(on: req)
            .all().flatMap({students in
                let context = studentContext(title: "Students", students: students.isEmpty ? nil : students)
                return try req.view().render("studentOverview", context)})
    }
    
    func transformToSunSeedHandler(_ req:Request)throws -> Future<Student>{
        
        return try flatMap(to: Student.self,req.parameters.next(Student.self),req.content.decode(Student.self)) {
            
            student, updatedStudent in
            student.studentType = updatedStudent.studentType
            return student.save(on: req)
        }
    }
    
    func getSunSeedViewHandler(_ req: Request) throws -> Future<View> {
        return Student.query(on: req)
            .filter(\.studentType == 1)
            .all().flatMap({students in
                let context = studentContext(title: "Students", students: students.isEmpty ? nil : students)
                return try req.view().render("sunSeedStudent", context)})
    }
    
    func transformToNormalHandler(_ req:Request)throws -> Future<Student>{
        
        return try flatMap(to: Student.self,req.parameters.next(Student.self),req.content.decode(Student.self)) {
            
            student, updatedStudent in
            student.studentType = updatedStudent.studentType
            return student.save(on: req)
        }
    }

}


struct StudentImageStruct: Content,Encodable {
    let studentID: String
    let image: Data
}
struct LicenseImageStruct: Content,Encodable {
    let licenseID: UUID
    let image: Data
}
struct GradeImageStruct: Content,Encodable {
    let gradeID: UUID
    let image: Data
}
struct Login: Content{
    var studentID: String
    var password: String
}
struct LoginRtn: Content{
    var studentUUID: String
    var studentName: String
}
struct GetImageStruct: Content,Encodable {
    let image: Data
}

struct GetImagesName: Content, Encodable {
    let studentIdCard: String
    let idCard: String
    let idCardBackSide: String
    let bankbook: String
    let portrait: String
}

struct studentContext: Encodable {
    let title: String
    let students: [Student]?
}
