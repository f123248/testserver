import Vapor
import Fluent

struct LicenseController: RouteCollection {
    
    func boot(router: Router) throws {
        
        let licenseRoute = router.grouped("api", "license")
        licenseRoute.post(LicenseWithImageStruct.self, use: createHandler)
        licenseRoute.get(use: getAllHandler)
        licenseRoute.get(License.parameter, use: getHandler)
        licenseRoute.get(License.parameter, "student", use: getStudentHandler)
        licenseRoute.delete("deleteLicense",License.parameter, use: deleteHandler)
        licenseRoute.get("pushLicenseGrade", UUID.parameter,  use: pushLicenseGradeHandler)
        licenseRoute.get("passLicenseGrade", UUID.parameter,  use: passLicenseGradeHandler)
        licenseRoute.get("declineLicenseGrade", UUID.parameter,  use: declineLicenseGradeHandler)
        
//        let imageRoute = router.grouped("api", "picture")
        
    
//        imageRoute.get(String.parameter, use: getImageHandler)
        
        
        let webRoute = router
        webRoute.get("pushLicense", use: pushLicenseViewHandler)
        webRoute.get("passLicense", use: passLicenseViewHandler)
        webRoute.get("licenseDecline", use: licenseDeclineViewHandler)
        webRoute.put("licenseCheckEdit",License.parameter, use: licenseStatePassHandler)
        webRoute.put("licenseCheckEditDecline",License.parameter, use: licenseStateDeclineHandler)
        webRoute.get("licenseCheck",UUID.parameter, use: licenseCheckViewHandler)
        webRoute.get("licensePassCheck",UUID.parameter, use: licensePassCheckViewHandler)
        webRoute.get("licenseDeclineCheck",UUID.parameter, use: licenseDeclineCheckViewHandler)

    }
    
    func createHandler(
    _ req: Request,
    license: LicenseWithImageStruct
    ) throws -> Future<License> {
        let timeInterval:TimeInterval = 0
        let date1 = Date(timeIntervalSince1970: timeInterval)
        let directory = DirectoryConfig.detect()
        let workPath = directory.workDir
        let licenseImagesPath: String = "http://163.21.117.108/documents/images/licenseImages/"
        let folder = "Public/documents/images/licenseImages"
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
        let file = "licenseImages"+license.studentID.uuidString+dateFormatString+".jpg"
        let saveURL = URL(fileURLWithPath: workPath).appendingPathComponent(folder, isDirectory: true).appendingPathComponent(file, isDirectory: false)
        
        //studentIdCard
        //        let directoryStudentIdCard = DirectoryConfig.detect()
        //        let workPathStudentIdCard = directoryStudentIdCard.workDir
        let studentIdCardPath: String = "http://163.21.117.108/documents/images/studentIdCard/"
        let studentIdCardfolder = "Public/documents/images/studentIdCard"
        let studentIdCardfile = "studentIdCard"+license.studentID.uuidString+".jpg"
        let studentIdCardsaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(studentIdCardfolder, isDirectory: true).appendingPathComponent(studentIdCardfile, isDirectory: false)
        
        //idCard
        let idCardPath: String = "http://163.21.117.108/documents/images/idCard/"
        let idfolder = "Public/documents/images/idCard"
        let idfile = "idCard"+license.studentID.uuidString+".jpg"
        let idsaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(idfolder, isDirectory: true).appendingPathComponent(idfile, isDirectory: false)
        
        //idbackCard
        let sidebackIdCardPath: String = "http://163.21.117.108/documents/images/idCardBackSide/"
        let sidebackIdfolder = "Public/documents/images/idCardBackSide"
        let sidebackIdfile = "idCardBackSide"+license.studentID.uuidString+".jpg"
        let sidebackIdsaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(sidebackIdfolder, isDirectory: true).appendingPathComponent(sidebackIdfile, isDirectory: false)
        
        //bankbook
        let bankbookPath: String = "http://163.21.117.108/documents/images/bankbook/"
        let bankbookfolder = "Public/documents/images/bankbook"
        let bankbookfile = "bankbook"+license.studentID.uuidString+".jpg"
        let bankbooksaveURL = URL(fileURLWithPath: workPath).appendingPathComponent(bankbookfolder, isDirectory: true).appendingPathComponent(bankbookfile, isDirectory: false)
        
        do {
            try license.image.write(to: saveURL)
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post license image."))
        }

        do {
            try license.studentIdCard.write(to: studentIdCardsaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post studentIdCard."))
        }
        
        do {
            try license.idCard.write(to: idsaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post idcard."))
        }
        
        do {
            try license.idCardBackSide.write(to: sidebackIdsaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post idCardBackSide."))
        }
        
        
        do {
            try license.bankbook.write(to: bankbooksaveURL)
            
        }
        catch {
            throw Abort(.custom(code: 608, reasonPhrase: "Can't post bankbook."))
        }
        
        let newLicense = License(licenseName: license.licenseName,
                                 licenseType: license.licenseType,
                                 licenseDate: license.licenseDate,
                                 licensePicture: licenseImagesPath+file,
                                 studentID: license.studentID,
                                 licenseState: 0,
                                 verificationRejectedReason: "",
                                 applyDate: Date(),
                                 verifiedDate: date1)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy.MM"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateChange = dateFormatterGet.date(from: license.studentRegisterDate)
        
        _ = Student.query(on: req).filter(\Student.id==license.studentID)
            .first().flatMap(to: Student.self) {
                student in
                student?.studentDepartment=license.studentDepartment
                if let dateTrue = dateChange{
                    student?.studentRegisterDate=dateTrue
                }
                student?.studentAddress=license.studentAddress
                student?.studentPhoneNumber=license.studentPhoneNumber
                
                student?.studentIdCard=studentIdCardPath+studentIdCardfile
                student?.idCard=idCardPath+idfile
                student?.idCardBackSide=sidebackIdCardPath+sidebackIdfile
                student?.bankbook=bankbookPath+bankbookfile
                
                return student!.save(on: req)
        }

        return newLicense.save(on: req)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[License]> {
        return License.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<License> {
        return try req.parameters.next(License.self)
    }
    
    func getStudentHandler(_ req: Request) throws -> Future<Student> {
        return try req
            .parameters.next(License.self)
            .flatMap(to: Student.self) { license in
                license.student.get(on: req)
        }
    }
    
    func deleteHandler(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(License.self)
            .delete(on: req)
            .transform(to: HTTPStatus.noContent)
    }
    

    // MARK: License for back-end
    // handler for pushLicense, passLicense, licenseDecline
    func handler(req: Request, source: String) -> Future<View> {
        var licenseAll:[LicenseOneStruct] = []
        return License.query(on: req)
            
            .join(\Student.id, to: \License.studentID)
            .alsoDecode(Student.self)
            .all()
            .flatMap(to: View.self){ licenses in
                var licenseOne: LicenseOneStruct?
                for license in licenses {
                    if license.0.id != nil {
                        licenseOne = LicenseOneStruct(studentID: license.1.studentID,
                                                      studentName: license.1.studentName,
                                                      licenseName: license.0.licenseName,
                                                      licenseType: license.0.licenseType,
                                                      licenseState: license.0.licenseState,
                                                      licenseID: license.0.id!,
                                                      studentDepartment: license.1.studentDepartment,
                                                      studentRegisterDate: license.1.studentRegisterDate,
                                                      studentAddress: license.1.studentAddress,
                                                      studentPhoneNumber: license.1.studentPhoneNumber,
                                                      studentIdCard: license.1.studentIdCard,
                                                      idCard: license.1.idCard,
                                                      idCardBackSide: license.1.idCardBackSide,
                                                      bankbook: license.1.bankbook,
                                                      licensePicture: license.0.licensePicture,
                                                      licenseDate: license.0.licenseDate,
                                                      verificationRejectedReason: license.0.verificationRejectedReason,
                                                      applyDate: license.0.applyDate,
                                                      verifiedDate: license.0.verifiedDate)
                    }
                    if licenseOne != nil {
                        licenseAll.append(licenseOne!)
                    }
                }
                let context = LicenseAllStruct(title: "title", oneAll: licenseAll)
                print(licenseAll)
                return try req.view().render(source, context)
        }
    }
    
    // pushLicense
    func pushLicenseViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "pushLicense")
    }
    
    // passLicense
    func passLicenseViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "passLicense")
    }
    
    // licenseDecline
    func licenseDeclineViewHandler(_ req: Request) throws -> Future<View> {
        return handler(req: req, source: "licenseDecline")
    }
    
    
    
    // get push license and grade
    func pushLicenseGradeHandler(_ req: Request)
        throws -> Future<[PushLicenseGrade]> {
            let stuID = try req.parameters.next(UUID.self)
            var pushAll: [PushLicenseGrade] = []
            _ = License.query(on: req)
                .filter(\License.studentID == stuID)
                .filter(\License.licenseState == 0)
                .all()
                .map(to: [PushLicenseGrade].self) { licenses in
                   
                    for license in licenses {
                       let pushOne = PushLicenseGrade(name: license.licenseName,
                                                   pushDate: license.applyDate,
                                                   state: 0)
                        
                        pushAll.append(pushOne)
                        
                    }
//                    pushAll = pushAll.sorted(by: {$0.pushDate.compare($1.pushDate) == .orderedDescending})

                    return pushAll
            }
            return GradeProgression.query(on: req)
                .filter(\GradeProgression.studentID == stuID)
                .filter(\GradeProgression.gradeState == 0)
                .all()
                .map(to: [PushLicenseGrade].self) { grades in
                    
                    for grade in grades {
                     let  pushOne = PushLicenseGrade(name: grade.applyYear+grade.applySemester,
                                                   pushDate: grade.applyDate,
                                                   state: 1)
                        
                        pushAll.append(pushOne)
                        
                    }
                    pushAll = pushAll.sorted(by: {$0.pushDate.compare($1.pushDate) == .orderedDescending})

                    return pushAll
            }
    }
    
    // get pass license and grade
    func passLicenseGradeHandler(_ req: Request)
        throws -> Future<[VerifiedLicenseGrade]> {
            let stuID = try req.parameters.next(UUID.self)
            var pushAll: [VerifiedLicenseGrade] = []
            _ = License.query(on: req)
                .filter(\License.studentID == stuID)
                .filter(\License.licenseState == 1)
                .all()
                .map(to: [VerifiedLicenseGrade].self) { licenses in
                    
                    for license in licenses {
                        let pushOne = VerifiedLicenseGrade(name: license.licenseName,
                                                       verifiedDate: license.verifiedDate,
                                                       state: 0)
                        
                        pushAll.append(pushOne)
                        
                    }
                    //                    pushAll = pushAll.sorted(by: {$0.pushDate.compare($1.pushDate) == .orderedDescending})
                    
                    return pushAll
            }
            return GradeProgression.query(on: req)
                .filter(\GradeProgression.studentID == stuID)
                .filter(\GradeProgression.gradeState == 1)
                .all()
                .map(to: [VerifiedLicenseGrade].self) { grades in
                    
                    for grade in grades {
                        let  pushOne = VerifiedLicenseGrade(name: grade.applyYear+grade.applySemester,
                                                        verifiedDate: grade.verifiedDate,
                                                        state: 1)
                        
                        pushAll.append(pushOne)
                        
                    }
                    pushAll = pushAll.sorted(by: {$0.verifiedDate.compare($1.verifiedDate) == .orderedDescending})
                    
                    return pushAll
            }
    }
    
    // get decline license and grade
    func declineLicenseGradeHandler(_ req: Request)
        throws -> Future<[NoneVerifiedLicenseGrade]> {
            let stuID = try req.parameters.next(UUID.self)
            var pushAll: [NoneVerifiedLicenseGrade] = []
            _ = License.query(on: req)
                .filter(\License.studentID == stuID)
                .filter(\License.licenseState == 2)
                .all()
                .map(to: [NoneVerifiedLicenseGrade].self) { licenses in
                    
                    for license in licenses {
                        let pushOne = NoneVerifiedLicenseGrade(name: license.licenseName,
                                                           verifiedDate: license.verifiedDate,
                                                           state: 0, verificationRejectedReason: license.verificationRejectedReason)
                        
                        pushAll.append(pushOne)
                        
                    }
                    //                    pushAll = pushAll.sorted(by: {$0.pushDate.compare($1.pushDate) == .orderedDescending})
                    
                    return pushAll
            }
            return GradeProgression.query(on: req)
                .filter(\GradeProgression.studentID == stuID)
                .filter(\GradeProgression.gradeState == 2)
                .all()
                .map(to: [NoneVerifiedLicenseGrade].self) { grades in
                    
                    for grade in grades {
                        let  pushOne = NoneVerifiedLicenseGrade(name: grade.applyYear+grade.applySemester,
                                                            verifiedDate: grade.verifiedDate,
                                                            state: 1, verificationRejectedReason: grade.verificationRejectedReason)
                        
                        pushAll.append(pushOne)
                        
                    }
                    pushAll = pushAll.sorted(by: {$0.verifiedDate.compare($1.verifiedDate) == .orderedDescending})
                    
                    return pushAll
            }
    }
    
    
    func handlerCheckViewHandler(req: Request, source: String) throws -> Future<View> {
        let licenseID = try req.parameters.next(UUID.self)
        var licenseStruct: LicenseOneStruct?
        return License.query(on: req)
            .filter(\License.id==licenseID)
            .join(\Student.id, to: \License.studentID)
            .alsoDecode(Student.self)
            .first()
            .flatMap(to: View.self){ license in
                //                var licenseTest: LicenseOneStruct?
                if license != nil {
                    licenseStruct = LicenseOneStruct(studentID: license!.1.studentID,
                                             studentName: license!.1.studentName,
                                             licenseName: license!.0.licenseName,
                                             licenseType: license!.0.licenseType,
                                             licenseState: license!.0.licenseState,
                                             licenseID: license!.0.id!,
                                             studentDepartment: license!.1.studentDepartment,
                                             studentRegisterDate: license!.1.studentRegisterDate,
                                             studentAddress: license!.1.studentAddress,
                                             studentPhoneNumber: license!.1.studentPhoneNumber,
                                             studentIdCard: license!.1.studentIdCard,
                                             idCard: license!.1.idCard,
                                             idCardBackSide: license!.1.idCardBackSide,
                                             bankbook: license!.1.bankbook,
                                             licensePicture: license!.0.licensePicture,
                                             licenseDate: license!.0.licenseDate,
                                             verificationRejectedReason: license!.0.verificationRejectedReason,
                                             applyDate: license!.0.applyDate,
                                             verifiedDate: license!.0.verifiedDate)
                    
                    //                    print("studentID: ", test1?.studentID)
                    //
                }
                let context = LicenseAllStruct2(title: "title", oneAll: licenseStruct!)
                
                return try req.view().render(source, context)
                
        }
    }
    
    // LicenseCheck
    func licenseCheckViewHandler(_ req: Request) throws -> Future<View> {
        return try handlerCheckViewHandler(req: req, source: "licenseCheck")
    }
    
    // LicensePassCheck
    func licensePassCheckViewHandler(_ req: Request) throws -> Future<View> {
        return try handlerCheckViewHandler(req: req, source: "licensePassCheck")
    }
    
    // LicenseDeclineCheck
    func licenseDeclineCheckViewHandler(_ req: Request) throws -> Future<View> {
        return try handlerCheckViewHandler(req: req, source: "licenseDeclineCheck")
    }
    
    // Put api for licensePass button
    func licenseStatePassHandler(_ req: Request) throws -> Future<License> {
        return try flatMap(to: License.self,
                           req.parameters.next(License.self),
                           req.content.decode(LicenseOneStruct.self)) {
                            license, updatedLicense in
                            license.licenseState = updatedLicense.licenseState
                            license.verifiedDate = Date()
                            return license.save(on: req)
        }
    }
    
    // Put api for licenseDecline button
    func licenseStateDeclineHandler(_ req: Request) throws -> Future<License> {
        return try flatMap(to: License.self,
                           req.parameters.next(License.self),
                           req.content.decode(LicenseOneStruct.self)) {
                            license, updatedLicense in
                            license.licenseState = updatedLicense.licenseState
                            license.verificationRejectedReason = updatedLicense.verificationRejectedReason
                            license.verifiedDate = Date()
                            return license.save(on: req)
        }
    }
    

}

struct LicenseOneStruct: Content,Encodable {
    let studentID: String
    let studentName: String
    let licenseName: String
    let licenseType: Int
    let licenseState: Int
    let licenseID: UUID
    let studentDepartment: String
    let studentRegisterDate: Date
    let studentAddress: String
    let studentPhoneNumber: String
    let studentIdCard: String
    let idCard: String
    let idCardBackSide: String
    let bankbook: String
    let licensePicture: String
    let licenseDate: String
    let verificationRejectedReason: String
    let applyDate: Date
    let verifiedDate: Date
}

struct LicenseAllStruct: Content, Encodable {
    let title: String
    let oneAll: [LicenseOneStruct]
}

struct ImageStruct: Content,Encodable {
    let studentID: String
    let image: Data
}

struct LicenseAllStruct2: Content,Encodable {
    let title: String
    let oneAll: LicenseOneStruct
}

struct PushLicenseGrade: Content, Encodable {
    let name: String
    let pushDate: Date
    let state: Int
}

struct VerifiedLicenseGrade: Content, Encodable {
    let name: String
    let verifiedDate: Date
    let state: Int
}

struct NoneVerifiedLicenseGrade: Content, Encodable {
    let name: String
    let verifiedDate: Date
    let state: Int
    let verificationRejectedReason: String
}


struct LicenseWithImageStruct: Content,Encodable {
    var licenseName: String
    var licenseType: Int
    var image: Data
    var licenseDate: String
    var studentID: Student.ID
    
    var studentDepartment: String
    var studentRegisterDate: String
    var studentAddress: String
    var studentPhoneNumber: String
    
    var studentIdCard: Data
    var idCard: Data
    var idCardBackSide: Data
    var bankbook: Data
}
