
import FluentPostgreSQL
import Vapor
import Leaf

public func configure(
    _ config: inout Config,
    _ env: inout Environment,
    _ services: inout Services
    ) throws {
    // Register providers first
    try services.register(FluentPostgreSQLProvider())
    
    //Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)
    var middlewares = MiddlewareConfig()
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self)
    services.register(middlewares)
    
    // Configure a database
    var databases = DatabasesConfig()
    let databaseConfig: PostgreSQLDatabaseConfig
    if let url = Environment.get("DATABASE_URL") {
        databaseConfig = PostgreSQLDatabaseConfig(url: url)!
    }
    else {
        databaseConfig = PostgreSQLDatabaseConfig(
            hostname: "127.0.0.1",
            username: "postgres",
            database: "testdb",
            password: "utaipei")
    }
    let database = PostgreSQLDatabase(config: databaseConfig)
    databases.add(database: database, as: .psql)
    services.register(databases)
    
    // Configure migraitons
    var migrations = MigrationConfig()
    migrations.add(model: Course.self, database: .psql)
    migrations.add(model: Student.self, database: .psql)
    migrations.add(model: RecommendCourse.self, database: .psql)
    migrations.add(model: CourseEvaluation.self, database: .psql)
    migrations.add(model: Leave.self, database: .psql)
    
    migrations.add(model: RollCall.self, database: .psql)
    migrations.add(model: VotePivot.self, database: .psql)
    migrations.add(model: InCoursePivot.self, database: .psql)
    migrations.add(model: ApplyPivot.self, database: .psql)
    migrations.add(model: FavoritePivot.self, database: .psql)
    
    migrations.add(model: FavRecPivot.self, database: .psql)
    migrations.add(model: Admin.self, database: .psql)
    migrations.add(model: CourseSatisfaction.self, database: .psql)
    migrations.add(model: CourseSatisfactionAns.self, database: .psql)
    migrations.add(model: OpenCoursePivot.self, database: .psql)
    
    migrations.add(model: ReopenCoursePivot.self, database: .psql)
    migrations.add(model: RCAccount.self, database: .psql)
    migrations.add(model: License.self, database: .psql)
    migrations.add(model: GradeProgression.self, database: .psql)
    migrations.add(model: RCACoursePivot.self, database: .psql)
    
    migrations.add(model: RCAStudentPivot.self, database: .psql)
    migrations.add(model: Point.self, database: .psql)
    migrations.add(model: Lottery.self, database: .psql)
    migrations.add(model: Notification.self, database: .psql)
    migrations.add(model: NoticeStudentPivot.self, database: .psql)
    
    migrations.add(model: Banner.self, database: .psql)
    migrations.add(model: LuckyDay.self, database: .psql)
    migrations.add(model: LicenseGradeTime.self, database: .psql)
    
    services.register(migrations)
    
    let leafProvider = LeafProvider()
    try services.register(leafProvider)
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
    //原本只能接受1MB的資料，對於圖片上傳來說不太夠用
    var nioServerConfig = NIOServerConfig.default(hostname: "163.21.117.108", port: 8887)
    //修改為 4MB
    nioServerConfig.maxBodySize = 1024 * 1024 * 1024
    services.register(nioServerConfig)
}
