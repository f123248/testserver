
function luckyDayEdit(luckyDayid,luckyDayPoint){
    var percent = document.getElementById("percent").value;
    var monthTotal = document.getElementById("monthTotal").value;
    
    if ((percent <= 0 && percent != -1) || percent >= 1){
        alert("請輸入正確的機率");
    }else{
        alert("編輯LuckyDay：" + luckyDayPoint);
        var data = {
            "id": luckyDayid,
            "points": parseInt(luckyDayPoint),
            "percent": parseFloat(percent),
            "monthTotal": parseInt(monthTotal)
        };
        var dataJson = JSON.stringify(data);
        
        var url = window.location.href;
        var url2 = url.split("editLuckyDay");
        var url3 = url2[0]+"api/luckyDay/updateLuckyDay/"+luckyDayid;
        url2 = url2[0]+"setLuckyDay";
        
        
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url3,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url2;
               }
               });
    }
}

function backToPass(){
    var url = window.location.href;
    var url2 = url.split("editLuckyDay");
    url2 = url2[0]+"setLuckyDay";
    location.href = url2;
}
