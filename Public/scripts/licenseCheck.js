function licensePass(studentID,studentName,licenseName,licenseType,licenseID,studentDepartment,studentRegisterDate,studentAddress,studentPhoneNumber,studentIdCard,idCard,idCardBackSide,bankbook,licensePicture,licenseDate,applyDate){
    var type = parseInt(licenseType);
    
    // fake data
    var verificationRejectedReason = "";
    var studentRegisterDate1 = document.getElementById('+studentRegisterDate').innerHTML;
    var studentRegisterDate2 = moment(studentRegisterDate1,"YYYY-MM-DD hh:mm A").format();
    var applyDate1 = document.getElementById('+applyDate').innerHTML;
    var applyDate2 = moment(applyDate1,"YYYY-MM-DD hh:mm A").format();
    var verifiedDate = moment().format();

//    console.log("licenseID: " + studentRegisterDate1);
//    console.log("licenseID: " + studentRegisterDate2);
    
    var check = getConfirmation("確定通過"+studentName+"的"+licenseName+"證照申請?")
    if (check){
        var data = {
            "studentID": studentID,
            "studentName": studentName,
            "licenseName": licenseName,
            "licenseType": type,
            "licenseID":licenseID,
            "studentDepartment":studentDepartment,
            "studentRegisterDate": studentRegisterDate2,
            "studentAddress":studentAddress,
            "studentPhoneNumber":studentPhoneNumber,
            "studentIdCard":studentIdCard,
            "idCard":idCard,
            "idCardBackSide":idCardBackSide,
            "bankbook":bankbook,
            "licensePicture":licensePicture,
            "licenseDate":licenseDate,
            "verificationRejectedReason":verificationRejectedReason,
            "applyDate":applyDate2,
            "verifiedDate":verifiedDate,
            "licenseState": 1
        };
        var dataJson = JSON.stringify(data);
        var url = window.location.href;
        url = url.split("licenseCheck");
        var url2 = url[0]+"pushLicense";
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url[0]+"licenseCheckEdit/"+licenseID,
//               url: window.location.href+"/"+licenseID,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url2;
               }
               });
    }
}

function licenseDecline(studentID,studentName,licenseName,licenseType,licenseID,studentDepartment,studentRegisterDate,studentAddress,studentPhoneNumber,studentIdCard,idCard,idCardBackSide,bankbook,licensePicture,licenseDate,applyDate){
    var type = parseInt(licenseType);
    
    // fake data
//    var verificationRejectedReason = "";
    var studentRegisterDate1 = document.getElementById('+studentRegisterDate').innerHTML;
    var studentRegisterDate2 = moment(studentRegisterDate1,"YYYY-MM-DD hh:mm A").format();
    var applyDate1 = document.getElementById('+applyDate').innerHTML;
    var applyDate2 = moment(applyDate1,"YYYY-MM-DD hh:mm A").format();
    var verifiedDate = moment().format();
    
    //    console.log("licenseID: " + studentRegisterDate1);
    //    console.log("licenseID: " + studentRegisterDate2);
    
    var check = prompt("請輸入審核不通過原因", "缺少資料");
    if (check != null){
        var data = {
            "studentID": studentID,
            "studentName": studentName,
            "licenseName": licenseName,
            "licenseType": type,
            "licenseID":licenseID,
            "studentDepartment":studentDepartment,
            "studentRegisterDate": studentRegisterDate2,
            "studentAddress":studentAddress,
            "studentPhoneNumber":studentPhoneNumber,
            "studentIdCard":studentIdCard,
            "idCard":idCard,
            "idCardBackSide":idCardBackSide,
            "bankbook":bankbook,
            "licensePicture":licensePicture,
            "licenseDate":licenseDate,
            "verificationRejectedReason":check,
            "applyDate":applyDate2,
            "verifiedDate":verifiedDate,
            "licenseState": 2
        };
        var dataJson = JSON.stringify(data);
        var url = window.location.href;
        url = url.split("licenseCheck");
        var url2 = url[0]+"pushLicense";
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url[0]+"licenseCheckEditDecline/"+licenseID,
               //               url: window.location.href+"/"+licenseID,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url2;
               }
               });
    }
}
