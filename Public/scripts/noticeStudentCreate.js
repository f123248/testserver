function noticeCreate(){
    var studentID = document.getElementById("studentID").value;
    var title = document.getElementById("title").value;
    var content = document.getElementById("content").value;
    var type = 2; //個人通知
    var courseType = 3; //教發中心
    var date = moment().format();
    var check = getConfirmation("確定新增通知？");
    if(check){
        var data = {
            "title":title,
            "content":content,
            "type":type,
            "courseType":courseType,
            "pushTime":date
        };
        
        var url = window.location.href;
        url = url.split("noticeStudentCreate");
        var url2 = url[0]+"api/notification/student/"+studentID;
        var dataJson = JSON.stringify(data);
        
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               alert(errorThrown+" "+studentID);
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url[0]+"noticeStudent";
               }
               });
    }
}

function backToPass(){
    var url = window.location.href;
    url = url.split("noticeStudentCreate");
    url = url[0]+"noticeStudent";
    location.href = url;
}

