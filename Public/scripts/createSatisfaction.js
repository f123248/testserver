var type = [];
var questId = [];
var scaleId=[];
var choiceId = [];
var multiChoiceId = [];
var numId = 0;
function createQuestion(){
    var courseId = document.getElementById("courseId").value;
    var num = document.getElementById("table").rows.length;
    var title = [];
    var quest = [];
    var kind = [];
    var scale = [];
    var scaleall = [];
    var quee = {};
    var questNum=0;
    for(var i=0;i<scaleId.length;i++){
        title.push("問題"+(i+1));
        kind.push(0);
        var id = scaleId[i];
        var question = document.getElementById("quest"+id).value;
        quest.push(question);
        
        var scale1 = document.getElementById("scale1"+id).value;
        var scale2 = document.getElementById("scale2"+id).value;
        scale.push(scale1);
        scale.push(scale2);
//        console.log(scale);
        scaleall.push(scale);
        scale = [];
        questNum = i+1;
    }
    
    for(var i = 0; i < choiceId.length;i++) {
        title.push("問題"+(questNum+1));
        kind.push(2);
        var id = choiceId[i];
        console.log(id);
        var question = document.getElementById("quest"+id).value;
        quest.push(question);
        for (var j = 0; j < choiceBig[id].length; j++) {
            console.log(choiceBig[i]);
            
            var scale1 = document.getElementById(choiceBig[id][j]).value;
            
            scale.push(scale1);
        }
        scaleall.push(scale);
        scale = [];
        console.log(scaleall);
        questNum+=1;
        
    }
    for(var i = 0; i < multiChoiceId.length;i++) {
        title.push("問題"+(questNum+1));
        kind.push(3);
        var id = multiChoiceId[i];
//        console.log(id);
        var question = document.getElementById("quest"+id).value;
        quest.push(question);
        for (var j = 0; j < multiChoiceBig[id].length; j++) {
//            console.log(choiceBig[i]);
            
            var scale1 = document.getElementById(multiChoiceBig[id][j]).value;
            
            scale.push(scale1);
        }
        scaleall.push(scale);
        scale = [];
//        console.log(scaleall);
        questNum+=1;
        
    }
    for(var i=0;i<questId.length;i++){
        title.push("問題"+(questNum+1));
        kind.push(1);
        var id = questId[i];
        var question = document.getElementById("quest"+id).value;
        quest.push(question);
        var scale5 = [""];
        scaleall.push(scale5);
        scale5 = [];
        questNum+=1;
    }
    
    var check = getConfirmation("確定新增問卷？");
    
//    var scaleBox = QuestionBox(questionbox: scale);
    
    if(check){
        var data = {
            "courseID":courseId,
            "title":title,
            "quest":quest,
            "kind":kind,
            "scale":scaleall
        };
        
        var url = window.location.href;
        url = url.split("createSatisfaction");
        var url2 = url[0]+"api/courseSatisfaction";
        var dataJson = JSON.stringify(data);
        
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = window.location.href;
               }
               });
    }
}



function addQuestion(questionType){
    
    var table = document.getElementById("table");
    
    var num = document.getElementById("table").rows.length;

    var row = table.insertRow(num);
    row.id = numId;
    
    var num = num + 1;
    
    var cell = row.insertCell(0);
    
    if (questionType == 0){
        //type.push(0);
        scaleId.push(numId);
//        console.log(scaleId);
        cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text id=quest'+numId+' /> <br><br> <input type=text value="" placeholder="此欄為5分" id=scale1'+numId+' />&nbsp;&nbsp;&nbsp;<input type=text value ="" placeholder="此欄為1分"id=scale2'+numId+' />&nbsp;&nbsp;&nbsp;五分量表<button onclick="removeRow(this,'+numId+',0)">刪除</button>';
    }else if (questionType == 1){
        //type.push(1);
        questId.push(numId);
        cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text id=quest'+numId+' /> <br><br>&nbsp;&nbsp;&nbsp;簡答<button onclick="removeRow(this,'+numId+',1)">刪除</button>';
    }else if (questionType == 2){
        cell.id = "tdchoice"+numId;
        
        choiceId.push(numId);

        choiceBig[numId] = [];
        choiceNumCount[numId] = 0;
        
        cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text id=quest'+numId+'> <br><br><input type=text id=choicevalue'+numId+'0 value="" placeholder="輸入選項" id=choice1'+numId+' />&nbsp;&nbsp;&nbsp;<button onclick="addChoice(tdchoice'+numId+',2,'+numId+')">新增選項</button>&nbsp;&nbsp;單選題<button onclick="removeRow(this,'+numId+',2)">刪除</button>';
        var putToArray = "choicevalue"+numId+0;

        choiceBig[numId][0] = putToArray;
    
    }
    else if (questionType == 3){
        cell.id = "tdnmultichoice"+numId;

        multiChoiceId.push(numId);
        
        multiChoiceBig[numId] = [];
        multiChoiceNumCount[numId] = 0;
        
        cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text id=quest'+numId+'><br><br><input type=text id=multichoicevalue'+numId+'0 value="" placeholder="輸入選項" id=choice1'+numId+' />&nbsp;&nbsp;&nbsp;<button onclick="addChoice(tdnmultichoice'+numId+',3,'+numId+')">新增選項</button>&nbsp;&nbsp;多選題<button onclick="removeRow(this,'+numId+',0)">刪除</button>';
        
        var putToArray = "multichoicevalue"+numId+0;
        multiChoiceBig[numId][0] = putToArray;
    }
    numId+=1;
}


var choiceBig = [];
var choiceNumCount = [];
var multiChoiceBig = [];
var multiChoiceNumCount = [];

function addChoice(id,type,bignum) {
    if (type == 2){
        choiceNumCount[bignum]+=1;
        var putChoice = "choicevalue"+bignum+choiceNumCount[bignum];
        id.innerHTML += '<div id=divchoice'+bignum+''+choiceNumCount[bignum]+'><input type=text id=choicevalue'+bignum+''+choiceNumCount[bignum]+' value="" placeholder="輸入選項"/>&nbsp;&nbsp;&nbsp;<button onclick="removeChoice(divchoice'+bignum+''+choiceNumCount[bignum]+',2,'+bignum+','+putChoice+')">刪除</button></div>';
        
        choiceBig[bignum].push(putChoice);
    }
    else if (type == 3) {
        
        multiChoiceNumCount[bignum]+=1;
        var putChoice = "multichoicevalue"+bignum+multiChoiceNumCount[bignum];
        
        id.innerHTML += '<div id=divmultichoice'+bignum+''+multiChoiceNumCount[bignum]+'><input type=text id=multichoicevalue'+bignum+''+multiChoiceNumCount[bignum]+' value="" placeholder="輸入選項"/>&nbsp;&nbsp;&nbsp;<button onclick="removeChoice(divmultichoice'+bignum+''+multiChoiceNumCount[bignum]+',3,'+bignum+','+putChoice+')">刪除</button></div>';
        
        multiChoiceBig[bignum].push(putChoice);
    }
}

function removeChoice(divalue,type,num,choiceidnum) {
    if (type == 2) {

//        console.log(choiceidnum.id);
        divalue.remove();
//        console.log(test);
        var index = choiceBig[num].indexOf(choiceidnum.id);
        
        choiceBig[num].splice(index,1);

//        console.log(choiceBig);

    }
    else if (type == 3) {
        
        divalue.remove();
        var index = multiChoiceBig[num].indexOf(choiceidnum.id);
        multiChoiceBig[num].splice(index,1);
    }
    

    
}

function removeRow(r,id,type) {
    
    if(type==0){
        var index = scaleId.indexOf(id);
        scaleId.splice(index,1);
    }
    else if(type==1) {
        var index = questId.indexOf(id);
        questId.splice(index,1);
    }
    else if(type==2){
        var index = choiceId.indexOf(id);
        choiceId.splice(index,1);
    }
    else {
        var index = multiChoiceId.indexOf(id);
        multiChoiceId.splice(index,1);
    }
    
    //type.splice(index,1);
    
    var i = r.parentNode.parentNode.rowIndex;
    
    document.getElementById("table").deleteRow(i);
}

function checkQuestion(){
    document.getElementById("table").innerHTML = "";
    var courseId = document.getElementById("courseId").value;
    var url = window.location.href;
    url = url.split("createSatisfaction");
    var url2 = url[0]+"api/courseSatisfaction/"+courseId;
    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(data,status){
               if(data.title==""){
                   alert("無資料");
               }
               var i = 0,count=0;
               for(i=0;i<data.title.length;i++){
                   if(data.kind[i]==0){
                   addScaleQuestion(data.quest[i],data.scale[i][count],data.scale[i][count+1]);
//                   count+=1;
                   }
                   else if(data.kind[i]==1){
                   addQAQuestion(data.quest[i]);
                   }
                   else if (data.kind[i]==2) {
           
                    addChoiceQuestion(data.quest[i],data.scale[i]);
//           console.log(data.scale[i]);
//           console.log(data.scale[2]);
                   }
                    else if (data.kind[i]==3) {
           
                    addMultiChoiceQuestion(data.quest[i],data.scale[i]);
           //           console.log(data.scale[i]);
           //           console.log(data.scale[2]);
           }
               }
           }
           });
}

function addScaleQuestion(question,scale1,scale2){
    
    var table = document.getElementById("table");
    
    var num = document.getElementById("table").rows.length;
    
    var row = table.insertRow(num);
    row.id = numId;
    
    var cell = row.insertCell(0);
    
    //type.push(0);
    scaleId.push(numId);
    cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text value="'+question+'" id=quest'+numId+'> <br><br> <input type=text value="'+scale1+'" id=scale1'+numId+' />&nbsp;&nbsp;&nbsp;<input type=text value ="'+scale2+'" id=scale2'+numId+'/>&nbsp;&nbsp;&nbsp;五分量表<button onclick="removeRow(this,'+numId+',0)">刪除</button>';
    numId+=1;
}

function addQAQuestion(question){
    
    var table = document.getElementById("table");
    
    var num = document.getElementById("table").rows.length;
    
    var row = table.insertRow(num);
    row.id = numId;
    
    var cell = row.insertCell(0);
   
    questId.push(numId);
    cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text value="'+question+'" id=quest'+numId+' /> <br><br>&nbsp;&nbsp;&nbsp;簡答<button onclick="removeRow(this,'+numId+',1)">刪除</button>';
    numId+=1;
}

function addChoiceQuestion(question,choiceArray){
    
    var table = document.getElementById("table");
    
    var num = document.getElementById("table").rows.length;
    
    var row = table.insertRow(num);
    row.id = numId;
    
    var cell = row.insertCell(0);
    
    choiceId.push(numId);
    console.log(choiceArray);
    cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text value="'+question+'" id=quest'+numId+' /> <br><br>';
    for (i=0; i < choiceArray.length; i++) {
        cell.innerHTML += '<input type=text id=choicevalue'+numId+''+i+' value="'+choiceArray[i]+'" id=choiceArray[i]'+numId+' /><br>';

    }
    cell.innerHTML += '單選題<button onclick="removeRow(this,'+numId+',2)">刪除</button>';
    numId+=1;
}
function addMultiChoiceQuestion(question,choiceArray){
    
    var table = document.getElementById("table");
    
    var num = document.getElementById("table").rows.length;
    
    var row = table.insertRow(num);
    row.id = numId;
    
    var cell = row.insertCell(0);
    
    multiChoiceId.push(numId);
//    console.log(choiceArray);
    cell.innerHTML = '題目:&nbsp;&nbsp;<input type=text value="'+question+'" id=quest'+numId+' /> <br><br>';
    for (i=0; i < choiceArray.length; i++) {
        cell.innerHTML += '<input type=text id=multichoicevalue'+numId+''+i+' value="'+choiceArray[i]+'" id=multichoiceArray[i]'+numId+' /><br>';
        
    }
    cell.innerHTML += '多選題<button onclick="removeRow(this,'+numId+',3)">刪除</button>';
    numId+=1;
}

function removeAll(){
    document.getElementById("table").innerHTML="";
}
