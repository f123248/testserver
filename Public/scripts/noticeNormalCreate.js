function noticeCreate(){
    var title = document.getElementById("title").value;
    var content = document.getElementById("content").value;
    var type = 0; //一般公告
    var courseType = 3; //教發中心
    var date = moment().format();
    var check = getConfirmation("確定新增通知？");
    if(check){
        var data = {
            "title":title,
            "content":content,
            "type":type,
            "courseType":courseType,
            "pushTime":date
        };
        
        var url = window.location.href;
        url = url.split("noticeNormalCreate");
        var url2 = url[0]+"api/notification";
        var dataJson = JSON.stringify(data);
        
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url[0]+"noticeNormal";
               }
               });
    }
}

function backToPass(){
    var url = window.location.href;
    url = url.split("noticeNormalCreate");
    url = url[0]+"noticeNormal";
    location.href = url;
}
