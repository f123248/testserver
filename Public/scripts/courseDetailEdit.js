var courseId,courseName,courseType,campus,courseState,numberOfApply;
var courseDate=[];
var content=[];
var count=0;
var id=[];
function courseEdit(){
    var courseCost = document.getElementById('courseCost').value;
    
    var targetStudent = parseInt(document.getElementById('targetStudent').value);
    var courseTeacher = document.getElementById('courseTeacher').value;
    var coursePlace = document.getElementById('coursePlace').value;
    var courseCost = parseInt(document.getElementById('courseCost').value);
    var courseDate=[];
    var courseContent=[];
    var i=0;
    var dateError=0;
    for(i=0;i<id.length;i++){
        var courseStartDate = document.getElementById('courseStartDate'+id[i]).value;
        var courseStartTime = document.getElementById('courseStartTime'+id[i]).value;
        var courseEndDate = courseStartDate;
        var courseEndTime = document.getElementById('courseEndTime'+id[i]).value;
        
        courseStartDate = courseStartDate.split("-");
        courseStartDate = courseStartDate[1]+"-"+courseStartDate[2]+"-"+courseStartDate[0];
        courseEndDate = courseEndDate.split("-");
        courseEndDate = courseEndDate[1]+"-"+courseEndDate[2]+"-"+courseEndDate[0];
        var courseStartTimeData = moment(courseStartDate+" "+courseStartTime,"MM-DD-YYYY h:mm A").format();
        var courseEndTimeData = moment(courseEndDate+" "+courseEndTime, "MM-DD-YYYY h:mm A").format();
        if(courseEndTimeData<courseStartTimeData || courseStartTimeData == "Invalid date" || courseEndTimeData == "Invalid date"){
            dateError=1;
        }
        courseDate.push(courseStartTimeData);
        courseDate.push(courseEndTimeData);
        
        var content = document.getElementById('content'+id[i]).value;
        courseContent.push(content);
    }
    
    var applyStartDate = document.getElementById('firstApplyStartDate').value;
    var applyStartTime = document.getElementById('firstApplyStartTime').value;
    var applyEndDate = document.getElementById('firstApplyEndDate').value;
    var applyEndTime = document.getElementById('firstApplyEndTime').value;
    
    applyStartDate = applyStartDate.split("-");
    applyStartDate = applyStartDate[1]+"-"+applyStartDate[2]+"-"+applyStartDate[0];
    applyEndDate = applyEndDate.split("-");
    applyEndDate = applyEndDate[1]+"-"+applyEndDate[2]+"-"+applyEndDate[0];
    var firstApplyStartTimeData = moment(applyStartDate+" "+applyStartTime,"MM-DD-YYYY h:mm A").format();
    var firstApplyEndTimeData = moment(applyEndDate+" "+applyEndTime, "MM-DD-YYYY h:mm A").format();
    var firstNumberOfStudents = parseInt(document.getElementById('firstNumberOfStudents').value);
    
    applyStartDate = document.getElementById('secondApplyStartDate').value;
    applyStartTime = document.getElementById('secondApplyStartTime').value;
    applyEndDate = document.getElementById('secondApplyEndDate').value;
    applyEndTime = document.getElementById('secondApplyEndTime').value;
    
    applyStartDate = applyStartDate.split("-");
    applyStartDate = applyStartDate[1]+"-"+applyStartDate[2]+"-"+applyStartDate[0];
    applyEndDate = applyEndDate.split("-");
    applyEndDate = applyEndDate[1]+"-"+applyEndDate[2]+"-"+applyEndDate[0];
    var secondApplyStartTimeData = moment(applyStartDate+" "+applyStartTime,"MM-DD-YYYY h:mm A").format();
    var secondApplyEndTimeData = moment(applyEndDate+" "+applyEndTime, "MM-DD-YYYY h:mm A").format();
    var secondNumberOfStudents = parseInt(document.getElementById('secondNumberOfStudents').value);
    
    var applyMethod = parseInt(document.getElementById('applyMethod').value);
    var enrollPost = parseInt(document.getElementById('enrollPost').value);
    var courseIntroduction = document.getElementById('courseIntroduction').value;
    var other = document.getElementById('other').value;
    
    if(courseDate.length==0){
        courseDate.push(moment("1970-01-01").format());
    }
    if(courseContent.length==0){
        courseContent.push("");
    }
    
    if(Number.isInteger(firstNumberOfStudents) && Number.isInteger(secondNumberOfStudents)&& Number.isInteger(courseCost)){
        if(dateError==1 || firstApplyStartTimeData == "Invalid date" || firstApplyEndTimeData == "Invalid date" || secondApplyStartTimeData == "Invalid date" || secondApplyEndTimeData == "Invalid date"){
            alert("請輸入正確的日期及時間");
        }
        else if(firstApplyEndTimeData>secondApplyStartTimeData){
            alert("「第二階段報名開始時間」早於「第一階段報名結束時間」");
        }
        else{
            alert("編輯課程：" + courseName);
            
            var type = parseInt(courseType);
            courseState = parseInt(courseState);
            var data = {
                "id": courseId,
                "courseName": courseName,
                "courseType": courseType,
                "campus": campus,
                "courseDate": courseDate,
                "courseTeacher":courseTeacher,
                "coursePlace": coursePlace,
                "courseCost": courseCost,
                "targetStudent": targetStudent,
                "courseState": courseState,
                "numberOfApply": numberOfApply,
                "firstApplyStartTime": firstApplyStartTimeData,
                "firstApplyEndTime": firstApplyEndTimeData,
                "firstNumberOfStudents": firstNumberOfStudents,
                "secondApplyStartTime": secondApplyStartTimeData,
                "secondApplyEndTime": secondApplyEndTimeData,
                "secondNumberOfStudents": secondNumberOfStudents,
                "applyMethod": applyMethod,
                "enrollPost": enrollPost,
                "courseIntroduction": courseIntroduction,
                "other": other,
                "courseContent":courseContent
            };
            var dataJson = JSON.stringify(data);
            var url = window.location.href;
            var url2 = url.split("courseDetailEdit");
            
            if(courseState == 0 ){
                url2 = url2[0]+"courseAvailable";
            }else if(courseState == 1){
                url2 = url2[0]+"courseDuring";
            }else{
                url2 = url2[0]+"courseEnd";
            }
            $.ajax({
                   
                   method: "PUT",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   url: url,
                   data:dataJson,
                   error : function(jqXHR, textStatus, errorThrown) {
                   // log the error to the console
                   console.log("The following error occured: " + textStatus, errorThrown);
                   },
                   success : function(result){
                   location.href = url2;
                   }
                   });
                }
        }
    else{
        
        alert("請於「招生人數」和「課程費用」中輸入數字");
    }
    
}

function backToCourseView(){
    var url = window.location.href;
    var url2 = url.split("courseDetailEdit");
    if(courseState == 0 ){
        url2 = url2[0]+"courseAvailable";
        location.href = url2;
    }else if(courseState == 1){
        url2 = url2[0]+"courseDuring";
        location.href = url2;
    }else{
        url2 = url2[0]+"courseEnd";
        location.href = url2;
    }
}


function getDate(date){
    courseDate.push(date);
    var courseStartDate = displayTime(1,date[0])[0];
    alert("date="+date[0]+"\nstartDate"+courseStartDate);
    var table = document.getElementById("table");
    var row = table.insertRow(table.rows.length);
    var date = row.insertCell(0);
    id.push(count);
    date.innerHTML="<input type=text class='date start' id=courseStartDate"+count+" value="+courseStartDate+" size=12>";
    datePicker();
    count+=1;
}
//function getContent(oneContent){
//    content.push(oneContent);
//}
function getCourseDateContent(){
    var i=0;
    alert("dateLength="+courseDate.length);
    for(i=0;i<content.length;i++){
        var courseStartDate = displayTime(1,courseDate[i*2])[0];
        var courseStartTime = displayTime(1,courseDate[i*2])[1];
        var courseEndTime = displayTime(1,courseDate[i*2+1])[1];
        alert("addCourseDate courseStartDate="+courseStartDate+"courseStratTime="+courseStartTime+"courseEndTime="+courseEndTime);
        
        addCourseDate(courseStartDate,courseStartTime,courseEndTime,content[i]);
    }
}

function addCourseDate(courseStartDate,courseStartTime,courseEndTime,oneContent){
    var table = document.getElementById("table");
    var row = table.insertRow(table.rows.length);
    var date = row.insertCell(0);
    var content = row.insertCell(1);
    id.push(count);
    date.innerHTML="<p id=alternateUiWidgetsExample><input type=text class='date start' id=courseStartDate"+count+" size=12>&nbsp;&nbsp;<input type=text class='time start' id=courseStartTime"+count+" size=8>&nbsp;&nbsp;～&nbsp;&nbsp;<input type=text class='time end' id=courseEndTime"+count+" value="+courseEndTime+" size=8></p>";
    content.innerHTML="<td><input type=text size=50 id=content"+count+" value="+oneContent+" >&nbsp;<button onclick=removeRow(this,"+count+")>刪除</button></td>";
    document.getElementById('courseStartDate'+count).value = courseStartDate;
    document.getElementById('courseStartTime'+count).value = courseStartTime;
    document.getElementById('courseEndTime'+count).value = courseEndTime;
    datePicker();
    count+=1;
}

function addRow(){
    var table = document.getElementById("table");
    var row = table.insertRow(table.rows.length);
    var date = row.insertCell(0);
    var content = row.insertCell(1);
    id.push(count);
    date.innerHTML="<p id=alternateUiWidgetsExample><input type=text class='date start' id=courseStartDate"+count+" size=12>&nbsp;&nbsp;<input type=text class='time start' id=courseStartTime"+count+" size=8>&nbsp;&nbsp;～&nbsp;&nbsp;<input type=text class='time end' id=courseEndTime"+count+"  size=8></p>";
    content.innerHTML="<td><input type=text size=50 id=content"+count+">&nbsp;<button onclick=removeRow(this,"+count+")>刪除</button></td>";
    datePicker();
    count+=1;
}
function removeRow(r,cellid){
    var row = r.parentNode.parentNode;
    row.parentNode.removeChild(row);
    var index = id.indexOf(cellid);
    id.splice(index,1);
}
function show(){
    var url = window.location.href;
    url = url.split("courseDetailEdit/");
    courseId = url[1];
    
    var url2 = url[0]+"api/course/"+courseId;
    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(data,status){
           courseName = data.courseName;
           courseType = data.courseType;
           campus = data.campus;
           courseState = data.courseState;
           numberOfApply = data.numberOfApply;
           document.getElementById('courseName').innerHTML = data.courseName;
           document.getElementById('courseType').innerHTML = courseTypeString(data.courseType);
           document.getElementById('campus').innerHTML = courseCampusString(data.campus);
           document.getElementById('courseTeacher').value = data.courseTeacher;
           document.getElementById('coursePlace').value = data.coursePlace;
           document.getElementById('courseCost').value = data.courseCost;
           document.getElementById('firstApplyStartDate').value = moment(data.firstApplyStartTime).format("YYYY-MM-DD");
           document.getElementById('firstApplyStartTime').value = moment(data.firstApplyStartTime).format("h:mm A");
           
           document.getElementById('firstApplyEndDate').value = moment(data.firstApplyEndTime).format("YYYY-MM-DD");
           document.getElementById('firstApplyEndTime').value = moment(data.firstApplyEndTime).format("h:mm A");
           document.getElementById('firstNumberOfStudents').value = data.firstNumberOfStudents;
           
           document.getElementById('secondApplyStartDate').value = moment(data.secondApplyStartTime).format("YYYY-MM-DD");
           document.getElementById('secondApplyStartTime').value = moment(data.secondApplyStartTime).format("h:mm A");
           
           document.getElementById('secondApplyEndDate').value = moment(data.secondApplyEndTime).format("YYYY-MM-DD");
           document.getElementById('secondApplyEndTime').value = moment(data.secondApplyEndTime).format("h:mm A");
           document.getElementById('secondNumberOfStudents').value = data.secondNumberOfStudents;
           
           document.getElementById('courseIntroduction').value = data.courseIntroduction;
           document.getElementById('other').value = data.other;
           
           var i=0;
           for(i=0;i<data.courseContent.length;i++){
           var startDate = moment(data.courseDate[i*2]).format("YYYY-MM-DD");
           var startTime = moment(data.courseDate[i*2]).format("h:mm A");
           var endTime = moment(data.courseDate[i*2+1]).format("h:mm A");
           if(startDate>moment("1970-01-01").format("YYYY-MM-DD")){
           addCourseDate(startDate,startTime,endTime,data.courseContent[i]);
           }
           
           }
           }
           });
    
}
