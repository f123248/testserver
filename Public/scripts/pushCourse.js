function alertPass(courseName,id,courseType,proposerID,recommendReason,campus){
    var type = parseInt(courseType);

    //fake data
    var campus = 0;
    var voteStartTime = moment().format();
    var voteEndTime = moment().format();
    var recommendTime = moment().format();
    var verifiedTime = moment().format();
    var verificationRejectedReason = "";
    var courseIntroduction = "";
    var numberOfVoters = 0;
    //
    var check = getConfirmation("確定通過"+courseName+"課程?");
    if (check){
        var data = {
            "id": id,
            "courseName": courseName,
            "courseType": type,
            "campus": campus,
            "recommendReason":recommendReason,
            "courseIntroduction":courseIntroduction,
            "proposerID": proposerID,
            "voteStartTime":voteStartTime,
            "voteEndTime":voteEndTime,
            "recommendTime":recommendTime,
            "verifiedTime":verifiedTime,
            "verifyState": 1,
            "verificationRejectedReason": verificationRejectedReason,
            "numberOfVoters":numberOfVoters
        };
        var dataJson = JSON.stringify(data);
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: window.location.href+"/"+id,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
                   location.href = window.location.href;
               }
               });
    }
    else{
        
        alert("通過失敗");
    }
}

function pushReject(courseId){
    var url = window.location.href;
    var url2 = url.split("pushCourse");
    url2 = url2[0]+"pushReject/"+courseId;
    location.href = url2;
}

$(document).ready(function($) {

    $('#table').DataTable({

    });

});
