var courseName=[],courseCode=[],stuName=[],stuID=[],countCourse=0,countStudent=0,courseid=[],stuid=[],addCourseA=[],addStudentA=[];
function getCourse(){
    var url = window.location.href;
    url = url.split("rcAccountEdit");
    var url2 = url[0]+"rcaCoursePivot"+url[1];

    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           data:{},
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result,status){
           var i;
           for(i=0;i<result.courseName.length;i++){
           courseid.push(result.courseid[i]);
           courseName.push(result.courseName[i]);
           courseCode.push(result.courseCode[i]);
           }
           for(i=0;i<result.studentName.length;i++){
           stuid.push(result.stuid[i]);
           stuName.push(result.studentName[i]);
           stuID.push(result.studentID[i]);
           }
           addRow(account,password,courseName,courseCode,stuName,stuID);
           }
           });
     
}

function getData(account,password){
    var url = window.location.href;
    url = url.split("rcAccountEdit");
    var url2 = url[0]+"rcaCoursePivot"+url[1];
    
    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           data:{},
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result,status){
           var i;
           for(i=0;i<result.courseName.length;i++){
           courseName.push(result.courseName[i]);
           courseCode.push(result.courseCode[i]);
           }
           for(i=0;i<result.studentName.length;i++){
           stuName.push(result.studentName[i]);
           stuID.push(result.studentID[i]);
           }
           addRow(account,password,courseName,courseCode,stuName,stuID);
           }
           });
}

function addRow(account,password,courseName,courseCode,stuName,stuID){
    var rowCourse = document.getElementById("course");
    var cellCourseName = rowCourse.insertCell(1);
    cellCourseName.id="cellCourse";
    var rowStu = document.getElementById("student");
    var cellStudentName = rowStu.insertCell(1);
    cellStudentName.id="cellStudent";
    
    if(courseName.length==0){
        cellCourseName.innerHTML="無課程名稱";
    }
    if(stuName.length==0){
        cellStudentName.innerHTML="無學生名稱";
    }
    
    var i=0;
    for(i=0;i<courseName.length;i++){
        cellCourseName.innerHTML+="<p id='"+courseCode[i]+"'>"+courseCode[i]+" "+courseName[i]+" <button onclick=deleteCourse('"+courseid[i]+"','"+courseCode[i]+"','"+courseName[i]+"');>x</button></p>";
    }
    
    for(i=0;i<stuName.length;i++){
        cellStudentName.innerHTML+="<p id='"+stuID[i]+"'>"+stuID[i]+" "+stuName[i]+" <button onclick=deleteStudent('"+stuid[i]+"','"+stuID[i]+"','"+stuName[i]+"');>x</button></p>";
    }
}

function deleteCourse(cid,cCode,cName){
    var check = getConfirmation("確定刪除"+cName+"?");
    if (check){
        var child = document.getElementById(cCode).innerHTML="";
        delete courseCode[courseCode.indexOf(cCode)];
        delete courseName[courseName.indexOf(cName)];
        var url = window.location.href;
        url = url.split("rcAccountEdit");
        var url2 = url[0]+"deleteRCACourse"+url[1]+"/"+cid;
        
        $.ajax({
               method: "DELETE",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:{},
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result,status){
               }
               });
    }
}

function deleteStudent(sid,sID,stuName){
    var check = getConfirmation("確定刪除"+sID+" "+stuName+"?");
    if (check){
        var child = document.getElementById(sID).innerHTML="";
        delete stuID[stuID.indexOf(stuName)];
        delete stuName[stuName.indexOf(stuName)];
        var url = window.location.href;
        url = url.split("rcAccountEdit");
        var url2 = url[0]+"deleteRCAStudent"+url[1]+"/"+sid;
        
        $.ajax({
               method: "DELETE",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:{},
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result,status){
               }
               });
    }
}

function deleteSelect(htmlId,type){
    document.getElementById(htmlId).innerHTML="";
    if(type==0){
        delete addCourseA[addCourseA.indexOf(htmlId)];
    }
    else{
        htmlId = htmlId.split("pId");
        delete addStudentA[addStudentA.indexOf("input"+htmlId[1])];
        console.log(addStudent);
    }
}

function addCourse(){
    var url = window.location.href;
    url = url.split("rcAccountEdit");
    var url2 = url[0]+"api/course";
    var cellCourseName = document.getElementById("cellCourse");

    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           data:{},
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result,status){
           var i=0;
           var output="<form id=formId"+countCourse+"><select id=courseId"+countCourse+"><option value=''>不設定</option>";
           for(i=0;i<result.length;i++){
           output+="<option value="+result[i].id+">"+result[i].courseName+"</option>";
           }
           output+="<form></select> <button onclick=deleteSelect('formId"+countCourse+"',0);>x</button></form>";
           cellCourseName.innerHTML+=output;
           addCourseA.push("formId"+countCourse);
           countCourse+=1;
           }
           });
}

function addStudent(){
    var cellStudentName = document.getElementById("cellStudent");
    cellStudentName.innerHTML+="<p id=pId"+countStudent+"><input id=inputId"+countStudent+"> <button onclick=deleteSelect('pId"+countStudent+"',1);>x</button>";
    addStudentA.push("inputId"+countStudent);
    countStudent+=1;
}

function update(){
    var account = document.getElementById("account").value;
    var password = document.getElementById("password").value;
    var data = {"account":account,"password":password};
    var dataJson = JSON.stringify(data);
    var url = window.location.href;
    url = url.split("rcAccountEdit");
    var url2 =url[0]+"rcAccount";
    var url3 = url2+url[1];
    
    $.ajax({
           method: "PUT",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url3,
           data:dataJson,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result){
           }
           });
    
    var i = 0;
    for(i=0;i<addCourseA.length;i++){
        var course = addCourseA[i].split("formId");
        course = "courseId"+course[1];
        course = document.getElementById(course).value;
        if(course==""){
            continue;
        }
        url3 = url2+url[1]+"/RCACourse/"+course;
        console.log("url="+url3);
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "html",
               url: url3,
               data:{},
               error : function(jqXHR, status, errorThrown) {
               alert(errorThrown);
               // log the error to the console
               console.log("The following error occured: " + status, errorThrown);
               },
               success : function(result){
               }
               });
    }
    for(i=0;i<addStudentA.length;i++){
        var stuID = document.getElementById(addStudentA[i]).value;
        url3 = url2+url[1]+"/RCAStudent/"+stuID;
        console.log("url="+url3);
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "html",
               url: url3,
               data:{},
               error : function(jqXHR, status, errorThrown) {
               alert(errorThrown+" "+stuID);
               // log the error to the console
               console.log("The following error occured: " + status, errorThrown);
               },
               success : function(result){
               }
               });
        
    }
    rcaCreateGo();
}

function rcaCreateGo(){
    var url = window.location.href;
    url = url.split("rcAccountEdit");
    url = url[0]+"rcAccount";
    location.href = url;
}
