function deleteAccount(accountID,account){
    var url = window.location.href;
    url = url.split("rcAccount");
    url = url[0]+"deleteAccount/"+accountID;
    var check = getConfirmation("確定刪除"+account+"?");
    if (check){
        $.ajax({
               method: "DELETE",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url,
               data:{},
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = window.location.href;
               }
               });
    }
}

function editAccount(accountID){
    var url = window.location.href;
    url = url.split("rcAccount");
    url = url[0]+"rcAccountEdit/"+accountID;
    location.href = url;
}
function createAccount(){
    var url = window.location.href;
    url = url.split("rcAccount");
    url = url[0]+"rcAccountCreate";
    location.href = url;
}
function getData(accountID,account,password){
    var url = window.location.href;
    url = url.split("rcAccount");
    var url2 = url[0]+"rcaCoursePivot/"+accountID;
    
    var courseName=[],courseCode=[],stuName=[],stuID=[];
    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           data:{},
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result,status){
           var i;
           for(i=0;i<result.courseName.length;i++){
           courseName.push(result.courseName[i]);
           courseCode.push(result.courseCode[i]);
           }
           for(i=0;i<result.studentName.length;i++){
           stuName.push(result.studentName[i]);
           stuID.push(result.studentID[i]);
           }
           addRow(accountID,account,password,courseName,courseCode,stuName,stuID);
           }
           });
}

function addRow(accountID,account,password,courseName,courseCode,stuName,stuID){
    var table = document.getElementById("table");
    var row = table.insertRow(table.length);
    var button = row.insertCell(0);
    var cellAccount = row.insertCell(1);
    var cellPassword = row.insertCell(2);
    var cellCourseCode = row.insertCell(3);
    var cellCourseName = row.insertCell(4);
    var cellStudentName = row.insertCell(5);
    var cellStudentID = row.insertCell(6);
    
    button.innerHTML = "<button type=button onclick=deleteAccount('"+accountID+"','"+account+"');>刪除帳號</button><button type=button onclick=editAccount('"+accountID+"');>編輯</button>";
    cellAccount.innerHTML = account;
    cellPassword.innerHTML = password;
    
    if(courseName.length==0){
        cellCourseCode.innerHTML="無課程代碼";
        cellCourseName.innerHTML="無課程名稱";
    }
    if(stuName.length==0){
        cellStudentName.innerHTML="無學生名稱";
        cellStudentID.innerHTML="無學生ID";
    }
    
    var i=0;
    for(i=0;i<courseName.length;i++){
        if(i==0){
            cellCourseCode.innerHTML+=courseCode[i];
            cellCourseName.innerHTML+=courseName[i];
        }
        else{
            cellCourseCode.innerHTML+="<br>"+courseCode[i];
            cellCourseName.innerHTML+="<br>"+courseName[i];
        }
    }
    
    for(i=0;i<stuName.length;i++){
        if(i==0){
            cellStudentName.innerHTML+=stuName[i];
            cellStudentID.innerHTML+=stuID[i];
        }
        else{
            cellStudentName.innerHTML += "<br>"+stuName[i];
            cellStudentID.innerHTML += "<br>"+stuID[i];
        }
    }
}

