function applyTypeString(applyResult){
    if(applyResult==0){
        return "正取";
    }
    else{
        return "備取";
    }
}

function changeResult(applyResult,applyTime,studentID){
    var url = window.location.href;
    url = url.split("courseApplyStudents");
    var courseID = url[1];
    var url2 = url[0]+"api/student/applyCourses";
    var data = {
        "courseID": courseID,
        "studentID": studentID,
        "applyTime": applyTime,
        "applyResult": !applyResult,
    };
    var dataJson = JSON.stringify(data);
    $.ajax({
           method: "PUT",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           data:dataJson,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result){
           location.href = window.location.href;
           }
    });
}

$(document).ready(function($) {

    $('#table').DataTable({

    });

});
