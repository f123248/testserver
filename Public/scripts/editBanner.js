
function setIndex(imgNumber){
    var i=0;
    var url  = window.location.href;
    url = url.split("editBanner");
    var url2 = url[0]+"updateBanner/";
    for(i=0;i<imgNumber;i++){
        var newIndex = document.getElementById("index"+i).value;
        var id = document.getElementById("id"+i).value;
        var title = document.getElementById("title"+i).value;
        var content = document.getElementById("content"+i).value;
        var webPage = document.getElementById("webPage"+i).value;
        var url3 = url2+id;
        var data = {
            "title": title,
            "content": content,
            "sort": parseInt(newIndex),
            "webPage": webPage,
            "pictureName": ""
        };
        var dataJson = JSON.stringify(data);
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               data: dataJson,
               url: url3,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success: function (status) {
               location.href = window.location.href;
               }
               });
    }
    
}

function deleteImg(imgid){
    var url  = window.location.href;
    url = url.split("editBanner");
    var url2 = url[0]+"deleteBanner/"+imgid;
    var check = getConfirmation("確定刪除此Banner?");
    if (check){
        $.ajax({
               url: url2,
               type: 'DELETE',
               data: "",
               contentType: "application/json; charset=utf-8",
               dataType: "",
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success: function (status) {
                    location.href = window.location.href;
               }
        });
    }
}

function previewFile(){
    var preview = document.getElementById("upload"); //selects the query named img
    var file    = document.querySelector('input[type=file]').files[0]; //sames as here
    var reader  = new FileReader();
    
    reader.onloadend = function () {
        preview.src = reader.result;
        preview.style.display = "";
    }
    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    } else {
        preview.src = "";
    }
}
previewFile();  //calls the function named previewFile()



