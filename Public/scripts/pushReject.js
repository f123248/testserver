function rejectReason(courseName,id,courseType,proposerID,courseIntroduction,recommendReason){
    
    var reason = document.getElementById("reason").value;
    
    //fake data
    var campus = 0;
    var voteStartTime = moment().format();
    var voteEndTime = moment().format();
    var recommendTime = moment().format();
    var verifiedTime = moment().format();
    var numberOfVoters = 0;
    
    if(reason!=""){
        reason = "審核未通過，"+reason;
        var check = getConfirmation("確定不通過"+courseName+"課程?");
        if (check){
            var type = parseInt(courseType);
            var data = {
                "id": id,
                "courseName": courseName,
                "courseType": type,
                "campus": campus,
                "recommendReason":recommendReason,
                "courseIntroduction":courseIntroduction,
                "proposerID": proposerID,
                "voteStartTime":voteStartTime,
                "voteEndTime":voteEndTime,
                "recommendTime":recommendTime,
                "verifiedTime":verifiedTime,
                "verifyState": 2,
                "verificationRejectedReason": reason,
                "numberOfVoters":numberOfVoters

            };
            var dataJson = JSON.stringify(data);
            
            var url = window.location.href;
            url = url.split("pushReject");
            url = url[0]+"pushCourse";
            
            $.ajax({
                   method: "PUT",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   url: window.location.href,
                   data:dataJson,
                   error : function(jqXHR, textStatus, errorThrown) {
                   // log the error to the console
                   console.log("The following error occured: " + textStatus, errorThrown);
                   },
                   success : function(result){
                   location.href = url;
                   }
            });
        }
    }
    else{
        alert("請輸入不通過原因");
    }
}

function backToPush(){
    var url = window.location.href;
    var url2 = url.split("pushReject");
    url2 = url2[0]+"pushCourse";
    location.href = url2;
}
