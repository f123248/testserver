function change(){
    var courseid = document.getElementById("courseId").value;
    var data =  document.getElementById(courseid).innerHTML;
    document.getElementById('title').value = data+"通知";
}

function noticeCreate(){
    var courseid = document.getElementById("courseId").value;
    var title = document.getElementById("title").value;
    var content = document.getElementById("content").value;
    var type = 1;//課程群組
    var courseType = -1;
    var date = moment().format();
    var check = getConfirmation("確定新增通知？");
    if(check){
        var data = {
            "title":title,
            "content":content,
            "type":type,
            "courseType":courseType,
            "pushTime":date
        };
        
        var url = window.location.href;
        url = url.split("noticeCourseCreate");
        var url2 = url[0]+"api/notification/course/"+courseid;
        var dataJson = JSON.stringify(data);
        
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url[0]+"noticeCourse";
               }
               });
    }
}

function backToPass(){
    var url = window.location.href;
    url = url.split("noticeCourseCreate");
    url = url[0]+"noticeCourse";
    location.href = url;
}
