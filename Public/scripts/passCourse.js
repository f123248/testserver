function passEdit(courseId){
    var url = window.location.href;
    var url2 = url.split("passCourse");
    url2 = url2[0]+"passCourseEdit/"+courseId;
    location.href = url2;
}
function passToVote(courseId,courseName,courseType,proposerID,recommendReason,voteStartTime,voteEndTime,campus){
    var timeNow = Date.now();
    
    //fake data
    var campus = 0;
    var courseIntroduction = "";
    var recommendTime = moment().format();
    var verifiedTime = moment().format();
    var verificationRejectedReason = "";
    var numberOfVoters = 0;
    //
    
    if(voteStartTime<=0.0||voteEndTime<=0.0){
        alert("請編輯課程資訊");
        passEdit(courseId);
    }
    else{
        var check = getConfirmation("確定進行投票?");
        if (check){
            var type = parseInt(courseType);
            var startTime = moment.unix(voteStartTime).format();
            var endTime = moment.unix(voteEndTime).format();
            
            var data = {
                "id": courseId,
                "courseName": courseName,
                "courseType": type,
                "campus": campus,
                "recommendReason":recommendReason,
                "courseIntroduction":courseIntroduction,
                "proposerID": proposerID,
                "voteStartTime":startTime,
                "voteEndTime":endTime,
                "recommendTime":recommendTime,
                "verifiedTime":verifiedTime,
                "verifyState": 3,
                "verificationRejectedReason": verificationRejectedReason,
                "numberOfVoters":numberOfVoters
            };
            var dataJson = JSON.stringify(data);
            $.ajax({
                   method: "PUT",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   url: window.location.href+"/"+courseId,
                   data:dataJson,
                   error : function(jqXHR, textStatus, errorThrown) {
                   // log the error to the console
                   console.log("The following error occured: " + textStatus, errorThrown);
                   },
                   success : function(result){
                   location.href = window.location.href;
                   }
                   });
        }
    }
}

$(document).ready(function($) {
                  
      $('#table').DataTable({
      
      });
  
  });
