

function checkQuestion(){
//    document.getElementById("table").innerHTML = "";
    var courseId = document.getElementById("courseId").value;
    var url = window.location.href;
    url = url.split("viewSatisfaction");
    var url2 = url[0]+"api/courseSatisfactionAns/"+courseId;
    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(data,status){
               if(!(data[0].hasOwnProperty('quest'))){
                alert("無資料");
               }
               else{
                addQuestion(data);
                for(var i=0;i<data.length;i++){
                    addAnswer(data[i]);
                }
               }
           }
    });
}

function addQuestion(data){
    var table = document.getElementById("table");
    table.innerHTML="<thead><tr></tr></thead><tbody></tbody>";
    var tr = document.getElementById('table').tHead.children[0];
    
    var th = document.createElement('th');
    th.innerHTML = "學生ID";
    tr.appendChild(th);
    
    var cellCount = 1;
    for(var i=0;i<data[0].quest.length;i++){
        th = document.createElement('th');
        th.innerHTML = "問題"+cellCount+": "+data[0].quest[i];
        if(data[0].kind[i]==0){
            th.innerHTML+="(量表題)";
        }
        else if(data[0].kind[i]==1){
            th.innerHTML+="(問答題)";
        }
        else if(data[0].kind[i]==2){
            th.innerHTML+="(單選題)";
        }
        else if(data[0].kind[i]==3){
            th.innerHTML+="(多選題)";
        }
        tr.appendChild(th);
        cellCount++;

    }
}

function addAnswer(data){
    var table = document.getElementById("table");
    var row = table.insertRow(table.rows.length);
    var stuID = row.insertCell(0);
    stuID.innerHTML = data.studentID;
    for(var i=0;i<data.answer.length;i++){
        var cell2 = row.insertCell(i+1);
        cell2.innerHTML = data.answer[i];
    }
    
}
