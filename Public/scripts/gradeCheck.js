function gradePass(studentID,studentName,gradeDescription,gradeID,studentDepartment,studentRegisterDate,studentAddress,studentPhoneNumber,studentIdCard,idCard,idCardBackSide,bankbook,gradePicture,applyYearSemester,applyDate){
    
    // fake data
    var verificationRejectedReason = "";
    var verifiedDate = moment().format();
    var studentRegisterDate1 = document.getElementById('+studentRegisterDate').innerHTML;
    var studentRegisterDate2 = moment(studentRegisterDate1,"YYYY-MM-DD hh:mm A").format();
    var applyDate1 = document.getElementById('+applyDate').innerHTML;
    var applyDate2 = moment(applyDate1,"YYYY-MM-DD hh:mm A").format();
//    var verifiedDate1 = document.getElementById('+verifiedDate').innerHTML;
//    var verifiedDate2 = moment(verifiedDate1,"YYYY-MM-DD hh:mm A").format();
    
//        console.log("licenseID: " + verifiedDate);
    //    console.log("licenseID: " + studentRegisterDate2);
    
    var check = getConfirmation("確定通過"+studentName+"的成績進步申請?")
    if (check){
        var data = {
            "studentID": studentID,
            "studentName": studentName,
            "gradeDescription":gradeDescription,
            "gradeID":gradeID,
            "studentDepartment":studentDepartment,
            "studentRegisterDate": studentRegisterDate2,
            "studentAddress":studentAddress,
            "studentPhoneNumber":studentPhoneNumber,
            "studentIdCard":studentIdCard,
            "idCard":idCard,
            "idCardBackSide":idCardBackSide,
            "bankbook":bankbook,
            "gradePicture":gradePicture,
            "verificationRejectedReason":verificationRejectedReason,
            "applyYearSemester":applyYearSemester,
            "applyDate":applyDate2,
            "verifiedDate":verifiedDate,
            "gradeState": 1
        };
        var dataJson = JSON.stringify(data);
        var url = window.location.href;
        url = url.split("gradeCheck");
        var url2 = url[0]+"pushGrade";
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url[0]+"gradeCheckEdit/"+gradeID,
               //               url: window.location.href+"/"+licenseID,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url2;
               }
               });
    }
}

function gradeDecline(studentID,studentName,gradeDescription,gradeID,studentDepartment,studentRegisterDate,studentAddress,studentPhoneNumber,studentIdCard,idCard,idCardBackSide,bankbook,gradePicture,applyYearSemester,applyDate){
    
    // fake data
    //    var verificationRejectedReason = "";
    var studentRegisterDate1 = document.getElementById('+studentRegisterDate').innerHTML;
    var studentRegisterDate2 = moment(studentRegisterDate1,"YYYY-MM-DD hh:mm A").format();
    var applyDate1 = document.getElementById('+applyDate').innerHTML;
    var applyDate2 = moment(applyDate1,"YYYY-MM-DD hh:mm A").format();
    var verifiedDate = moment().format();
    //    console.log("licenseID: " + studentRegisterDate1);
    //    console.log("licenseID: " + studentRegisterDate2);
    
    var check = prompt("請輸入審核不通過原因", "缺少資料");
    if (check != null){
        var data = {
            "studentID": studentID,
            "studentName": studentName,
            "gradeDescription":gradeDescription,
            "gradeID":gradeID,
            "studentDepartment":studentDepartment,
            "studentRegisterDate": studentRegisterDate2,
            "studentAddress":studentAddress,
            "studentPhoneNumber":studentPhoneNumber,
            "studentIdCard":studentIdCard,
            "idCard":idCard,
            "idCardBackSide":idCardBackSide,
            "bankbook":bankbook,
            "gradePicture":gradePicture,
            "verificationRejectedReason":check,
            "applyYearSemester":applyYearSemester,
            "applyDate":applyDate2,
            "verifiedDate":verifiedDate,
            "gradeState": 2
        };
        var dataJson = JSON.stringify(data);
        var url = window.location.href;
        url = url.split("gradeCheck");
        var url2 = url[0]+"pushGrade";
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url[0]+"gradeCheckEditDecline/"+gradeID,
               //               url: window.location.href+"/"+licenseID,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url2;
               }
               });
    }
}

