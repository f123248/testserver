function courseTypeString(int){
    
    var txt = "";
    
    if (int == 0){
        txt = "語言"
    }else if (int == 1){
        txt = "證照"
    }else if (int == 2){
        txt = "其它"
    }else if (int == 3){
        txt = "教發中心"
    }
    
    return txt
}

function studentTypeString(int){
    
    var txt = "";
    
    if (int == 0){
        txt = "一般生"
    }else if (int == 1){
        txt = "陽光種子學生"
    }
    
    return txt
}

function courseCampusString(int){
    
    var txt = "";
    
    if (int == 0){
        txt = "博愛校區"
    }else if (int == 1){
        txt = "天母校區"
    }
    
    return txt
}

function setBlackList(studentId,studentName,blackDate,studentBlackList){

    if(studentBlackList == 0){
        var blackLastDate = prompt("確定將"+studentName+"設定為黑名單嗎？"+"請輸入日期！ 格式：YYYY-MM-DD", "");
    }else{
        blackDate = moment.unix(blackDate).format("YYYY-MM-DD");
        var blackLastDate = prompt("確定變更"+studentName+"黑名單解除日期嗎？"+"請輸入日期！ 格式：YYYY-MM-DD", blackDate);
    }
    if(blackLastDate == null){
  
    }else{
        blackLastDate = moment(blackLastDate).format();
        var dateNow = moment().format();
    if (blackLastDate == "Invalid date" || blackLastDate < dateNow) {
        
        alert("請輸入正確日期");
        
    }else{
        var url = window.location.href;
        var url2 = url.split("/");
        url2 = url2[0]+"/"+url2[1]+"/"+url2[2]+"/"+"blackList"+"/"+studentId;
        //fake data
        var registerDate = moment().format();
        
        var data = {
            "id": studentId,
            "studentID": "",
            "password": "",
            "studentName": studentName,
            "studentType": 0,
            "studentDepartment": "",
            "studentRegisterDate": registerDate,
            "studentAddress":"",
            "studentPhoneNumber":"",
            "studentQRcode": "",
            "studentIdCard":"",
            "idCard":"",
            "idCardBackSide":"",
            "bankbook": "",
            "portrait": "",
            "studentBlackList": 1,
            "blackLastDate":blackLastDate,
            "token":"",
            "pushToken":""

        };
        var dataJson = JSON.stringify(data);
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url;
               }
               });
    }
    }
    
}

function cancelBlackList(studentId,studentName){
    
    var check = getConfirmation("確定將"+studentName+"取消黑名單嗎？");
    if (check) {
        var blackLastDate = moment().format();
        var url = window.location.href;
        var url2 = url.split("/");
         url2 = url2[0]+"/"+url2[1]+"/"+url2[2]+"/"+"blackList"+"/"+studentId;
        //fake data
        var registerDate = moment().format();
        var data = {
            "id": studentId,
            "studentID": "",
            "password": "",
            "studentName": studentName,
            "studentType": 0,
            "studentDepartment": "",
            "studentRegisterDate": registerDate,
            "studentAddress":"",
            "studentPhoneNumber":"",
            "studentQRcode": "",
            "studentIdCard":"",
            "idCard":"",
            "idCardBackSide":"",
            "bankbook": "",
            "portrait": "",
            "studentBlackList": 0,
            "blackLastDate":blackLastDate,
            "token":"",
            "pushToken":""
        };
        var dataJson = JSON.stringify(data);
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url;
               }
               });
    }
}

function autoCancelBlackList(studentId){
    
    var blackLastDate = moment().format();
    var url = window.location.href;
    var url2 = url.split("/");
     url2 = url2[0]+"/"+url2[1]+"/"+url2[2]+"/"+"blackList"+"/"+studentId;
    //fake data
    var registerDate = moment().format();
        var data = {
            "id": studentId,
            "studentID": "",
            "password": "",
            "studentName": "",
            "studentType": 0,
            "studentDepartment": "",
            "studentRegisterDate": registerDate,
            "studentAddress":"",
            "studentPhoneNumber":"",
            "studentQRcode": "",
            "studentIdCard":"",
            "idCard":"",
            "idCardBackSide":"",
            "bankbook": "",
            "portrait": "",
            "studentBlackList": 0,
            "blackLastDate":blackLastDate,
            "token":"",
            "pushToken":""
        };
        var dataJson = JSON.stringify(data);
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url2,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
                location.href = url
               }
               });
    
}

function displayTime(identity,Time){
    if(Time == "" || Time <= 0.0){
        if(identity == 0){
            var timeData = ""
        }
        if(identity == 1){
            var timeData = ["",""]
        }
    }else if(identity == 0){
        var timeData = moment.unix(Time).format("YYYY-MM-DD <br> h:mm A");
        
    }else if(identity == 1){
        var timeData = moment.unix(Time).format("YYYY-MM-DD|h:mm A");
        timeData = timeData.split("|");
    }else if(identity == 2){
        var timeData = moment.unix(Time).format("YYYY-MM-DD");
        timeData = timeData.split("|");
    }else if(identity == 3){
        var timeData = moment.unix(Time).format("YYYY-MM-DD h:mm A");
    }
    
    return timeData
}

function test(){
    var url = window.location.href;
    url = url.split("votingResult");
    url = url[0]+"votingResultEdit";
    location.href = url;
}

function getConfirmation(confirmString) {
    var retVal = confirm(confirmString);
    if(retVal == true) {
        return true;
    } else {
        return false;
    }
}

function datePicker(){
    // initialize input widgets
    // ptTimeSelect doesn't trigger change event by default
    $('#alternateUiWidgetsExample .time').ptTimeSelect({
       'onClose': function($self) {
       $self.trigger('change');
        }
       });
    
    $('#alternateUiWidgetsExample .date').pikaday();
        var TIMEFORMAT = 'h:mm A';
        var alternateUiWidgetsExampleEl = document.getElementById('alternateUiWidgetsExample');
        var alternateWidgetsDatepair = new Datepair(alternateUiWidgetsExampleEl, {
            parseTime: function(input){
            // use moment.js to parse time
            var m = moment(input.value, TIMEFORMAT);
            return m.toDate();
            },
            updateTime: function(input, dateObj){
            var m = moment(dateObj);
            input.value = m.format(TIMEFORMAT);
            },
            parseDate: function(input){
            var picker = $(input).data('pikaday');
            return picker.getDate();
            },
            updateDate: function(input, dateObj){
            var picker = $(input).data('pikaday');
            return picker.setDate(dateObj);
            }
            });
}

function datePicker2(){
    // initialize input widgets
    // ptTimeSelect doesn't trigger change event by default
    $('#alternateUiWidgetsExample2 .time').ptTimeSelect({
        'onClose': function($self) {
        $self.trigger('change');
        }
        });
    
    $('#alternateUiWidgetsExample2 .date').pikaday();
    var TIMEFORMAT = 'h:mm A';
    var alternateUiWidgetsExampleEl = document.getElementById('alternateUiWidgetsExample2');
    var alternateWidgetsDatepair = new Datepair(alternateUiWidgetsExampleEl, {
        parseTime: function(input){
        // use moment.js to parse time
        var m = moment(input.value, TIMEFORMAT);
        return m.toDate();
        },
        updateTime: function(input, dateObj){
        var m = moment(dateObj);
        input.value = m.format(TIMEFORMAT);
        },
        parseDate: function(input){
        var picker = $(input).data('pikaday');
        return picker.getDate();
        },
        updateDate: function(input, dateObj){
        var picker = $(input).data('pikaday');
        return picker.setDate(dateObj);
        }
        });
}


function checkLogin(){
    
    var url = window.location.href;
    url = url.split("/");
    url = url[0]+"/"+url[1]+"/"+url[2]+"/"+"blackList";
    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(data,status){
           for(var i=0;i<data.length;i++){
                autoCancelBlackList(data[i].id);
          
           }
           }
           });
    
    
    if (typeof(Storage) !== "undefined") {
        var login = sessionStorage.getItem("login");
        if (login != "true") {
            alert("請先登入");
            var url = window.location.href;
            url = url.split("/");
            url = url[0]+"login";
            
            location.href = url;
        }
        setRightNav();
    }
    
}

function setRightNav(){
    var login = sessionStorage.getItem("login");
    if (login == "true") {
        document.getElementById("login").innerHTML="<span class='glyphicon glyphicon-log-out active'></span>  Logout";
        document.getElementById("login").href="/index";
        document.getElementById("login").onclick=function(){setLogout()};

    }
    else{
        document.getElementById("login").innerHTML+=" Login";
        document.getElementById("login").href="/login";
    }
}

function setLogout(){
    if (typeof(Storage) !== "undefined") {
        sessionStorage.removeItem("login");
        alert("登出成功");
        var url = window.location.href;
        url = url.split("logout");
        url = url[0]+"index";
        location.href = url;
    }
}

function licenseTypeString(int){
    
    var txt = "";
    
    if (int == 0){
        txt = "國家考試"
    }else if (int == 1){
        txt = "政府機關"
    }else if (int == 2){
        txt = "國際認證"
    }else if (int == 3){
        txt = "外語認證"
    }else if (int == 4){
        txt = "其他"
    }
    return txt
}

function noticeTypeString(type){
    var txt = "";
    if (type==0){
        txt = "一般公告";
    }
    else if (type==1){
        txt = "課程群組";
    }
    else if (type==2){
        txt = "個人通知";
    }
    return txt;
}

function blackListTypeString(int){
    
    var txt = "";
    
    if (int == 0){
        txt = "否"
    }else if (int == 1){
        txt = "在黑名單"
    }
    return txt    
}
