
function create(){
    var account = document.getElementById("account").value;
    var password = document.getElementById("password").value;
    var courseID = document.getElementById("courseId").value;
    var studentID = document.getElementById("studentID").value;
    
    if(account==""||password==""){
        alert("請輸入帳號密碼");
    }
    else{
        var data = {
            "account":account,
            "password":password
        };
        
        var dataJson = JSON.stringify(data);
        var url = window.location.href;
        url = url.split("rcAccountCreate");
        url = url[0]+"rcAccount";
        
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               if(courseID!=""){
                    createCoursePivot(result.id,courseID);
               }
               if(studentID!=""){
               createStudentPivot(result.id,studentID);
               }
               location.href = url;
               }
        });
    }
}

function createCoursePivot(accountID,courseID){
    alert("accountID = "+accountID);
    var url = window.location.href;
    url = url.split("rcAccountCreate");
    url = url[0]+"rcAccount/"+accountID+"/RCACourse/"+courseID;
    $.ajax({
           method: "POST",
           contentType: "application/json; charset=utf-8",
           dataType: "html",
           url: url,
           data:"",
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result){
           }
    });
}
function createStudentPivot(accountID,studentID){
    
    var url = window.location.href;
    url = url.split("rcAccountCreate");
    url = url[0]+"rcAccount/"+accountID+"/RCAStudent/"+studentID;
    $.ajax({
           method: "POST",
           contentType: "application/json; charset=utf-8",
           dataType: "html",
           url: url,
           data:"",
           error : function(jqXHR, textStatus, errorThrown) {
           alert(errorThrown+" "+accountID);
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result){
           }
    });
}


function rcaCreateGo(){
    var url = window.location.href;
    url = url.split("rcAccountCreate");
    url = url[0]+"rcAccount";
    location.href = url;
}
