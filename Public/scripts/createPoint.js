
function createPoint(){
    var studentID = document.getElementById("studentID").value;
    var point = parseInt(document.getElementById("point").value);
    var reason = document.getElementById("reason").value;
    
    if(studentID==""||point==""||reason==""){
        alert("請輸入完整資料");
    }
    else{
        var data = {
            "studentID":studentID,
            "point":point,
            "reason":reason
        };
        
        var dataJson = JSON.stringify(data);
        var url = window.location.href;
        var url2 = url.split("createPoint");
        url2 = url2[0]+"point";
        
        $.ajax({
               method: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: url,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
               // log the error to the console
               console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
               location.href = url2;
               }
               });
    }
}

function backToPass(){
    var url = window.location.href;
    var url2 = url.split("createPoint");
    url2 = url2[0]+"point";
    location.href = url2;
}
