function courseStart(id, courseName, courseType, campus, courseTeacher, coursePlace, courseCost, targetStudent, numberOfStudents, numberOfApply, applyTime, applyMethod, enrollPost, other){
    courseType = parseInt(courseType);
    campus = parseInt(campus);
    numberOfStudents = parseInt(numberOfStudents);
    targetStudent = parseInt(targetStudent);
    courseCost = parseInt(courseCost);
    applyMethod = parseInt(applyMethod);
    enrollPost = parseInt(enrollPost);
    numberOfApply = parseInt(numberOfApply);
    
    //fake data
    var courseDate = "";
    var courseTime = "";
    var courseIntroduction = "";
    var courseDate=[];
    var courseContent=[];
    var firstApplyStartTimeData = moment().format();
    var firstApplyEndTimeData = moment().format();
    var firstNumberOfStudents = 0
    var secondApplyStartTimeData = moment().format();
    var secondApplyEndTimeData = moment().format();
    var secondNumberOfStudents = 0;

    var check = getConfirmation("確定開始"+courseName+"課程?");
    if (check){
    
    var data = {
        "id": id,
        "courseName": courseName,
        "courseType": courseType,
        "campus": campus,
        "courseDate": courseDate,
        "courseTeacher":courseTeacher,
        "coursePlace": coursePlace,
        "courseCost": courseCost,
        "targetStudent": targetStudent,
        "courseState": 1,
        "numberOfApply": numberOfApply,
        "firstApplyStartTime": firstApplyStartTimeData,
        "firstApplyEndTime": firstApplyEndTimeData,
        "firstNumberOfStudents": firstNumberOfStudents,
        "secondApplyStartTime": secondApplyStartTimeData,
        "secondApplyEndTime": secondApplyEndTimeData,
        "secondNumberOfStudents": secondNumberOfStudents,
        "applyMethod": applyMethod,
        "enrollPost": enrollPost,
        "courseIntroduction": courseIntroduction,
        "other": other,
        "courseContent":courseContent
    };
    var dataJson = JSON.stringify(data);
    var url = window.location.href;
    url = url.split("courseAvailable");
    $.ajax({
           method: "PUT",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url[0]+"/courseStateChange/"+id,
           data:dataJson,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(result){
           addInCourse(id);
           location.href = window.location.href;
           }
           });
    }
}

function courseEditGo(courseId){
    var url = window.location.href;
    var url2 = url.split("courseAvailable");
    url2 = url2[0]+"courseDetailEdit/"+courseId;
    location.href = url2;
}

function courseApplyStudents(courseId,courseName){
    sessionStorage.setItem("courseName", courseName);
    var url = window.location.href;
    var url2 = url.split("courseAvailable");
    url2 = url2[0]+"courseApplyStudents/"+courseId;
    location.href = url2;
}

function addInCourse(courseId){
    var url = window.location.href;
    url = url.split("courseAvailable");
    var url2 = url[0]+"getApplyStudents/"+courseId;
    $.ajax({
           method: "GET",
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           url: url2,
           error : function(jqXHR, textStatus, errorThrown) {
           // log the error to the console
           console.log("The following error occured: " + textStatus, errorThrown);
           },
           success : function(data,status){
               for(var i=0;i<data.length;i++){
                   var studentID = data[i].studentID;
                   var url2 = url[0]+"api/student/"+studentID+"/inCourses/"+courseId;
                   $.ajax({
                          method: "POST",
                          contentType: "application/json; charset=utf-8",
                          url: url2,
                          error : function(jqXHR, textStatus, errorThrown) {
                          // log the error to the console
                          console.log("The following error occured: " + textStatus, errorThrown);
                          },
                          success : function(result){
                          }
                    });
                }
            }
    });
}

$(document).ready(function($) {
                  
    $('#table').DataTable({

    });

});
