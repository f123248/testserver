function transformNormal(studentId,studentName){
    
        var check = getConfirmation("確定將"+studentName+"轉成一般生身分嗎？");
        if (check) {
            
            var url = window.location.href;
            var url2 = url+"/"+studentId;
            //        url2 = url2[0]+"/"+url2[1]+"/"+url2[2]+"/"+"blackList"+"/"+studentId;
            //fake data
            var registerDate = moment().format();
            var blackLastDate = moment().format();
            var data = {
                "id": studentId,
                "studentID": "",
                "password": "",
                "studentName": studentName,
                "studentType": 0,
                "studentDepartment": "",
                "studentRegisterDate": registerDate,
                "studentAddress":"",
                "studentPhoneNumber":"",
                "studentQRcode": "",
                "studentIdCard":"",
                "idCard":"",
                "idCardBackSide":"",
                "bankbook": "",
                "portrait": "",
                "studentBlackList": 0,
                "blackLastDate":blackLastDate,
                "token":"",
                "pushToken":""
            };
            var dataJson = JSON.stringify(data);
            $.ajax({
                   method: "PUT",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   url: url2,
                   data:dataJson,
                   error : function(jqXHR, textStatus, errorThrown) {
                   // log the error to the console
                   console.log("The following error occured: " + textStatus, errorThrown);
                   },
                   success : function(result){
                   location.href = url;
                   }
                   });
        }
    
}

