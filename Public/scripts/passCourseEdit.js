function courseEdit(id,courseName,courseType,proposerID,recommendReason,campus){
    
    courseName = document.getElementById('courseName').value;
    courseIntroduction = document.getElementById('courseIntroduction').value;
    var voteStartDate = document.getElementById('voteStartDate').value;
    var voteStartTime = document.getElementById('voteStartTime').value;
    var voteEndDate = document.getElementById('voteEndDate').value;
    var voteEndTime = document.getElementById('voteEndTime').value;
    voteStartDate = voteStartDate.split("-");
    voteStartDate = voteStartDate[1]+"-"+voteStartDate[2]+"-"+voteStartDate[0];
    voteEndDate = voteEndDate.split("-");
    voteEndDate = voteEndDate[1]+"-"+voteEndDate[2]+"-"+voteEndDate[0];
    var startTimeData = moment(voteStartDate+" "+voteStartTime,"MM-DD-YYYY h:mm A").format();
    var endTimeData = moment(voteEndDate+" "+voteEndTime, "MM-DD-YYYY h:mm A").format();

    //fake data
    var campus = 0;
    var recommendTime = moment().format();
    var verifiedTime = moment().format();
    var verificationRejectedReason = "";
    var numberOfVoters = 0;
    //
    
    if (startTimeData == "Invalid date" || endTimeData == "Invalid date"){
         alert("請輸入正確的日期及時間");
    }else{
        alert("編輯課程：" + courseName);
        var type = parseInt(courseType);
        var data = {
            "id": id,
            "courseName": courseName,
            "courseType": type,
            "campus": campus,
            "recommendReason":recommendReason,
            "courseIntroduction":courseIntroduction,
            "proposerID": proposerID,
            "voteStartTime":startTimeData,
            "voteEndTime":endTimeData,
            "recommendTime":recommendTime,
            "verifiedTime":verifiedTime,
            "verifyState": 1,
            "verificationRejectedReason": verificationRejectedReason,
            "numberOfVoters":numberOfVoters
                };
        var dataJson = JSON.stringify(data);
        
        var url = window.location.href;
        url = url.split("passCourseEdit");
        url = url[0]+"passCourse";
        
        $.ajax({
               method: "PUT",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               url: window.location.href,
               data:dataJson,
               error : function(jqXHR, textStatus, errorThrown) {
                   // log the error to the console
                   console.log("The following error occured: " + textStatus, errorThrown);
               },
               success : function(result){
                    location.href = url;
               }
        });
    }
}

function backToPass(){
    var url = window.location.href;
    var url2 = url.split("passCourseEdit");
    url2 = url2[0]+"passCourse";
    location.href = url2;
}
