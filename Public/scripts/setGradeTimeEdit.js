function setCheck(){
    
    var applyStartDate = document.getElementById('applyStartDate').value;
    var applyStartTime = document.getElementById('applyStartTime').value;
    var applyEndDate = document.getElementById('applyEndDate').value;
    var applyEndTime = document.getElementById('applyEndTime').value;
    var dateError=0;
    
    applyStartDate = applyStartDate.split("-");
    applyStartDate = applyStartDate[1]+"-"+applyStartDate[2]+"-"+applyStartDate[0];
    applyEndDate = applyEndDate.split("-");
    applyEndDate = applyEndDate[1]+"-"+applyEndDate[2]+"-"+applyEndDate[0];
    var applyStartTimeData = moment(applyStartDate+" "+applyStartTime,"MM-DD-YYYY h:mm A").format();
    var applyEndTimeData = moment(applyEndDate+" "+applyEndTime, "MM-DD-YYYY h:mm A").format();
    if(applyEndTimeData<applyStartTimeData || applyStartTimeData == "Invalid date" || applyEndTimeData == "Invalid date"){
        dateError=1;
    }
    
    
    if(dateError==1 || applyStartTimeData == "Invalid date" || applyEndTimeData == "Invalid date"){
        alert("請輸入正確的日期及時間");
    }
    
    else{
        var check = getConfirmation("確定設定申請時間?");
        if (check){
            var data = {
                
                "startTime": applyStartTimeData,
                "endTime": applyEndTimeData,
                "state": 1
                
            };
            var dataJson = JSON.stringify(data);
            var url = window.location.href;
            url = url.split("setGradeTime");
            var url2 = url[0]+"lgSetTime";
            var url3 = url[0]+"setGradeTime";
            
            $.ajax({
                   method: "POST",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   url: url2,
                   data:dataJson,
                   error : function(jqXHR, textStatus, errorThrown) {
                   // log the error to the console
                   console.log("The following error occured: " + textStatus, errorThrown);
                   },
                   success : function(result){
                   location.href = url3;
                   }
                   });
        }
    }
    
    
}


