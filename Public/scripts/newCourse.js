var count=1;
var id=[0];
function courseAvailable(){
    var courseName = document.getElementById('courseName').value;
    var courseType = parseInt(document.getElementById('courseType').value);
    var campus = parseInt(document.getElementById('campus').value);
    var targetStudent = parseInt(document.getElementById('targetStudent').value);
    var courseTeacher = document.getElementById('courseTeacher').value;
    var coursePlace = document.getElementById('coursePlace').value;
    var courseCost = parseInt(document.getElementById('courseCost').value);
    var courseDate=[];
    var courseContent=[];
    var i=0;
    var dateError=0;
    for(i=0;i<id.length;i++){
        var courseStartDate = document.getElementById('courseStartDate'+id[i]).value;
        var courseStartTime = document.getElementById('courseStartTime'+id[i]).value;
        var courseEndDate = courseStartDate;
        var courseEndTime = document.getElementById('courseEndTime'+id[i]).value;
        
        courseStartDate = courseStartDate.split("-");
        courseStartDate = courseStartDate[1]+"-"+courseStartDate[2]+"-"+courseStartDate[0];
        courseEndDate = courseEndDate.split("-");
        courseEndDate = courseEndDate[1]+"-"+courseEndDate[2]+"-"+courseEndDate[0];
        var courseStartTimeData = moment(courseStartDate+" "+courseStartTime,"MM-DD-YYYY h:mm A").format();
        var courseEndTimeData = moment(courseEndDate+" "+courseEndTime, "MM-DD-YYYY h:mm A").format();
        if(courseEndTimeData<courseStartTimeData || courseStartTimeData == "Invalid date" || courseEndTimeData == "Invalid date"){
            dateError=1;
        }
        courseDate.push(courseStartTimeData);
        courseDate.push(courseEndTimeData);
        
        var content = document.getElementById('content'+id[i]).value;
        courseContent.push(content);
    }
    
    var applyStartDate = document.getElementById('firstApplyStartDate').value;
    var applyStartTime = document.getElementById('firstApplyStartTime').value;
    var applyEndDate = document.getElementById('firstApplyEndDate').value;
    var applyEndTime = document.getElementById('firstApplyEndTime').value;
    
    applyStartDate = applyStartDate.split("-");
    applyStartDate = applyStartDate[1]+"-"+applyStartDate[2]+"-"+applyStartDate[0];
    applyEndDate = applyEndDate.split("-");
    applyEndDate = applyEndDate[1]+"-"+applyEndDate[2]+"-"+applyEndDate[0];
    var firstApplyStartTimeData = moment(applyStartDate+" "+applyStartTime,"MM-DD-YYYY h:mm A").format();
    var firstApplyEndTimeData = moment(applyEndDate+" "+applyEndTime, "MM-DD-YYYY h:mm A").format();
    var firstNumberOfStudents = parseInt(document.getElementById('firstNumberOfStudents').value);
    
    applyStartDate = document.getElementById('secondApplyStartDate').value;
    applyStartTime = document.getElementById('secondApplyStartTime').value;
    applyEndDate = document.getElementById('secondApplyEndDate').value;
    applyEndTime = document.getElementById('secondApplyEndTime').value;
    
    applyStartDate = applyStartDate.split("-");
    applyStartDate = applyStartDate[1]+"-"+applyStartDate[2]+"-"+applyStartDate[0];
    applyEndDate = applyEndDate.split("-");
    applyEndDate = applyEndDate[1]+"-"+applyEndDate[2]+"-"+applyEndDate[0];
    var secondApplyStartTimeData = moment(applyStartDate+" "+applyStartTime,"MM-DD-YYYY h:mm A").format();
    var secondApplyEndTimeData = moment(applyEndDate+" "+applyEndTime, "MM-DD-YYYY h:mm A").format();
    var secondNumberOfStudents = parseInt(document.getElementById('secondNumberOfStudents').value);
    
    var applyMethod = parseInt(document.getElementById('applyMethod').value);
    var enrollPost = parseInt(document.getElementById('enrollPost').value);
    var courseIntroduction = document.getElementById('courseIntroduction').value;
    var other = document.getElementById('other').value;
    var courseState = parseInt(0);
    var numberOfApply = parseInt(0);
    
    if(courseDate.length==0){
        courseDate.push(moment("1970-01-01").format());
    }
    if(courseContent.length==0){
        courseContent.push("");
    }
    
    if(Number.isInteger(firstNumberOfStudents) && Number.isInteger(secondNumberOfStudents)&& Number.isInteger(courseCost)){
        
        if(dateError==1 || firstApplyStartTimeData == "Invalid date" || firstApplyEndTimeData == "Invalid date" || secondApplyStartTimeData == "Invalid date" || secondApplyEndTimeData == "Invalid date"){
            alert("請輸入正確的日期及時間");
        }
        else if(firstApplyEndTimeData>secondApplyStartTimeData){
            alert("「第二階段報名開始時間」早於「第一階段報名結束時間」");
        }
        else{
            var check = getConfirmation("確定新增至開課報名?");
            if (check){
                var data = {
                    "courseName": courseName,
                    "courseType": courseType,
                    "campus": campus,
                    "courseDate": courseDate,
                    "courseTeacher":courseTeacher,
                    "coursePlace": coursePlace,
                    "courseCost": courseCost,
                    "targetStudent": targetStudent,
                    "courseState": courseState,
                    "numberOfApply": numberOfApply,
                    "firstApplyStartTime": firstApplyStartTimeData,
                    "firstApplyEndTime": firstApplyEndTimeData,
                    "firstNumberOfStudents": firstNumberOfStudents,
                    "secondApplyStartTime": secondApplyStartTimeData,
                    "secondApplyEndTime": secondApplyEndTimeData,
                    "secondNumberOfStudents": secondNumberOfStudents,
                    "courseStartTime": courseStartTimeData,
                    "courseEndTime": courseEndTimeData,
                    "applyMethod": applyMethod,
                    "enrollPost": enrollPost,
                    "courseIntroduction": courseIntroduction,
                    "other": other,
                    "courseContent":courseContent
                };
                var dataJson = JSON.stringify(data);
                var url = window.location.href;
                url = url.split("newCourse");
                var url2 = url[0]+"courseAvailable";
                
                $.ajax({
                       method: "POST",
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       url: window.location.href,
                       data:dataJson,
                       error : function(jqXHR, textStatus, errorThrown) {
                       // log the error to the console
                       console.log("The following error occured: " + textStatus, errorThrown);
                       },
                       success : function(result){
                       location.href = url2;
                       }
                       });
            }
        }
    }
    else{
        alert("請於「錄取人數」和「課程費用」中輸入數字");
    }
    
}

function backToVotingResult(){
    var url = window.location.href;
    var url2 = url.split("newCourse");
    url2 = url2[0]+"index";
    location.href = url2;
}

function addCourseDate(){
    var table = document.getElementById("table");
    var row = table.insertRow(table.rows.length);
    var date = row.insertCell(0);
    var content = row.insertCell(1);
    id.push(count);
    date.innerHTML="<p id=alternateUiWidgetsExample><input type=text class='date start' id=courseStartDate"+count+" size=12>&nbsp;&nbsp;<input type=text class='time start' id=courseStartTime"+count+" size=8>&nbsp;&nbsp;～&nbsp;&nbsp;<input type=text class='time end' id=courseEndTime"+count+" size=8></p>";
    content.innerHTML="<td><input type=text size=50 id=content"+count+">&nbsp;<button onclick=removeRow(this,"+count+")>刪除</button></td>";
    datePicker();
    count+=1;
}

function removeRow(r,cellid){
    var row = r.parentNode.parentNode;
    row.parentNode.removeChild(row);
    var index = id.indexOf(cellid);
    id.splice(index,1);
}
